'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminTipoLicenciasCtrl', ['$scope', '$rootScope', 'AdminTipoLicenciasService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminTipoLicenciasService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
        $scope.paginationRequest = {
          term: '',
          orderBy: 'fecha_solicitud',
          orderDirection: 'asc',
          page: 0,
          pageSize: '10'
        };
        $scope.paginationResponse = {};
        $scope.paginationPages = [];

        $scope.setOrder = function(field) {
          $scope.paginationRequest.orderBy = field;

          if ($scope.paginationRequest.orderDirection =='asc') {
            $scope.paginationRequest.orderDirection = 'desc';
          } else {
            $scope.paginationRequest.orderDirection = 'asc';
          }
          $scope.loadPage();
        }

      	$scope.init = function() {
      		$scope.loadPage();
      	}

        $scope.loadPage = function(page) {
          if (page || page === 0) $scope.paginationRequest.page = page; 

          usSpinnerService.spin('spinner-1');
          AdminTipoLicenciasService.getTipoLicenciasPaginated(
            $scope.paginationRequest,
            function (response) {
              $scope.paginationResponse = response;

              $scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

              usSpinnerService.stop('spinner-1');
            }, 
            $rootScope.showErrorResponse
          );
        }

        $scope.editar = function (tipoLicencia) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-tipo-licencia-editar.html",
	        	controller: 'AdminTipoLicenciaEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		tipoLicencia: function() {
	          			return angular.copy(tipoLicencia);
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

    	$scope.borrar = function (tipoLicencia) {
    		usSpinnerService.spin('spinner-1');

    		AdminTipoLicenciasService.borrarTipoLicencia(
    			tipoLicencia,
    			function (response) {
					usSpinnerService.stop('spinner-1');
					$scope.init();
				},
				$rootScope.showErrorResponse
    		);
    	}
	}
]);

angular.module('app')
  .controller('AdminTipoLicenciaEditarCtrl', ['$scope', '$rootScope', 'AdminTipoLicenciasService', 'usSpinnerService', '$modal', '$modalInstance', 'tipoLicencia',
  	function ($scope, $rootScope, AdminTipoLicenciasService, usSpinnerService, $modal, $modalInstance, tipoLicencia) {
    	$scope.tipoLicencia = tipoLicencia;
    	
    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		//console.debug(angular.toJson($scope.tipoLicencia));
    		
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');
    		
			AdminTipoLicenciasService.saveTipoLicencia(
    			$scope.tipoLicencia,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$modalInstance.close(response);
    			},
				$rootScope.showErrorResponse
    		);
    	}

    	$scope.close = function() {
          	$modalInstance.close();
        };
	}
]);