'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AprobacionCtrl', [ '$scope', '$rootScope', '$modal', '$stateParams', '$compile', 'AprobacionService', 'LicenciasService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', 
  	function ($scope, $rootScope, $modal, $stateParams, $compile, AprobacionService, LicenciasService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService ) {

	  	$scope.tipoLicencias = [];
	  	$scope.tipoAprobacion = "";
	  	$scope.jefes = [];

	     $scope.paginationRequest = {
			term: '',
			orderBy: 'fecha_solicitud',
			orderDirection: 'asc',
			page: 0,
			pageSize: '10'
		};
		$scope.paginationResponse = {};
		$scope.paginationPages = [];

		$scope.setOrder = function(field) {
			$scope.paginationRequest.orderBy = field;

			if ($scope.paginationRequest.orderDirection =='asc') {
				$scope.paginationRequest.orderDirection = 'desc';
			} else {
				$scope.paginationRequest.orderDirection = 'asc';
			}
			$scope.loadPage();
		}

	    $scope.$watch('currentPeriodo', function(newValue, oldValue){
	        if (newValue && newValue != null) {
	          $scope.setTipoLicencias(newValue);
	        }
	    });

	    $scope.setTipoLicencias = function(periodo) {
	      LicenciasService.getTiposLicenciaUsuario($rootScope.user.empleado_id, periodo.id,
	          function (response) {
	          	$scope.tipoLicencias = response;
	          },
	          $rootScope.showErrorResponse
	        );
	    };

	    $scope.init = function() {
	    	$scope.tipoAprobacion = $stateParams.tipo;
	  		$scope.licencias = [];
	  		$scope.jefes = [];

	    	usSpinnerService.spin('spinner-1');
			AprobacionService.getJefesYDelegados(
				function (response) {
					for (var i = 0; i < response.length; i++) {
						$scope.jefes.push(response[i]);
					}
					$scope.loadPage();
				}, 
				$rootScope.showErrorResponse
			);
	    };

	    $scope.loadPage = function(page) {
	    	if (page || page === 0) $scope.paginationRequest.page = page; 

	    	usSpinnerService.spin('spinner-1');
	    	AprobacionService.getLicenciasToApprobe(
				$scope.tipoAprobacion,
	    		$scope.paginationRequest,
	    		function (response) {
	    			$scope.paginationResponse = response;

	    			$scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

		    		usSpinnerService.stop('spinner-1');
	    		}, 
	    		$rootScope.showErrorResponse
	    	);
	    }

	    $scope.getResponsable = function(area) {
	    	var empleado = null;
	    	if (area.responsable_delegado_id) {
	    		for (var i = 0; i < $scope.jefes.length; i++) {
	    			var jefe = $scope.jefes[i];
	    			if (jefe.id == area.responsable_delegado_id) {
	    				empleado = jefe;
	    				break;
	    			}
	    		}
	    	} else {
	    		for (var i = 0; i < $scope.jefes.length; i++) {
	    			var jefe = $scope.jefes[i];
	    			if (jefe.id == area.responsable_id) {
	    				empleado = jefe;
	    				break;
	    			}
	    		}
	    	}

	    	var nombres = '';
	    	if (empleado) {
	    		nombres = empleado.nombres + ' ' + empleado.apellidos;
	    	}
	    	return nombres;
	    }

	    $scope.aprove = function(licencia) {
	        AprobacionService.approbeLicencia(licencia, function(response) {
	        	$scope.init();
	        }, $rootScope.showErrorResponse);
	    }

	    $scope.reject = function(licencia) {
	        AprobacionService.rejectLicencia(licencia, function(response) {
	        	$scope.init();
	        }, $rootScope.showErrorResponse);
	    }

	    $scope.setPopoverContent = function(licencia) {
	    	$scope.userInfoPopover.content.empleado = licencia.empleado;
	    	LicenciasService.getTiposLicenciaUsuario(
	    		licencia.empleado.id, 
	    		$scope.currentPeriodo.id,
				function (response) {
					$scope.userInfoPopover.content.otras = response;
				},
				$rootScope.showErrorResponse
	        );

			LicenciasService.getVacacionesHistorico(
				licencia.empleado.id, 
				function (response) {
					$scope.userInfoPopover.content.vacaciones = response;
				},
				$rootScope.showErrorResponse
			);
	    }

	    $scope.userInfoPopover = {
	    	content: {},
			templateUrl: 'views/aprobacion.popover.html',
			title: 'Información del usuario'
		};

		$scope.editar = function (licencia) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/dialog-editar-licencia.html",
	        	controller: 'AppLicenciaEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		licencia: function() {
	          			return angular.copy(licencia);
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }
  	}
  ]);


angular.module('app')
  .controller('AppLicenciaEditarCtrl', ['$scope', '$rootScope', 'LicenciasService', 'usSpinnerService', '$modal', '$modalInstance', 'licencia',
  	function ($scope, $rootScope, LicenciasService, usSpinnerService, $modal, $modalInstance, licencia) {
    	$scope.licencia = {};
    	$scope.empleados = [];
    	
    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		licencia.fecha_inicio = new Date(licencia.fecha_inicio.replace(/-/g, '\/').replace(/T.+/, ''));
    		licencia.fecha_fin = new Date(licencia.fecha_fin.replace(/-/g, '\/').replace(/T.+/, ''));

    		$scope.licencia = licencia;
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');

			LicenciasService.saveLicencia(
				$scope.licencia, 
				function(response) {
					usSpinnerService.stop('spinner-1');
          			$modalInstance.close(response);
				}, 
				$rootScope.showErrorResponse
			);
		}

    	$scope.close = function() {
          	$modalInstance.close();
        };
	}
]);