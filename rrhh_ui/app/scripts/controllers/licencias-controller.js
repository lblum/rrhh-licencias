'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciasCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
.controller('LicenciasCtrl', ['$scope', '$rootScope', 'LicenciasService', 'usSpinnerService', '$modal',
  function ($scope, $rootScope, LicenciasService, usSpinnerService, $modal) {

    $scope.feriados = [];

    $scope.eventos = [];
    $scope.eventsF = function (start, end, timezone, callback) {
      if ($rootScope.user) {
        usSpinnerService.spin('spinner-1');
        LicenciasService.getCalendarEvents(
          $rootScope.user.empleado_id, 
          start.toISOString(), 
          end.toISOString(),
          function(response) {
            //console.debug(angular.toJson(response));
            callback(response);
            usSpinnerService.stop('spinner-1');
          },
          $rootScope.showErrorResponse
        );
      }
    }

    $scope.eventSources = [$scope.eventos, $scope.eventsF];

  	$scope.tipoLicencias = [];
    $scope.misTipoLicencias = [];
    $scope.misVacacionesHistorico = [];
    $scope.empleado = {};
    $scope.me = {};

    $scope.currentlicencia = null;

    var periodoDone = false;
    var userDone = false;
    var watchedPeriodo = null;

    $scope.$watch('currentPeriodo', function(newValue, oldValue){
        if (newValue && newValue != null) {
          periodoDone = true;
          watchedPeriodo = newValue;
          if (userDone)
            $scope.setMisTipoLicencias(newValue);
        }
    });

    $scope.$watch('user', function(newValue, oldValue) {
      if (newValue) {
        userDone = true;

        LicenciasService.getEmpleado(
            $rootScope.user.empleado_id,
            function (response) {
              //console.debug(angular.toJson(response));
              $scope.me = response;
            },
            $rootScope.showErrorResponse
        );

        LicenciasService.getFeriados(
          function (response) {
            $scope.feriados = response;
          },
          $rootScope.showErrorResponse
        );

        if (periodoDone) {
          $scope.setMisTipoLicencias(watchedPeriodo);
        }
      }
    });

    $scope.init = function() {
      
    }

    $scope.setEmpleadoDestino = function(empleado) {
      $scope.empleado = empleado;
      $scope.currentlicencia.empleado_id = empleado.id;
      $scope.setTipoLicencias($scope.currentPeriodo, empleado.id);
    }

    $scope.setTipoLicencias = function(periodo, empleado_id) {
      LicenciasService.getTiposLicenciaUsuario(empleado_id, periodo.id,
          function (response) {
            //console.debug(angular.toJson(response));
            $scope.tipoLicencias = response;
          },
          $rootScope.showErrorResponse
      );
    };

    $scope.setMisTipoLicencias = function(periodo) {
      LicenciasService.getTiposLicenciaUsuario($rootScope.user.empleado_id, periodo.id,
          function (response) {
            //console.debug(angular.toJson(response));
            $scope.misTipoLicencias = response;

            // DIALOGO ON STARTUP
            $scope.currentlicencia = {
              empleado_id : $rootScope.user.empleado_id,
              periodo_id : periodo.id
            };
            $scope.tipoLicencias = $scope.misTipoLicencias;
            $scope.empleado = $scope.me;
          },
          $rootScope.showErrorResponse
        );
        LicenciasService.getVacacionesHistorico($rootScope.user.empleado_id, 
            function (response) {
              //console.debug(angular.toJson(response));
              $scope.misVacacionesHistorico = response;
            },
            $rootScope.showErrorResponse
        );
    };

    $scope.refresh = function() {
      angular.element('#calendar').fullCalendar('refetchEvents');
      $scope.setMisTipoLicencias($scope.currentPeriodo);
    }

  	$scope.uiConfig = {
      calendar:{
        editable: true,
        header:{
          right: 'prev,next'
        },
        eventClick: function(event) {
          if (!$scope.hasPermiso('licencia.alta')) {
            return;
          }
          if (event.status == "pendiente") {
            $scope.tipoLicencias = $scope.misTipoLicencias;
            $scope.empleado = $scope.me;

            LicenciasService.getPeriodoFor(event.start.toDate(), function (response) {
              if (response) {
                //console.debug(angular.toJson(event));
                var tipo = null;
                
                for (var i = 0; i < $scope.tipoLicencias.length; i++) {
                  var tipoLicencia = $scope.tipoLicencias[i];
                  if (tipoLicencia.nombre == event.title) {
                    tipo = tipoLicencia;
                  }
                }

                $scope.currentlicencia = {
                  id: event.id,
                  empleado_id : $rootScope.user.empleado_id,
                  periodo_id : response.id,
                  fecha_inicio : event.start.format('DD/MM/YYYY'),
                  // TODO: substract hack
                  fecha_fin : (event.end ? event.end.subtract(1, 'day').format('DD/MM/YYYY') : event.start.format('DD/MM/YYYY')),
                  tipo_licencia : tipo,
                  notas_empleado : event.comentarios,
                  evento_cantidad : event.cantidad
                };
              }
            }, $rootScope.showErrorResponse);
          }
        },
        dayClick: function(date, jsEvent, view) {
          if (!$scope.hasPermiso('licencia.alta')) {
            return;
          }
          LicenciasService.getPeriodoFor(date, function (response) {
            if (response) {
              $scope.currentlicencia = {
                empleado_id : $rootScope.user.empleado_id,
                periodo_id : response.id,
                fecha_inicio : date.format('DD/MM/YYYY'),
                  evento_cantidad : 0
              };

              $scope.tipoLicencias = $scope.misTipoLicencias;
              $scope.empleado = $scope.me;
            }
          }, $rootScope.showErrorResponse);
        },
        dayRender: function (date, cell) {
          if ((date.day() == 6 || date.day() == 0))
            cell.addClass("weekend");

          $scope.$watch('feriados', function(newValue, oldValue){
              if (newValue && newValue != null && newValue.length) {
                $scope.setFeriado(date, cell, newValue);
              }
          });
          return "";              
        },
        eventRender: function(event, element) {       
          // Setea el ícono según el estado de la aprobación
          var iconClass = 'icon-calendar ';
          switch(event.status.trim()){                                        
            case "aprobado":
              iconClass += 'icon-ok' ;
              break;
            case "rechazado":
              iconClass += 'icon-cancel';
              break;
            case "pendiente":
              iconClass += 'icon-wait';
              break;
            case "app_jefe":
              iconClass += 'icon-pre';
              break;
            default:
              iconClass += '';                      
          }
          element.append( '<div class="' + iconClass + '">&nbsp;</div>');
        },
        nextDayThreshold: '00:00:00'
      }
    };

    $scope.setFeriado = function(date, cell, feriados) {
      for (var i = 0; i < feriados.length; i++) {
        var feriado = feriados[i];
        var d = new Date(feriado.fecha.replace(/-/g, '\/').replace(/T.+/, ''));
        
        if (d.getDate() == date.date() 
          && d.getMonth() == date.month()
          && d.getFullYear() == date.year()) {
          cell.addClass(feriado.simbolo);
          cell.append('<span>' + feriado.nombre + '</span>');
        }
      }
    };

    var getCantidadDisponible = function (callback) {
      if ($scope.currentlicencia.tipo_licencia.simbolo != 'event-vacaciones') {
        callback($scope.currentlicencia.tipo_licencia.cantidadDiponible);
      } else {
        LicenciasService.getVacacionesPendientesFor(
          $scope.currentlicencia.empleado_id,
          function (response) {
        console.debug(response);
            callback(response.cantidad);
          },
          $rootScope.showErrorResponse
        );
      }
    };
  	
    $scope.guardarLicencia = function () {
      var inicio = $rootScope.parseStringDate($scope.currentlicencia.fecha_inicio);
      var fin = $rootScope.parseStringDate($scope.currentlicencia.fecha_fin);
      if (inicio > fin) {
        $rootScope.showError('La fecha de inicio debe preceder a la de fin');
        return;
      }
      getCantidadDisponible(function (result) {
        var days = (fin-inicio)/(1000*60*60*24) + 1;
        var disponible = result;
        if (days > disponible) {
          if (disponible > 0) {
            $rootScope.showError('El límite actual para este tipo de licencias es de ' + disponible + ' días');
          } else {
            $rootScope.showError('No tiene días disponibles para este tipo de licencias.');
          }
          return;
        }

        $scope.currentlicencia.fecha_inicio = inicio.toISOString();
        $scope.currentlicencia.fecha_fin = fin.toISOString();

        usSpinnerService.spin('spinner-1');
        LicenciasService.saveLicencia($scope.currentlicencia, function(response) {
          //console.debug(angular.toJson(response));
          $scope.refresh();
          usSpinnerService.stop('spinner-1');
          $scope.currentlicencia = null;
        }, $rootScope.showErrorResponse);
      });
    };

    $scope.borrarLicencia = function () {
      usSpinnerService.spin('spinner-1');
      LicenciasService.borrarLicencia(
        $scope.currentlicencia, 
        function(response) {
          $scope.refresh();
          usSpinnerService.stop('spinner-1');
          $scope.currentlicencia = null;
        }, 
        $rootScope.showErrorResponse
      );
    }

    $scope.elegirEmpleado = function (area) {
      var modalInstance = $modal.open({
        backdrop : MODALES_BACKDROP,
        templateUrl: "views/licencia/licencia-pick-user.html",
        controller: 'LicenciasPickUsrCtrl',
        windowClass: 'dialog-modal'
      });

      modalInstance.result.then(
        function(result) {
          if (result) {
              $scope.setEmpleadoDestino(result);
          }
        }, 
        $rootScope.showErrorResponse
      );
    }

}]);



//FILTERS
app.filter('licenciaDisponible', function() {
  return function(arr) {
    return arr.filter(function(licencia){
      return licencia && licencia.cantidadDiponible > 0;
    });
  };
});



angular.module('app')
  .controller('LicenciasPickUsrCtrl', ['$scope', '$rootScope', 'LicenciasService', 'usSpinnerService', '$modal', '$modalInstance', 
    function ($scope, $rootScope, LicenciasService, usSpinnerService, $modal, $modalInstance) {
      $scope.resultados = [];
      $scope.term = "";

      $scope.init = function () {
      }

      $scope.buscarEmpleado = function () {
        if ($scope.term.length < 1) return;
        $scope.resultados = [];

        LicenciasService.buscarEmpleados(
          $scope.term,
          function (response) {
            if (response) {
              for (var x = 0; x < response.length; x++) {
                if (response[x].activo)
                  $scope.resultados.push(response[x]);
              }
            }
          },
          $rootScope.showErrorResponse
        );
      }

      $scope.elegir = function(empleado) {
        $modalInstance.close(empleado);
      }

      $scope.close = function() {
          $modalInstance.close();
      };
    }
  ]
);