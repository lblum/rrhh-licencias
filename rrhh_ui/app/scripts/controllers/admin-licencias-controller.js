'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminLicenciasCtrl', ['$scope', '$rootScope', 'AdminLicenciasService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminLicenciasService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	$scope.empleados = [];
    	$scope.responsable_id = 0;
	    
	    $scope.dtOptions = DTOptionsBuilder.newOptions()
	      .withBootstrap()
	      .withPaginationType('full_numbers');	     
	      //.withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	    	DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    	DTColumnDefBuilder.newColumnDef(1),
	    	DTColumnDefBuilder.newColumnDef(2),
	    	DTColumnDefBuilder.newColumnDef(3),
	    	DTColumnDefBuilder.newColumnDef(4),
	    	DTColumnDefBuilder.newColumnDef(5),
	    	DTColumnDefBuilder.newColumnDef(6),
	    	DTColumnDefBuilder.newColumnDef(7).notSortable(),
	    	DTColumnDefBuilder.newColumnDef(8).notSortable()
	    ];

    	$scope.init = function() {
    		$scope.empleados = [];
    		usSpinnerService.spin('spinner-1');
    		AdminLicenciasService.getEmpleadosByPrincipal(function (response) {
    			//console.debug(angular.toJson(response));
				setTimeout(function () {
					for (var i = 0; i < response.length; i++) {	    				    			
						$scope.empleados.push(response[i]);
					}
					usSpinnerService.stop('spinner-1');
				}, 100);
	    	}, $rootScope.showErrorResponse);
    	}

        $scope.delegar = function(empleado) {
        	//console.debug(angular.toJson(empleado));
	        $rootScope.infoMessage = 'Aviso default';            
	        
	    	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/dialog-modal-delegar.html",
	        	controller: 'DelegarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		empleado: function() {
	          			return empleado;
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

        $scope.desDelegar = function(empleado) {
        	usSpinnerService.spin('spinner-1');
	    	AdminLicenciasService.desDelegarEmpleado(
	    		empleado.id,
	    		function (response) {
		    		usSpinnerService.stop('spinner-1');
  	        		$scope.init();
		    	}, 
		    	$rootScope.showErrorResponse
		    );
        }
	}
]);
