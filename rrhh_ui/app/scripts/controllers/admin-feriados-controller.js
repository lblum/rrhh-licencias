'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminFeriadosCtrl', ['$scope', '$rootScope', 'AdminFeriadosService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminFeriadosService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
      $scope.paginationRequest = {
        term: '',
        orderBy: 'fecha_solicitud',
        orderDirection: 'asc',
        page: 0,
        pageSize: '10'
      };
      $scope.paginationResponse = {};
      $scope.paginationPages = [];

      $scope.setOrder = function(field) {
        $scope.paginationRequest.orderBy = field;

        if ($scope.paginationRequest.orderDirection =='asc') {
          $scope.paginationRequest.orderDirection = 'desc';
        } else {
          $scope.paginationRequest.orderDirection = 'asc';
        }
        $scope.loadPage();
      }

      $scope.init = function() {
    		$scope.loadPage();
    	}

      $scope.loadPage = function(page) {
        if (page || page === 0) $scope.paginationRequest.page = page; 

        usSpinnerService.spin('spinner-1');
        AdminFeriadosService.getFeriadosPaginated(
          $scope.paginationRequest,
          function (response) {
            $scope.paginationResponse = response;

            $scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

            usSpinnerService.stop('spinner-1');
          }, 
          $rootScope.showErrorResponse
        );
      }

        $scope.editar = function (feriado) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-feriado-editar.html",
	        	controller: 'AdminFeriadoEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		feriado: function() {
	          			var p = angular.copy(feriado);
                  if (p) {
                    var fecha = new Date(p.fecha.replace(/-/g, '\/').replace(/T.+/, ''));
                    p.fecha = fecha;
                  } else {
                    p = {};
                  }
	          			return p;
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

    	$scope.borrar = function (feriado) {
    		usSpinnerService.spin('spinner-1');
        AdminFeriadosService.borrarFeriado(
          feriado,
          function (response) {
            usSpinnerService.stop('spinner-1');
            $scope.init();
          },
          $rootScope.showErrorResponse
        );
      }
	}
]);

angular.module('app')
  .controller('AdminFeriadoEditarCtrl', ['$scope', '$rootScope', 'AdminFeriadosService', 'usSpinnerService', '$modal', '$modalInstance', 'feriado',
  	function ($scope, $rootScope, AdminFeriadosService, usSpinnerService, $modal, $modalInstance, feriado) {
    	$scope.feriado = feriado;
    	$scope.currentFeriado = {feriado_id: feriado.id};
    	
    	$scope.dateOptions = {
  			formatYear: 'yy',
  			startingDay: 1
  		};

    	$scope.init = function () {
    		
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');
        AdminFeriadosService.saveFeriado(
    			$scope.feriado,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$modalInstance.close(response);
    			},
				  $rootScope.showErrorResponse
    		);
    	}

    	$scope.close = function() {
        $modalInstance.close();
      };
	}
]);