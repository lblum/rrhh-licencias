'use strict';

/**
 * @ngdoc function
 * @name app.controller:HistoriaCtrl
 * @description
 * # HistoriaCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('HistoriaCtrl', ['$scope', '$rootScope', 'HistoriaService', 'LicenciasService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', 
  	function ($scope, $rootScope, HistoriaService, LicenciasService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService) {
	    
	    $scope.paginationRequest = {
			term: '',
			orderBy: 'fecha_solicitud',
			orderDirection: 'asc',
			page: 0,
			pageSize: '10'
		};
		$scope.paginationResponse = {};
		$scope.paginationPages = [];

		$scope.setOrder = function(field) {
			$scope.paginationRequest.orderBy = field;

			if ($scope.paginationRequest.orderDirection =='asc') {
				$scope.paginationRequest.orderDirection = 'desc';
			} else {
				$scope.paginationRequest.orderDirection = 'asc';
			}
			$scope.loadPage();
		}

	    $scope.$watch('currentPeriodo', function(newValue, oldValue){
	        if (newValue && newValue != null) {
	          $scope.setTipoLicencias(newValue);
	        }
	    });

	    $scope.setTipoLicencias = function(periodo) {
	      LicenciasService.getTiposLicenciaUsuario($rootScope.user.empleado_id, periodo.id,
	          function (response) {
	          	$scope.tipoLicencias = response;
	          },
	          $rootScope.showErrorResponse
	        );
	    };

	    $scope.init = function() {
	    	$scope.loadPage();
	    };

	    $scope.loadPage = function(page) {
	    	if (page || page === 0) $scope.paginationRequest.page = page; 

	    	usSpinnerService.spin('spinner-1');
	    	HistoriaService.getLicenciasHistory(
	    		$scope.paginationRequest,
	    		function (response) {
	    			$scope.paginationResponse = response;

	    			$scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

		    		usSpinnerService.stop('spinner-1');
	    		}, 
	    		$rootScope.showErrorResponse
	    	);
	    }

	    $scope.setPopoverContent = function(licencia) {
	    	$scope.userInfoPopover.content.empleado = licencia.empleado;
	    	LicenciasService.getTiposLicenciaUsuario(licencia.empleado.id, $scope.currentPeriodo.id,
	          function (response) {
	          	$scope.userInfoPopover.content.otras = response;
	          },
	          $rootScope.showErrorResponse
	        );

			LicenciasService.getVacacionesHistorico(
				licencia.empleado.id, 
				function (response) {
					$scope.userInfoPopover.content.vacaciones = response;
				},
				$rootScope.showErrorResponse
			);
	    }

	    $scope.userInfoPopover = {
	    	content: {},
			templateUrl: 'views/aprobacion.popover.html',
			title: 'Información del usuario'
		};
	    
	    $scope.dtOptions = DTOptionsBuilder.newOptions()
	      .withBootstrap()
	      .withPaginationType('full_numbers')
	      .withOption('order', [1, 'asc']);
	      //.withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	    	DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    	DTColumnDefBuilder.newColumnDef(1),
	    	DTColumnDefBuilder.newColumnDef(2),
	    	DTColumnDefBuilder.newColumnDef(3),
	    	DTColumnDefBuilder.newColumnDef(4),
	    	DTColumnDefBuilder.newColumnDef(5),
	    	DTColumnDefBuilder.newColumnDef(6),
	    	DTColumnDefBuilder.newColumnDef(7),
	    	DTColumnDefBuilder.newColumnDef(8),
	    	//DTColumnDefBuilder.newColumnDef(9),
	    	//DTColumnDefBuilder.newColumnDef(10),
	    	DTColumnDefBuilder.newColumnDef(9).notSortable()
	    ];
  	}
]);
