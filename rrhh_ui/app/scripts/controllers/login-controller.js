'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciasCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
	.controller('LoginCtrl', ['$scope', '$rootScope', '$state', '$http', '$cookies', 'LoginService','$modal', 
		function ($scope, $rootScope, $state, $http, $cookies, LoginService, $modal) {
	    
	  		$scope.username = "admin";
			$scope.password = "password";

	  		$scope.login = function() {
	  			var user = {
	  				usuario : $scope.username,
	  				password : $scope.password
	  			}
	  			
	  			LoginService.login(
	  				user, 
	  				function (response) {
	  					var token = response.token;

	  					$http.defaults.headers.common['Authorization'] = token;

	  					LoginService.getPerfil(response.usuario, function (response) {
	  						//console.debug(angular.toJson(response));
	  						$rootScope.user = response;
	  						$rootScope.user.token = token;
	  						$cookies.put('user', angular.toJson(response));

	  						$state.go("app.licencias");
	  					},
	  					function (response) {
	  						console.debug(response);
	  					});
	  				},
	  				$rootScope.showErrorResponse
	  			);
	  		}
	    
		}
	]);
// LoginCtrl end