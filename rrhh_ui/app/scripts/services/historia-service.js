'use strict';

app.factory('HistoriaService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getLicenciasHistoryOld: function(callbackOk, callbackFail) {
				var url = baseUrl + 'licencia/getforhistory';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getLicenciasHistory: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'licencia/postforhistory';
            	return $resource(url, {}, {
	            	getLicenciasHistory: {
		                    method: 'POST'
		                }
		            }
		        ).getLicenciasHistory({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},

		}


	}
]);