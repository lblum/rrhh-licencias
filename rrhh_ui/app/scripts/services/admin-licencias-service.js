'use strict';

app.factory('AdminLicenciasService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getEmpleados: function(callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},
			
			getEmpleadosByPrincipal: function(callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetByPrincipalRol';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},
			
			getEmpleado: function(id, callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/get';
            	return $resource(url, {'id': id} )
            		.get(callbackOk, callbackFail);
			},

			getAreas: function(area_id,callbackOk, callbackFail) {
				var url = baseUrl + 'areas/get/:area_id';
				console.log("url:" + url);
            	return $resource(url, {"area_id": area_id} )
            		.query(callbackOk, callbackFail);
			},

			buscarEmpleados: function(term, callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetByTerm';
            	return $resource(url, {'id': term} )
            		.query(callbackOk, callbackFail);
			},
			
			delegarEmpleado: function(id, jefeId, callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetDelegar';
            	return $resource(url, {'id': id, 'jefeId': jefeId} )
            		.get(callbackOk, callbackFail);
			},
			
			desDelegarEmpleado: function(id, callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetDeshacerDelegar';
            	return $resource(url, {'id': id} )
            		.get(callbackOk, callbackFail);
			},
			
			getCurrentPeriodo: function(callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/GetCurrent';
            	return $resource(url, {} )
            		.get(callbackOk, callbackFail);
			},
			
			getTiposLicenciaUsuario: function(empleado_id, periodo_id, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposlicenciausuario/get/:empleado_id';

				//console.log("empleado_id" + empleado_id +  "periodoId" + periodo_id);
            	return $resource(url, {"empleado_id":empleado_id, "periodoId": periodo_id} )
            		.query(callbackOk, callbackFail);
			},
			
			getTiposLicenciaVacaciones: function(empleado_id, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposlicenciausuario/GetVacacionesHistorico/:empleado_id';

				//console.log("empleado_id" + empleado_id +  "periodoId" + periodo_id);
            	return $resource(url, {"empleado_id":empleado_id} )
            		.query(callbackOk, callbackFail);
			},
			
			saveCondicionEspecial: function(licencia, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposlicenciausuario/post';
            	return $resource(url, {}, {
	            	saveCondicionEspecial: {
		                    method: 'POST'
		                }
		            }
		        ).saveCondicionEspecial({}, angular.toJson(licencia))
				.$promise.then(callbackOk, callbackFail);
			},

		}


	}
]);