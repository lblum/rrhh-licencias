'use strict';

app.factory('AdminTipoLicenciasService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getTipoLicencias: function(callbackOk, callbackFail) {
				var url = baseUrl + 'tiposLicencia/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getTipoLicenciasPaginated: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposLicencia/PostPaginated';
            	return $resource(url, {}, {
	            	getTipoLicenciasPaginated: {
		                    method: 'POST'
		                }
		            }
		        ).getTipoLicenciasPaginated({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},
			
			getEmpleados: function(tipoLicencia, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposLicencia/GetEmpleadosByTipoLicencia';
            	return $resource(url, {'id': tipoLicencia.id} )
            		.query(callbackOk, callbackFail);
			},
			
			saveTipoLicencia: function(tipoLicencia, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposLicencia/post';
            	return $resource(url, {}, {
	            	saveTipoLicencia: {
		                    method: 'POST'
		                }
		            }
		        ).saveTipoLicencia({}, angular.toJson(tipoLicencia))
				.$promise.then(callbackOk, callbackFail);
			},
			
			borrarTipoLicencia: function(tipoLicencia, callbackOk, callbackFail) {
				var url = baseUrl + 'tiposLicencia/GetDelete';
            	return $resource(url, {'id': tipoLicencia.id}, {
	            	borrarTipoLicencia: {
		                    method: 'GET'
		                }
		            }
		        ).borrarTipoLicencia({}, {})
            	.$promise.then(callbackOk, callbackFail);
			},

		}


	}
]);