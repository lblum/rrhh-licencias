
(function Controller (){
	
	var app = angular.module('app', ['menu']); 
	app.controller('AppController', function(){

	});	
	
	app.directive('header',function(){
		return{
			restrict:'E',
			templateUrl: 'navbar.html',
			
		}		
	});
	
	app.directive('content',function(){
		return{
			restrict:'E',
			templateUrl: 'licencias.html',
			
		}		
	});
	
	app.directive('footer',function(){
		return{
			restrict:'E',
			templateUrl: 'footer.html',
			
		}		
	});
})();
