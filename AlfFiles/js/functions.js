
//Setea el HTML de una sección
function setPage(section, page, def ){
	$("#"+section).empty();	
	$("#"+section).load(page, function() { if(def) def.resolve(); });
}

// Setea cual de los links del navbar es el activo
function setActive(id){
	if(id){
		var x = $('#navbar ul li');
		x.each(function(i){
				$(this).removeClass();			
		 });    
		 $('#'+id).addClass('active');
	}
}

//Inicializa las secciones
function init(){
	// Create Deferred instances that can be handed to $.when()
	var defHeader = new $.Deferred();
	var defContent = new $.Deferred();
	var defFooter = new $.Deferred();
	
	setPage("header", "navbar.html", defHeader );
	setPage("content", "licencias.html", defContent  );
	setPage("footer", "footer.html", defFooter );	
	
	// Set up the chain of events...This execute after all loads
	$.when(defHeader,defContent, defFooter).then(function() {
    setActive('nv_lic');
	});
	
	
}	


