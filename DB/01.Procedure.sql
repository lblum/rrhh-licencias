CREATE PROCEDURE set_condiciones_especiales
	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @employee_id VARCHAR(200);
	DECLARE @ingreso DATETIME;
	DECLARE @cantidad int;
	DECLARE @periodo int;
	DECLARE @tipo_licencia_id bigint;
	
	SELECT @tipo_licencia_id = id FROM tipo_licencia WHERE simbolo = 'event-vacaciones';
	
	DECLARE cur CURSOR LOCAL FOR SELECT t.employee_id employee_id, CAST(t.fecha_alta AS DATETIME) ingreso,
			MAX(CAST(t.teoricos AS int)) cantidad, 
			MAX(CAST(t.periodo AS int)) periodo
		FROM tmp2 t
		GROUP BY t.fecha_alta, t.employee_id
		ORDER BY t.employee_id;

	open cur;
	fetch next from cur into @employee_id, @ingreso, @cantidad, @periodo;
	while @@FETCH_STATUS = 0 BEGIN
		DECLARE @firstLicencia DATETIME;
		SELECT @firstLicencia = MIN(l.fecha_inicio) 
				FROM licencia l 
				INNER JOIN empleado e ON l.empleado_id = e.id
				WHERE e.employee_id = @employee_id;
		
		INSERT INTO condicion_especial (empleado_id, tipo_licencia_id, periodo_id, 
			cantidad_acordada, ingreso_acordado) 
		SELECT e.id, @tipo_licencia_id, p.id, @cantidad, @ingreso
		FROM periodo p, empleado e
		WHERE e.employee_id = @employee_id
		 AND p.fechaFin > e.fechaIngreso
		 AND p.nombre NOT IN (SELECT ip.nombre FROM
								condicion_especial ice
								INNER JOIN periodo ip ON ip.id = ice.periodo_id
								INNER JOIN empleado ie ON ie.id = ice.empleado_id
								WHERE ie.employee_id = @employee_id)
		ORDER BY p.nombre;
		
		
		INSERT INTO licencia (empleado_id, tipo_licencia_id, cantidad, estado, fecha_inicio, 
			fecha_fin, fecha_solicitud)
		SELECT e.id, @tipo_licencia_id, @cantidad, 'aprobado', p.fechaInicio, 
			DATEADD(day, @cantidad, p.fechaInicio), @firstLicencia 
		FROM empleado e, periodo p
		WHERE employee_id = @employee_id
		AND YEAR(p.fechaInicio) < YEAR(@firstLicencia); 
		
		
		fetch next from cur into @employee_id, @ingreso, @cantidad, @periodo
	END

	close cur;
	deallocate cur;
END;