update tipo_licencia set codigo = '30' where nombre = 'Mudanza';

update tipo_licencia set codigo = '9' where nombre = 'Vacaciones';
update tipo_licencia set codigo = '1' where nombre = 'Enfermedad';
update tipo_licencia set codigo = '13', nombre = 'Licencia por examen', descripcion = 'Licencia por examen' where nombre = 'Estudio';

insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Licencia por casamiento', 1, 'Licencia por casamiento', 0, 'event-mudanza', 'event-mudanza', '10');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Licencia por nacimiento', 1, 'Licencia por nacimiento', 0, 'event-mudanza', 'event-mudanza', '11');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Licencia por defunción', 1, 'Licencia por defunción', 0, 'event-mudanza', 'event-mudanza', '12');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Donación sangre', 1, 'Donación sangre', 0, 'event-mudanza', 'event-mudanza', '17');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Día cumpleaños', 1, 'Día cumpleaños', 0, 'event-mudanza', 'event-mudanza', '22');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Llegada tarde', 1, 'Llegada tarde', 0, 'event-mudanza', 'event-mudanza', '23');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Retiro anticipado', 1, 'Retiro anticipado', 0, 'event-mudanza', 'event-mudanza', '25');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Enfermedad familiar', 1, 'Enfermedad familiar', 0, 'event-mudanza', 'event-mudanza', '14');
insert into tipo_licencia (nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo)
VALUES ('Maternidad', 1, 'Maternidad', 0, 'event-mudanza', 'event-mudanza', '3');