UPDATE l SET l.estado = 'balance'
FROM licencia l
INNER JOIN empleado e ON l.empleado_id = e.id
WHERE YEAR(e.fechaIngreso) > YEAR(l.fecha_inicio) OR l.cantidad = 0;

UPDATE licencia set fecha_exportacion = fecha_inicio
WHERE fecha_solicitud < ('2015-02-01');