﻿using ClosedXML.Excel;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace rrhh_licencias.Helper
{
    public class ExcelHelper
    {
        private static readonly string LEGAJO = "Legajo";

        private static readonly string NOMBRE = "Nombre";

        private static readonly string AREA = "Área";

        private static readonly string JEFE = "Jefe";

        private static readonly string TIPO_LICENCIA = "Tipo de licencia";
        private static readonly string FECHA_DESDE = "Fecha Desde";
        private static readonly string FECHA_HASTA = "Fecha Hasta";
        private static readonly string DIA_COMPLETO = "Día Completo";
        private static readonly string ESTADO = "Estado";
        private static readonly string PERIODO_VACACIONES = "Período";
        private static readonly string OBSERVACIONES = "Observaciones";
        private static readonly string FECHA_CERTIFICADO = "Fecha de Solicitud";
        private static readonly string CANTIDAD_DIAS = "Cantidad de Días";
        private static readonly string DIAS_PENDIENTES = "DÍAS PENDIENTES";
        private static readonly string DIAS_GOZADOS = "DÍAS GOZADOS";
        private static readonly string DIAS_TEORICOS = "DÍAS TEÓRICOS";
        private static readonly string ANIO = "AÑO";
        private static readonly string RESPONSABLE = "RESPONSABLE";
        private static readonly string EMPRESA = "Empresa";

        public static void SaveXlsx(Stream stream, IEnumerable<List<Object>> data, List<Object> headers, string worksheetName)
        {
            using (XLWorkbook workbook = new XLWorkbook())
            {
                IXLWorksheet ws = workbook.Worksheets.Add(worksheetName);
                var row = ws.FirstRow();
                WriteRowData(row, headers);
                foreach (var rowData in data)
                {
                    row = ws.LastRowUsed().RowBelow();
                    WriteRowData(row, rowData);
                }
                ws.Columns().AdjustToContents();
                workbook.SaveAs(stream);
            }
        }

        public static MemoryStream SaveXlsx(IEnumerable<List<Object>> data, List<Object> headers, string worksheetName)
        {
            MemoryStream stream = new MemoryStream();
            SaveXlsx(stream, data, headers, worksheetName);
            return stream;
        }

        public static void WriteRowData(IXLRow row, List<Object> rowData)
        {
            foreach (var value in rowData)
            {
                WriteNextCell(row, value);
            }
        }

        private static void WriteFirstCell(IXLRow row, object value)
        {
            row.FirstCell().Value = value ?? "N/A";
        }

        private static void WriteNextCell(IXLRow row, object value)
        {
            if (String.IsNullOrEmpty(row.FirstCell().Value.ToString()))
            {
                WriteFirstCell(row, value);
            }
            else
            {
                row.LastCellUsed().CellRight().Value = value ?? "N/A";
            }
        }

        public static void Novedades(Stream stream, List<licencia> licencias)
        {
            List<Object> headers = new List<Object>()
            {
                LEGAJO,
                NOMBRE,
                EMPRESA,
                TIPO_LICENCIA,
                FECHA_CERTIFICADO,
                FECHA_DESDE,
                FECHA_HASTA,
                DIA_COMPLETO,
                CANTIDAD_DIAS,
                ESTADO,
                PERIODO_VACACIONES,
                OBSERVACIONES
            };

            var data = licencias.Select(l => new List<Object>
            {
                l.empleado.legajo,
                $"{l.empleado.nombres} {l.empleado.apellidos}",
                l.empleado.empresa.nombre,
                l.tipo_licencia.nombre,
                l.fecha_solicitud,//.ToString("dd/MM/yyyy"),
                l.fecha_inicio,//.ToString("dd/MM/yyyy"),
                l.fecha_fin,//.ToString("dd/MM/yyyy"),
                l.tipo_licencia.event_class == "event-tarde" || l.tipo_licencia.event_class == "event-anticipado"
                    ? "NO" : "SI",
                l.cantidad,
                l.estado,
                String.Join("-", l.licencia_periodo_cantidad.Select(lpc => lpc.periodo.nombre)),
                getObservaciones(l)
            });

            SaveXlsx(stream, data, headers, "Novedades");
        }

        public static MemoryStream LicenciasAlDia(List<licencia> licencias)
        {
            var headers = new List<Object>()
            {
                LEGAJO,
                NOMBRE,
                AREA,
                JEFE,
                TIPO_LICENCIA
            };

            var data = licencias.Select(l => new List<Object>
            {
                l.empleado.employee_id,
                $"{l.empleado.nombres} {l.empleado.apellidos}",
                l.empleado.area.nombre,
                l.empleado.jefe != null ? $"{l.empleado.jefe.nombres} {l.empleado.jefe.apellidos}" : "N/A",
                l.tipo_licencia.nombre
            });

            return SaveXlsx(data, headers, "Licencias");
        }

        public static MemoryStream Saldos(List<TipoLicenciaUsuarioTO> lus)
        {
            var headers = new List<Object>()
            {
                LEGAJO,
                NOMBRE,
                EMPRESA,
                AREA,
                RESPONSABLE,
                ANIO,
                TIPO_LICENCIA,
                DIAS_TEORICOS,
                DIAS_GOZADOS,
                DIAS_PENDIENTES,
            };

            var data = lus.Select(lu => new List<Object>
            {
                lu.empleado.legajo,
                $"{lu.empleado.nombres} {lu.empleado.apellidos}",
                lu.empleado.empresa.nombre,
                lu.empleado.area.nombre,
                lu.empleado.jefe != null ? $"{lu.empleado.jefe.nombres} {lu.empleado.jefe.apellidos}" : "N/A",
                lu.periodo.fechaInicio.Year,
                lu.tipoLicencia,
                lu.cantidadOriginal,
                lu.cantidadUtilizada,
                lu.cantidadDisponible
            });

            return SaveXlsx(data, headers, "Saldos");
        }
        private static string getObservaciones(licencia l)
        {
            if (l.aprobador_rrhh_notas != null)
                return l.aprobador_rrhh_notas;
            if (l.aprobador_jefe_notas != null)
                return l.aprobador_jefe_notas;
            return l.notas_empleado;
        }
    }
}