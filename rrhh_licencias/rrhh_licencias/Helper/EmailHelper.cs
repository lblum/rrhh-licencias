﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using rrhh_licencias.Models;

namespace rrhh_licencias.Helper
{
    public class EmailHelper
    {
        public static void SendMailGeneric(MailAddress to, MailAddress cc, string subject, AlternateView body)
        {
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.To.Add(to);
            if (cc != null)
                message.CC.Add(cc);
            message.Subject = subject;
            message.AlternateViews.Add(body);
            sendMail(message);
        }

        public static void sendMailAlertJefe(empleado jefe, empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_ALERT_JEFE_SUBJECT", emp);
            string templateName = "email.created.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(jefe, db), getMailAddress(emp, db), subject, body);
        }
        public static void sendMailAlertEmpleadoYJefe(empleado jefe, empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_MODIFICACION_SUBJECT", emp);
            string templateName = "email.modified.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(emp, db), getMailAddress(jefe, db), subject, body);
        }
        public static void sendApproval(empleado jefe, empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_APPROVAL_SUBJECT", emp);
            string templateName = "email.approval.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(emp, db), getMailAddress(jefe, db), subject, body);
        }
        public static void sendApprovalJefe(empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_APPROVAL_SUBJECT", emp);
            string templateName = "email.approval.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(mailAddressRRHH(), getMailAddress(emp, db), subject, body);
        }
        public static void sendAlertRRHH(empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_APPROVAL_SUBJECT", emp);
            string templateName = "email.approval.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(mailAddressRRHH(), null, subject, body);
        }
        public static void sendMailRRHHModifica(empleado emp, empleado jefe, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_MODIFICACION_SUBJECT", emp);
            string templateName = "email.modified.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(emp, db), getMailAddress(jefe, db), subject, body);
        }
        public static void sendRejection(empleado jefe, empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_REJECTION_SUBJECT", emp);
            string templateName = "email.rejection.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(emp, db), getMailAddress(jefe, db), subject, body);
        }
        public static void sendRejectionFromJefe(empleado emp, string observaciones, string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_REJECTION_SUBJECT", emp);
            string templateName = "email.rejectionjefe.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(emp, db), null, subject, body);
        }
        public static void sendAlertVencimiento(empleado emp, holando_rrhh_licenciasEntities db, string dias, string periodo)
        {
            string subject = getSubject("MAIL_EXPIRATION_SUBJECT", emp);
            string bodystr = getBody("email.vencimiento.template.html", emp);
            bodystr = replaceDias(bodystr, dias, periodo);
            AlternateView body = getAlternateView(bodystr);
            SendMailGeneric(getMailAddress(emp, db), null, subject, body);
        }
        
        public static void sendMailAlertAdjunto(empleado jefe, empleado emp, string observaciones,
                                                             string tipoLicencia, holando_rrhh_licenciasEntities db)
        {
            string subject = getSubject("MAIL_ALERT_JEFE_SUBJECT", emp);
            string templateName = "email.adjunto.template.html";
            AlternateView body = getReplacedAlternateView(templateName, emp, observaciones, tipoLicencia);
            SendMailGeneric(getMailAddress(jefe, db), getMailAddress(emp, db), subject, body);
        }

        private static AlternateView getReplacedAlternateView(string templateName, empleado empleado, string observaciones, string tipoLicencia)
        {
            string body = getBody(templateName, empleado);
            body = getObservaciones(body, observaciones);
            body = replaceTipoLicencia(body, tipoLicencia);
            return getAlternateView(body);
        }

        private static string replaceDias(string template, string dias, string periodo)
        {
            return template.Replace("{{dias}}", dias).Replace("{{periodo}}", periodo);
        }
        private static string getSubject(string configKey, empleado empleado)
        {
            string sSubject = ConfigurationManager.AppSettings[configKey];
            return sSubject.Replace("{{nombres}}", empleado.nombres).Replace("{{apellidos}}", empleado.apellidos);
        }

        private static string replaceTipoLicencia(string template, string tipoLicencia)
        {
            return String.IsNullOrEmpty(tipoLicencia) ? template : template.Replace("{{tipoLicencia}}", tipoLicencia);
        }

        private static string getBody(string templateName, empleado empleado)
        {
            string template = File.ReadAllText(HttpContext.Current.Server.MapPath(string.Format(@"~/Resources/{0}", templateName)));
            return template.Replace("{{nombres}}", empleado.nombres).Replace("{{apellidos}}", empleado.apellidos);
        }
        private static string getObservaciones(string template, string mensaje)
        {

            string replacedMessage = mensaje == null ? "" : mensaje.Replace("\n", "</p><p>");
            return template.Replace("{{observaciones}}", replacedMessage);
        }

        private static AlternateView getAlternateView(string body)
        {
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            LinkedResource lr = new LinkedResource(HttpContext.Current.Server.MapPath(@"~/Resources/header.png"));
            lr.ContentId = "101";
            htmlView.LinkedResources.Add(lr);
            return htmlView;
        }

        private static MailAddress mailAddressRRHH()
        {
            string mailRRHH = ConfigurationManager.AppSettings["MAIL_RRHH"];
            return new MailAddress(mailRRHH, "RRHH");
        }

        private static MailAddress getMailAddress(empleado empleado, holando_rrhh_licenciasEntities db)
        {
            if (empleado == null) return null;
            usuario usr = db.usuario.FirstOrDefault(u => u.empleado_id == empleado.id);
            return new MailAddress(usr.email, $"{empleado.nombres} {empleado.apellidos}");
        }

        private static void sendMail(MailMessage message)
        {
            string smtpServer = ConfigurationManager.AppSettings["SMTP_SERVER"];
            string smtpPort = ConfigurationManager.AppSettings["SMTP_PORT"];
            string smtpSsl = ConfigurationManager.AppSettings["SMTP_SSL"];
            string smtpUser = ConfigurationManager.AppSettings["SMTP_USER"];
            string smtpPassword = ConfigurationManager.AppSettings["SMTP_PASSWORD"];
            string sSenderName = ConfigurationManager.AppSettings["SMTP_SENDER_NAME"];
            string sSenderEmail = ConfigurationManager.AppSettings["SMTP_SENDER_EMAIL"];

            bool useSsl = smtpSsl != null && smtpSsl == "true";

            message.From = new MailAddress(sSenderEmail, sSenderName);

            using (SmtpClient client = new SmtpClient(smtpServer))
            {
                client.Port = int.Parse(smtpPort);
                client.EnableSsl = useSsl;
                client.Timeout = 100000;
                if (smtpUser != string.Empty)
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(smtpUser, smtpPassword);
                }
                client.Send(message);
            }
        }


    }
}
