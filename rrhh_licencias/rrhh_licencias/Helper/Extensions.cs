﻿using System;

namespace rrhh_licencias.Helper
{
    public static class Extensions
    {
        public static DateTime FechaAcumulacionLimite(this DateTime fecha)
        {
            var limite = new DateTime(DateTime.Now.Year - ConfigurationHelper.LimiteAniosAcumulacion, 1, 1);
            //var limite = new DateTime(DateTime.Now.Year - ConfigurationHelper.LimiteAniosAcumulacion - 1, 
            //                            ConfigurationHelper.LimiteMesesAcumulacion, 1);
            return new DateTime(Math.Max(limite.Ticks, fecha.Ticks));
        }
    }
}