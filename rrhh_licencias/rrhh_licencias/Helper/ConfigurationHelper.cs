﻿namespace rrhh_licencias.Helper
{
    public static class ConfigurationHelper
    {
        public static int LimiteAniosAcumulacion
        {
            get
            {
                if (int.TryParse(System.Configuration.ConfigurationManager.AppSettings["LIMITE_ACUMULACION"], out int limite))
                    return limite;
                return 2; // Valor por default
            }
        }
        public static int LimiteMesesAcumulacion
        {
            get
            {
                if (int.TryParse(System.Configuration.ConfigurationManager.AppSettings["LIMITE_ACUMULACION_MES"], out int limite))
                    return limite;
                return 9; // Valor por default
            }
        }
    }
}