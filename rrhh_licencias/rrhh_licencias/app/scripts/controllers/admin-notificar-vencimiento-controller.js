﻿'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdminNotificarVencimientoCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('AdminNotificarVencimientoCtrl', ['$scope', '$rootScope', '$state', '$http', '$cookies', 'AdminEmpleadosService', 'usSpinnerService', '$modal',
        function ($scope, $rootScope, $state, $http, $cookies, AdminEmpleadosService, usSpinnerService, $modal) {

            $scope.empleadosOk = [];
            $scope.empleadosError = [];
            $scope.enviado = false;
            $scope.hayErrores = function () {
                return $scope.empleadosError.length > 0;
            };
            $scope.hayEnviosOk = function () {
                return $scope.empleadosOk.length > 0;
            };
            $scope.enviando = function () {
                return !($scope.hayErrores() || $scope.hayEnviosOk()) && $scope.enviado;
            }
            $scope.sendEmail = function () {
                usSpinnerService.spin('spinner-1');
                $scope.enviado = true;
                AdminEmpleadosService.sendNotificacionVencimiento(function (response) {
                    $scope.empleadosOk = response.success;
                    $scope.empleadosError = response.errors;
                    usSpinnerService.stop('spinner-1');
                }, function (response) {
                    $rootScope.showErrorResponse(response);
                });
            };
        }
    ]);
// AdminNotificarVencimientoCtrl end