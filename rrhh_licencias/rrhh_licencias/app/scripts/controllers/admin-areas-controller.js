'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminAreasCtrl', ['$scope', '$rootScope', 'AdminAreasService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminAreasService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
    	$scope.paginationRequest = {
			term: '',
			orderBy: 'fecha_solicitud',
			orderDirection: 'asc',
			page: 0,
			pageSize: '10'
		};
		$scope.paginationResponse = {};
		$scope.paginationPages = [];

        $scope.setOrder = function(field) {
          $scope.paginationRequest.orderBy = field;

          if ($scope.paginationRequest.orderDirection =='asc') {
            $scope.paginationRequest.orderDirection = 'desc';
          } else {
            $scope.paginationRequest.orderDirection = 'asc';
          }
          $scope.loadPage();
        }

    	$scope.init = function() {
    		$scope.loadPage();
    	}

	    $scope.loadPage = function(page) {
	    	if (page || page === 0) $scope.paginationRequest.page = page; 

	    	usSpinnerService.spin('spinner-1');
	    	AdminAreasService.getAreasPaginated(
	    		$scope.paginationRequest,
	    		function (response) {
	    			$scope.paginationResponse = response;

	    			$scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

		    		usSpinnerService.stop('spinner-1');
	    		}, 
	    		$rootScope.showErrorResponse
	    	);
	    }

        $scope.editar = function (area) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-area-editar.html",
	        	controller: 'AdminAreaEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		area: function() {
	          			return angular.copy(area);
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

    	$scope.borrar = function (area) {
    		usSpinnerService.spin('spinner-1');

    		AdminAreasService.getEmpleados(
    				area,
					function (response) {
						if (response && response.length) {
    						usSpinnerService.stop('spinner-1');
							$rootScope.showError('No se puede eliminar el área ya que hay empleados en ella.');
						} else {
							AdminAreasService.borrarArea(
				    			area,
				    			function (response) {
				    				usSpinnerService.stop('spinner-1');
				    				$scope.init();
				    			},
								$rootScope.showErrorResponse
				    		);
						}
					},
					$rootScope.showErrorResponse
				);
    	}
	}
]);

angular.module('app')
  .controller('AdminAreaEditarCtrl', ['$scope', '$rootScope', 'AdminAreasService', 'usSpinnerService', '$modal', '$modalInstance', 'area',
  	function ($scope, $rootScope, AdminAreasService, usSpinnerService, $modal, $modalInstance, area) {
    	$scope.area = area;
    	$scope.empleados = [];
    	
    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		if ($scope.area && $scope.area.id) {
    			AdminAreasService.getEmpleados(
    				$scope.area,
					function (response) {
						if (response) {
							for (var x = 0; x < response.length; x++) {
								if (response[x].activo)
									$scope.empleados.push(response[x]);
							}
						}
					},
					$rootScope.showErrorResponse
				);
    		}
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');
    		
			AdminAreasService.saveArea(
    			$scope.area,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$modalInstance.close(response);
    			},
				$rootScope.showErrorResponse
    		);
    	}

    	$scope.close = function() {
          	$modalInstance.close();
        };
	}
]);