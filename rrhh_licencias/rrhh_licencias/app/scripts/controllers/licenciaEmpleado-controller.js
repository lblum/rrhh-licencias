'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciaEmpleadoCtrl
 * @description
 * # LicenciaEmpleadoCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('LicenciaEmpleadoCtrl', ['$scope','$rootScope', '$stateParams', '$modal', 'AdminLicenciasService',
  	function ($scope, $rootScope, $stateParams, $modal, AdminLicenciasService) {
    	$scope.empleado = {};
      $scope.periodo = {};
      $scope.tiposLicencia = [];
      $scope.vacaciones = [];

    	$scope.init = function() {
    		AdminLicenciasService.getEmpleado(
    			$stateParams.empleadoId,
    			function (response) {
    				$scope.empleado = response;
    			},
    			$rootScope.showErrorResponse
        );

        AdminLicenciasService.getCurrentPeriodo(
          function (response) {
            $scope.periodo = response;
            AdminLicenciasService.getTiposLicenciaUsuario(
              $stateParams.empleadoId,
              $scope.periodo.id,
              function (response) {
                $scope.tiposLicencia = response;
              },
              $rootScope.showErrorResponse
            );
          },
          $rootScope.showErrorResponse
        );

        AdminLicenciasService.getTiposLicenciaVacaciones(
          $stateParams.empleadoId,
          function (response) {
            $scope.vacaciones = response;
          },
          $rootScope.showErrorResponse
        );
    	}

      $scope.editar = function(licencia) {
        var modalInstance = $modal.open({
          backdrop : MODALES_BACKDROP,
          templateUrl: "views/admin/admin-maximo-especial.html",
          controller: 'LicenciaEmpleadoEditarCtrl',
          windowClass: 'dialog-modal',
          resolve: {
            area: function() {
              var l = angular.copy(licencia);
              if (l && l.ingresoAcordado) {
                var fecha = new Date(l.ingresoAcordado.replace(/-/g, '\/').replace(/T.+/, ''));
                l.ingresoAcordado = fecha;
              } else {
                l = {};
              }
              return l;
            }
          }
        });
        modalInstance.result.then(
          function(result) {
            if (result) {
              $scope.init();
            }
          }, 
          $rootScope.showErrorResponse
        );
      }

  	}
  ]
 );


angular.module('app')
  .controller('LicenciaEmpleadoEditarCtrl', ['$scope', '$rootScope', 'AdminLicenciasService', 'usSpinnerService', '$modal', '$modalInstance', 'area',
    function ($scope, $rootScope, AdminLicenciasService, usSpinnerService, $modal, $modalInstance, licencia) {
      $scope.licencia = licencia;
      $scope.empleados = [];
      
      $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
      };

      $scope.init = function () {
        if (!$scope.licencia.cantidadEspecial || $scope.licencia.cantidadEspecial == -1) {
          $scope.licencia.cantidadEspecial = $scope.licencia.cantidadOriginal;
        }
      }

      $scope.guardar = function() {
        AdminLicenciasService.saveCondicionEspecial(
          $scope.licencia,
          function (response) {
            $modalInstance.close(response);
          },
          $rootScope.showErrorResponse
        );
      }

      $scope.close = function() {
        $modalInstance.close();
      };
  }
]);