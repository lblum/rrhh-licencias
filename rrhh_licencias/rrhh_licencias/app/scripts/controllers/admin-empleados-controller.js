'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminEmpleadosCtrl', ['$scope', '$rootScope', 'AdminEmpleadosService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminEmpleadosService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
    	$scope.paginationRequest = {
			term: '',
			orderBy: '',
			orderDirection: 'asc',
			page: 0,
			pageSize: '10'
		};
		$scope.paginationResponse = {};
		$scope.paginationPages = [];

		$scope.setOrder = function(field) {
			$scope.paginationRequest.orderBy = field;

			if ($scope.paginationRequest.orderDirection =='asc') {
				$scope.paginationRequest.orderDirection = 'desc';
			} else {
				$scope.paginationRequest.orderDirection = 'asc';
			}
			$scope.loadPage();
		}

    	$scope.init = function() {
    		$scope.loadPage();
    	}

	    $scope.loadPage = function(page) {
	    	if (page || page === 0) $scope.paginationRequest.page = page; 

	    	usSpinnerService.spin('spinner-1');
	    	AdminEmpleadosService.getEmpleadosPaginadosByPrincipal(
	    		$scope.paginationRequest,
	    		function (response) {
	    			$scope.paginationResponse = response;

	    			$scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

		    		usSpinnerService.stop('spinner-1');
	    		}, 
	    		$rootScope.showErrorResponse
	    	);
	    }

        $scope.editar = function (empleado) {
        	if (!empleado) empleado = {activo: 1};

        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-empleado-editar.html",
	        	controller: 'AdminEmpleadoEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		empleado: function() {
	          			return angular.copy(empleado);
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

        $scope.bajar = function (empleado) {
        	if (!empleado) empleado = {};

        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-empleado-baja.html",
	        	controller: 'AdminEmpleadoEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		empleado: function() {
	          			var emp = angular.copy(empleado);
	          			emp.activo = 0;
	          			return emp;
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }
	}
]);

angular.module('app')
  .controller('AdminEmpleadoEditarCtrl', ['$scope', '$rootScope', 'AdminEmpleadosService', 'usSpinnerService', '$modal', '$modalInstance', 'empleado',
  	function ($scope, $rootScope, AdminEmpleadosService, usSpinnerService, $modal, $modalInstance, empleado) {
    	$scope.empleado = empleado;
        $scope.areas = [];
        $scope.empresas = [];
    	$scope.roles = [];
    	$scope.usuario = {usuario_rol: []};
    	$scope.selectedRol = {};

    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		AdminEmpleadosService.getAreas(
				function (response) {
					if (response) {
						for (var x = 0; x < response.length; x++) {
							$scope.areas.push(response[x]);
						}
					}
				},
				$rootScope.showErrorResponse
            );

            AdminEmpleadosService.getEmpresas(
                function (response) {
                    if (response) {
                        for (var x = 0; x < response.length; x++) {
                            $scope.empresas.push(response[x]);
                        }
                    }
                },
                $rootScope.showErrorResponse
            );

    		if ($scope.empleado.id) {
    			$scope.retrieveEmpleado();
    		} else {
    			$scope.setPendingRoles();
    		}
    	}

    	$scope.setPendingRoles = function() {
    		$scope.roles = [];
			AdminEmpleadosService.getRoles(
				function (response) {
					if (response) {
						for (var x = 0; x < response.length; x++) {
							var rol = response[x];
							var found = false;
							if ($scope.usuario.id) {
								for (var i = 0; i < $scope.usuario.usuario_rol.length; i++) {
									var uRol = $scope.usuario.usuario_rol[i].rol;
									if (uRol.id == rol.id) {
										found = true;
									}
								}
							}
							if (!found) $scope.roles.push(rol);
						}
					}
				},
				$rootScope.showErrorResponse
			);
    	}

    	$scope.retrieveEmpleado = function() {
    		//var ingreso = new Date($scope.empleado.fechaIngreso.replace(/-/g, '\/').replace(/T.+/, ''));
    		var ingreso = $scope.empleado.fechaIngreso.substring(0, 10);
			$scope.empleado.fechaIngreso = ingreso;
			$scope.empleado.fechaBaja = null;

			AdminEmpleadosService.getEmpleadoUsuario(
				$scope.empleado,
				function (response) {
					if (response) {
						$scope.usuario = response;
						$scope.setPendingRoles();
					}
				},
				$rootScope.showErrorResponse
			);
    	}

    	$scope.agregarRol = function() {
    		if (!$scope.selectedRol || !$scope.selectedRol.id) {
    			return;
    		}

    		$scope.usuario.usuario_rol.push({rol: $scope.selectedRol});
    		for (var x = 0; x < $scope.roles.length; x++) {
    			var rol = $scope.roles[x];
    			if (rol.id == $scope.selectedRol.id) {
    				$scope.roles.splice(x, 1);
    				break;
    			}
    		}
    	}

    	$scope.quitarRol = function(usuario_rol) {
    		$scope.roles.push(usuario_rol.rol);
    		$scope.usuario.usuario_rol.splice($scope.usuario.usuario_rol.indexOf(usuario_rol), 1);
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');

    		if ($scope.empleado.jefe && $scope.empleado.id == $scope.empleado.jefe.id) {
    			$rootScope.showError('El jefe seleccionado es el mismo empleado a editar.');
    			usSpinnerService.stop('spinner-1');
    			return;
    		}
    		if ($scope.empleado.jefe && $scope.empleado.jefe.jefe
    			&& $scope.empleado.id == $scope.empleado.jefe.jefe.id) {
    			$rootScope.showError('No se puede asignar como jefe a una persona que responde al empleado a editar.');
				usSpinnerService.stop('spinner-1');
    			return;
    		}
    		
			AdminEmpleadosService.saveEmpleado(
    			$scope.empleado,
    			function (response) {
    				$scope.usuario.empleado_id = response.id;
    				setTimeout(function(){
    					AdminEmpleadosService.saveUsuario(
			    			$scope.usuario,
			    			function (response) {
			    				$modalInstance.close(response);
								usSpinnerService.stop('spinner-1');
	    					},
							$rootScope.showErrorResponse
			    		);
    				}, 500);
    			},
				$rootScope.showErrorResponse
    		);
    	}

    	$scope.close = function() {
          	$modalInstance.close();
        };

        $scope.elegirJefe = function() {
        	var modalInstance = $modal.open({
	        backdrop : MODALES_BACKDROP,
	        templateUrl: "views/licencia/licencia-pick-user.html",
	        controller: 'LicenciasPickUsrCtrl',
	        windowClass: 'dialog-modal'
	      });

	      modalInstance.result.then(
	        function(result) {
	          $scope.empleado.jefe = result;
	        }
	      );
        }
	}
]);