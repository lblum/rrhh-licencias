'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdminLicenciasHoyCtrl
 * @description
 * # AdminLicenciasHoyCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminLicenciasHoyCtrl', ['$scope', '$rootScope', 'AdminLicenciasHoyService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
      function ($scope, $rootScope, AdminLicenciasHoyService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
    	$scope.paginationRequest = {
			term: '',
			orderBy: '',
			orderDirection: 'asc',
			page: 0,
			pageSize: '10'
		};
		$scope.paginationResponse = {};
        $scope.paginationPages = [];
        $scope.fecha = moment().format("DD/MM/YYYY");

          $scope.$watch('fecha', function (newValue, oldValue) {
              if (newValue && newValue !== null && newValue !== oldValue) {
                  $scope.loadPage();
              }
          });

          $scope.setOrder = function (field) {
              $scope.paginationRequest.orderBy = field;

              if ($scope.paginationRequest.orderDirection === 'asc') {
                  $scope.paginationRequest.orderDirection = 'desc';
              } else {
                  $scope.paginationRequest.orderDirection = 'asc';
              }
              $scope.loadPage();
          };

          $scope.init = function () {
              console.log("admin-licencias-hoy-controller init");
              $scope.loadPage();
          };

          $scope.loadPage = function (page) {
              if (page || page === 0) $scope.paginationRequest.page = page;

              usSpinnerService.spin('spinner-1');
              console.log("admin-licencias-hoy-controller loadPage");
              AdminLicenciasHoyService.getLicenciasHoy(
                  $scope.formatDate($scope.fecha),
                  $scope.paginationRequest,
                  function (response) {
                      $scope.paginationResponse = response;

                      $scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

                      usSpinnerService.stop('spinner-1');
                  },
                  $rootScope.showErrorResponse
              );
          };

          $scope.descargar = function () {
              var url = baseAppUrl + "/Export/ExportLicenciasForDate?date=" + $scope.formatDate($scope.fecha) + "&term=" + $scope.paginationRequest.term;
              window.location.href = url;
          };

          $scope.formatDate = function (date) {
              return moment(date, "DD/MM/YYYY").format("YYYY-MM-DD");
          };
	}
]);