'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminRolesCtrl', ['$scope', '$rootScope', 'AdminRolesService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminRolesService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
        $scope.paginationRequest = {
            term: '',
            orderBy: 'fecha_solicitud',
            orderDirection: 'asc',
            page: 0,
            pageSize: '10'
        };
        $scope.paginationResponse = {};
        $scope.paginationPages = [];

        $scope.setOrder = function(field) {
            $scope.paginationRequest.orderBy = field;

            if ($scope.paginationRequest.orderDirection =='asc') {
                $scope.paginationRequest.orderDirection = 'desc';
            } else {
                $scope.paginationRequest.orderDirection = 'asc';
            }
            $scope.loadPage();
        }

    	$scope.init = function() {
    		$scope.loadPage();
    	}

        $scope.loadPage = function(page) {
            if (page || page === 0) $scope.paginationRequest.page = page; 

            usSpinnerService.spin('spinner-1');
            AdminRolesService.getRolesPaginados(
                $scope.paginationRequest,
                function (response) {
                    $scope.paginationResponse = response;

                    $scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

                    usSpinnerService.stop('spinner-1');
                }, 
                $rootScope.showErrorResponse
            );
        }

        $scope.editar = function (rol) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-rol-editar.html",
	        	controller: 'AdminRolEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		rol: function() {
	          			var ret = angular.copy(rol);
	          			if (!ret) ret = {rol_permiso: []};
	          			return ret;
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

    	$scope.borrar = function (rol) {
    		usSpinnerService.spin('spinner-1');
			AdminRolesService.borrarRol(
				rol,
				function (response) {
					usSpinnerService.stop('spinner-1');
					$scope.init();
				},
				$rootScope.showErrorResponse
			);
    	}
	}
]);

angular.module('app')
  .controller('AdminRolEditarCtrl', ['$scope', '$rootScope', 'AdminRolesService', 'usSpinnerService', '$modal', '$modalInstance', 'rol',
  	function ($scope, $rootScope, AdminRolesService, usSpinnerService, $modal, $modalInstance, rol) {
    	$scope.rol = rol;
    	$scope.permisos = [];
    	$scope.selectedPermiso = {};
    	
    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		$scope.permisos = [];
			AdminRolesService.getPermisos(
				function (response) {
					if (response) {
						for (var x = 0; x < response.length; x++) {
							var permiso = response[x];
							var found = false;
							for (var i = 0; i < $scope.rol.rol_permiso.length; i++) {
								var rp = $scope.rol.rol_permiso[i].permiso;
								if (permiso.id == rp.id) {
									found = true;
									break;
								}

							}
							if (!found) $scope.permisos.push(response[x]);
						}
					}
				},
				$rootScope.showErrorResponse
			);
    	}

    	$scope.guardar = function() {
    		//console.debug(angular.toJson($scope.rol));
    		usSpinnerService.spin('spinner-1');
    		
			AdminRolesService.saveRol(
    			$scope.rol,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$modalInstance.close(response);
    			},
				$rootScope.showErrorResponse
    		);
    	}

    	$scope.agregarPermiso = function () {
    		if (!$scope.selectedPermiso || !$scope.selectedPermiso.id) {
    			return;
    		}

    		$scope.rol.rol_permiso.push({
    			rol_id: $scope.rol.id,
    			permiso: $scope.selectedPermiso,
    			permiso_id: $scope.selectedPermiso.id
    		});

    		for (var x = 0; x < $scope.permisos.length; x++) {
    			var permiso = $scope.permisos[x];
    			if (permiso.id == $scope.selectedPermiso.id) {
    				$scope.permisos.splice(x, 1);
    				break;
    			}
    		}
    	}

    	$scope.quitarPermiso = function (rolPermiso) {
    		$scope.permisos.push(rolPermiso.permiso);
    		$scope.rol.rol_permiso.splice($scope.rol.rol_permiso.indexOf(rolPermiso), 1);
    	}

    	$scope.close = function() {
          	$modalInstance.close();
        };
	}
]);