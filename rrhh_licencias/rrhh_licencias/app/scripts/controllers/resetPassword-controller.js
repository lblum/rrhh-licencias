﻿'use strict';

/**
 * @ngdoc function
 * @name app.controller:ResetPasswordCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('ResetPasswordCtrl', ['$scope', '$rootScope', '$state', '$http', '$cookies', 'AdminEmpleadosService', 'usSpinnerService', '$modal',
        function ($scope, $rootScope, $state, $http, $cookies, AdminEmpleadosService, usSpinnerService, $modal) {

            $scope.username = "";
            $scope.password = "";
            $scope.confirmPassword = null;
            $scope.empleado = null;
            $scope.elegirEmpleado = function (area) {
                var modalInstance = $modal.open({
                    backdrop: MODALES_BACKDROP,
                    templateUrl: "views/licencia/licencia-pick-user.html",
                    controller: 'LicenciasPickUsrCtrl',
                    windowClass: 'dialog-modal'
                });

                modalInstance.result.then(
                    function (result) {
                        if (result) {
                            $scope.empleado = result;
                        }
                    },
                    $rootScope.showErrorResponse
                );
            };
            $scope.changePassword = function (password) {
                console.log($scope.empleado);
                usSpinnerService.spin('spinner-1');
                AdminEmpleadosService.getEmpleadoUsuario($scope.empleado,
                    function (response) {
                        let usuario = response;
                        usuario.password = password;
                        AdminEmpleadosService.saveUsuario(usuario,
                            function (response) {
                                usSpinnerService.stop('spinner-1');
                                $rootScope.showInfo("contraseña modificada");
                            }
                            , $rootScope.showErrorResponse);
                    },
                    $rootScope.showErrorResponse
                );
            };
        }
    ]);
// ResetPasswordCtrl end