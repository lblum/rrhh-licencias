'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciasCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('LicenciasCtrl', ['$scope', '$rootScope', 'LicenciasService', 'usSpinnerService', '$modal',
        function ($scope, $rootScope, LicenciasService, usSpinnerService, $modal) {

            $scope.feriados = [];

            $scope.eventos = [];
            $scope.eventsF = function (start, end, timezone, callback) {
                if ($scope.empleado && $scope.empleado.id) {
                    usSpinnerService.spin('spinner-1');
                    LicenciasService.getCalendarEvents(
                        $scope.empleado.id,
                        start.toISOString(),
                        end.toISOString(),
                        function (response) {
                            //console.debug(angular.toJson(response));
                            callback(response);
                            usSpinnerService.stop('spinner-1');
                        },
                        $rootScope.showErrorResponse
                    );
                }
            }

            $scope.eventSources = [$scope.eventos, $scope.eventsF];
            $scope.tipoLicencias = [];
            $scope.misTipoLicencias = [];
            $scope.misVacacionesHistorico = [];
            $scope.empleado = {};
            $scope.fileChange = false;
            $scope.saldo = null;

            $scope.currentlicencia = null;

            var periodoDone = false;
            var watchedPeriodo = null;

            $scope.$watch('currentPeriodo', function (newValue, oldValue) {
                if (newValue && newValue != null) {
                    periodoDone = true;
                    watchedPeriodo = newValue;
                }
            });

            $scope.$watch('user', function (newValue, oldValue) {
                if (newValue) {

                    LicenciasService.getEmpleado(
                        $rootScope.user.empleado_id,
                        function (response) {
                            //console.debug(angular.toJson(response));
                            $scope.empleado = response;
                        },
                        $rootScope.showErrorResponse
                    );

                    LicenciasService.getFeriados(
                        function (response) {
                            $scope.feriados = response;
                        },
                        $rootScope.showErrorResponse
                    );
                }
            });

            $scope.$watch('empleado', function (newValue, oldValue) {
                if (newValue && newValue.id && (!oldValue || (oldValue && (!oldValue.id || (oldValue.id && newValue.id !== oldValue.id))))) {
                    $scope.empleado = newValue;
                    $scope.refresh();
                }
            });

            $scope.init = function () {

            };

            $scope.setMisTipoLicencias = function (periodo) {
                $scope.tipoLicencias = [];
                $scope.misTipoLicencias = [];
                $scope.misVacacionesHistorico = [];

                LicenciasService.getTiposLicenciaUsuario($scope.empleado.id, periodo.id,
                    function (response) {
                        $scope.misTipoLicencias = response;
                        var saldo = response.filter(tl => tl.simbolo === 'saldo-2018');
                        console.log(saldo);
                        console.log(saldo.length);
                        if (saldo.length > 0) {
                            $scope.saldo = saldo[0];
                        }
                        console.log($scope.saldo);
                        // DIALOGO ON STARTUP
                        $scope.currentlicencia = {
                            empleado_id: $scope.empleado.id,
                            periodo_id: periodo.id
                        };
                        $scope.tipoLicencias = $scope.misTipoLicencias;
                    },
                    $rootScope.showErrorResponse
                );
                LicenciasService.getVacacionesHistorico($scope.empleado.id,
                    function (response) {
                        $scope.misVacacionesHistorico = response;
                    },
                    $rootScope.showErrorResponse
                );
            };
            $scope.tieneCantidadDisponible = function (licenciaTipo) {
                return licenciaTipo.cantidadDisponible != 0;
            };
            $scope.refresh = function () {
                angular.element('#calendar').fullCalendar('refetchEvents');
                $scope.setMisTipoLicencias($scope.currentPeriodo);
            };

            $scope.uiConfig = {
                calendar: {
                    editable: true,
                    eventStartEditable: false,
                    eventDurationEditable: false,
                    header: {
                        right: 'prev,next'
                    },
                    eventClick: function (event) {
                        if (!$scope.hasPermiso('licencia.alta')) {
                            return;
                        }
                        //if ($scope.hasRol('rrhh') || event.status == "pendiente") {
                        if(true){
                            $scope.tipoLicencias = $scope.misTipoLicencias;

                            LicenciasService.getPeriodoFor(event.start.toDate(), function (response) {
                                if (response) {
                                    //console.debug(angular.toJson(event));
                                    var tipo = null;

                                    for (var i = 0; i < $scope.tipoLicencias.length; i++) {
                                        var tipoLicencia = $scope.tipoLicencias[i];
                                        if (tipoLicencia.nombre == event.title) {
                                            tipo = tipoLicencia;
                                            console.log(tipo);
                                        }
                                    }

                                    $scope.currentlicencia = {
                                        id: event.id,
                                        empleado_id: $scope.empleado.id,
                                        periodo_id: response.id,
                                        fecha_inicio: event.start.format('DD/MM/YYYY'),
                                        // TODO: substract hack
                                        fecha_fin: event.end.subtract(1, 'hours').format('DD/MM/YYYY'),
                                        //event.start.clone().add(event.cantidad - 1, 'days').format('DD/MM/YYYY'),
                                        tipo_licencia: tipo,
                                        notas_empleado: event.comentarios,
                                        evento_cantidad: event.cantidad,
                                        adjunto_id: event.adjunto_id,
                                        adjunto_nombre: event.adjunto_nombre,
                                        adjunto_estado: event.adjunto_estado,
                                        estado: event.status
                                    };
                                    console.log('event', event);
                                    console.log('currentlicencia:', $scope.currentlicencia);
                                    $scope.fileChange = false;
                                }
                            }, $rootScope.showErrorResponse);
                        }
                    },
                    dayClick: function (date, jsEvent, view) {
                        if (!$scope.hasPermiso('licencia.alta')) {
                            return;
                        }
                        LicenciasService.getPeriodoFor(date, function (response) {
                            if (response) {
                                $scope.currentlicencia = {
                                    empleado_id: $scope.empleado.id,
                                    periodo_id: response.id,
                                    fecha_inicio: date.format('DD/MM/YYYY'),
                                    evento_cantidad: 0
                                };

                                $scope.tipoLicencias = $scope.misTipoLicencias;
                            }
                        }, $rootScope.showErrorResponse);
                    },
                    dayRender: function (date, cell) {
                        if ((date.day() == 6 || date.day() == 0))
                            cell.addClass("weekend");

                        $scope.$watch('feriados', function (newValue, oldValue) {
                            if (newValue && newValue != null && newValue.length) {
                                $scope.setFeriado(date, cell, newValue);
                            }
                        });
                        return "";
                    },
                    eventRender: function (event, element) {
                        // Setea el ícono según el estado de la aprobación
                        var iconClass = 'icon-calendar ';
                        switch (event.status.trim()) {
                            case "aprobado":
                                iconClass += 'icon-ok';
                                break;
                            case "rechazado":
                                iconClass += 'icon-cancel';
                                break;
                            case "pendiente":
                                iconClass += 'icon-wait';
                                break;
                            case "app_jefe":
                                iconClass += 'icon-pre';
                                break;
                            default:
                                iconClass += '';
                        }
                        element.append('<div class="' + iconClass + '">&nbsp;</div>');
                    },
                    nextDayThreshold: '00:00:00'
                }
            };

            $scope.setFeriado = function (date, cell, feriados) {
                for (var i = 0; i < feriados.length; i++) {
                    var feriado = feriados[i];
                    var d = new Date(feriado.fecha.replace(/-/g, '\/').replace(/T.+/, ''));

                    if (d.getDate() == date.date() &&
                        d.getMonth() == date.month() &&
                        d.getFullYear() == date.year()) {
                        cell.addClass(feriado.simbolo);
                        cell.append('<span>' + feriado.nombre + '</span>');
                    }
                }
            };

            $scope.getCantidadDisponible = function (callback) {
                if ($scope.currentlicencia.id) {
                    $scope.getCantidadDisponibleReal(callback, $scope.currentlicencia.evento_cantidad);
                }
                else {
                    $scope.getCantidadDisponibleReal(callback, 0);
                }
            };

            $scope.getCantidadDisponibleReal = function (callback, cantidadDeLicenciaActual) {
                var cantidad;
                if ($scope.currentlicencia.tipo_licencia.simbolo != 'event-vacaciones') {
                    cantidad = $scope.currentlicencia.tipo_licencia.cantidadDisponible;
                } else {
                    cantidad = $scope.cantidadVacacionesDisponibles();
                }
                callback(cantidad + cantidadDeLicenciaActual);
            };

            $scope.cantidadVacacionesDisponibles = function () {
                var cantidad = 0;
                $scope.misVacacionesHistorico.forEach(p => {
                    cantidad += p.cantidadDisponible;
                });
                return cantidad;
            }

            $scope.guardarLicencia = function () {
                $scope.addOrUpdateLicencia(LicenciasService.saveLicencia);
            };

            $scope.modificarLicencia = function () {
                $scope.addOrUpdateLicencia(LicenciasService.editarLicencia);
            };

            $scope.addOrUpdateLicencia = function (serviceToCall) {
                var inicio = $rootScope.parseStringDate($scope.currentlicencia.fecha_inicio);
                var fin = $rootScope.parseStringDate($scope.currentlicencia.fecha_fin);
                if (inicio > fin) {
                    $rootScope.showError('La fecha de inicio debe preceder a la de fin');
                    return;
                }
                $scope.getCantidadDisponible(function (result) {
                    var fechaInicio = $scope.stringToDate($scope.currentlicencia.fecha_inicio);
                    var fechaFin = $scope.stringToDate($scope.currentlicencia.fecha_fin);
                    var days = $scope.cantidadDiasHabiles(fechaInicio, fechaFin);
                    var disponible = result;
                    if (days > disponible) {
                        if (disponible > 0) {
                            $rootScope.showError('El límite actual para este tipo de licencias es de ' + disponible + ' días');
                        } else {
                            $rootScope.showError('No tiene días disponibles para este tipo de licencias.');
                        }
                        return;
                    }

                    $scope.currentlicencia.fecha_inicio = inicio.toISOString();
                    $scope.currentlicencia.fecha_fin = fin.toISOString();

                    usSpinnerService.spin('spinner-1');
                    serviceToCall($scope.currentlicencia, function (response) {
                        //console.debug(angular.toJson(response));
                        $scope.refresh();
                        usSpinnerService.stop('spinner-1');
                        $scope.currentlicencia = null;
                    }, $rootScope.showErrorResponse);
                });
            };

            $scope.borrarLicencia = function () {
                usSpinnerService.spin('spinner-1');
                LicenciasService.borrarLicencia(
                    $scope.currentlicencia,
                    function (response) {
                        $scope.refresh();
                        usSpinnerService.stop('spinner-1');
                        $scope.currentlicencia = null;
                    },
                    $rootScope.showErrorResponse
                );
            };

            $scope.elegirEmpleado = function (area) {
                var modalInstance = $modal.open({
                    backdrop: MODALES_BACKDROP,
                    templateUrl: "views/licencia/licencia-pick-user.html",
                    controller: 'LicenciasPickUsrCtrl',
                    windowClass: 'dialog-modal'
                });

                modalInstance.result.then(
                    function (result) {
                        if (result) {
                            $scope.empleado = result;
                        }
                    },
                    $rootScope.showErrorResponse
                );
            };

            $scope.doUpload = function (theFile) {
                LicenciasService.uploadFile(
                    theFile,
                    function (response) {
                        $scope.currentlicencia.adjunto_nombre = response.fileName;
                        $scope.currentlicencia.adjunto_id = response.guid;
                        $scope.currentlicencia.adjunto_estado = "pendiente";
                        if ($scope.currentlicencia.id > 0) {
                            $scope.modificarLicencia();
                        }
                        $scope.fileChange = true;
                        console.log('uploaded');
                        $rootScope.showInfo("Adjunto subido a la licencia");
                    },
                    $rootScope.mostrarError);
            };

            $scope.doClear = function () {
                $scope.currentlicencia.adjunto_nombre = null;
                $scope.currentlicencia.adjunto_id = null;
                $scope.currentlicencia.adjunto_estado = null;
            };

            $scope.doDownload = function () {
                LicenciasService.downloadFile($scope.currentlicencia.id);
            };

            $scope.stringToDate = function (strDate) {
                var parts = strDate.split('/');
                var date = new Date(parts[2], parts[1] - 1, parts[0]);
                return date;
            }

            $scope.cantidadDiasHabiles = function (fechaInicio, fechaFin) {
                var cantidad = Math.abs((fechaFin - fechaInicio) / (1000 * 60 * 60 * 24)) + 1;

                $scope.feriados.forEach(function (feriado) {
                    if (feriado.fecha >= fechaInicio && feriado.fecha <= fechaFin
                        && feriado.fecha.getDate() != 0 && feriado.fecha.getDate() != 6) {
                        cantidad -= 1;
                    }
                });
                for (var fecha = fechaInicio; fecha <= fechaFin; fecha.setDate(fecha.getDate() + 1)) {
                    if (fecha.getDay() == 0 || fecha.getDay() == 6) {
                        cantidad -= 1;
                    }
                }
                return cantidad;
            }
        }
    ]);



//FILTERS
app.filter('licenciaDisponible', function () {
    return function (arr) {
        return arr.filter(function (licencia) {
            return licencia && licencia.cantidadDisponible > 0;
        });
    };
});



angular.module('app')
    .controller('LicenciasPickUsrCtrl', ['$scope', '$rootScope', 'LicenciasService', 'usSpinnerService', '$modal', '$modalInstance',
        function ($scope, $rootScope, LicenciasService, usSpinnerService, $modal, $modalInstance) {
            $scope.resultados = [];
            $scope.term = "";

            $scope.init = function () { }

            $scope.buscarEmpleado = function () {
                if ($scope.term.length < 1) return;
                $scope.resultados = [];

                LicenciasService.buscarEmpleados(
                    $scope.term,
                    function (response) {
                        if (response) {
                            for (var x = 0; x < response.length; x++) {
                                if (response[x].activo)
                                    $scope.resultados.push(response[x]);
                            }
                        }
                    },
                    $rootScope.showErrorResponse
                );
            }

            $scope.elegir = function (empleado) {
                $modalInstance.close(empleado);
            }

            $scope.close = function () {
                $modalInstance.close();
            };
        }
    ]);