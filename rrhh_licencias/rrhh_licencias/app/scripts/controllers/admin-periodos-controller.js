'use strict';

/**
 * @ngdoc function
 * @name app.controller:AdministracionCtrl
 * @description
 * # AdministracionCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('AdminPeriodosCtrl', ['$scope', '$rootScope', 'AdminPeriodosService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'usSpinnerService', '$modal',
  	function ($scope, $rootScope, AdminPeriodosService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, usSpinnerService, $modal) {
    	
        $scope.paginationRequest = {
          term: '',
          orderBy: 'fecha_solicitud',
          orderDirection: 'asc',
          page: 0,
          pageSize: '10'
        };
        $scope.paginationResponse = {};
        $scope.paginationPages = [];

        $scope.setOrder = function(field) {
          $scope.paginationRequest.orderBy = field;

          if ($scope.paginationRequest.orderDirection =='asc') {
            $scope.paginationRequest.orderDirection = 'desc';
          } else {
            $scope.paginationRequest.orderDirection = 'asc';
          }
          $scope.loadPage();
        }

      	$scope.init = function() {
      		$scope.loadPage();
      	}

        $scope.loadPage = function(page) {
          if (page || page === 0) $scope.paginationRequest.page = page; 

          usSpinnerService.spin('spinner-1');
          AdminPeriodosService.getPeriodosPaginated(
            $scope.paginationRequest,
            function (response) {
              $scope.paginationResponse = response;

              $scope.paginationPages = $scope.getPageRanges($scope.paginationResponse);

              usSpinnerService.stop('spinner-1');
            }, 
            $rootScope.showErrorResponse
          );
        }

        $scope.editar = function (periodo) {
        	var modalInstance = $modal.open({
	        	backdrop : MODALES_BACKDROP,
	        	templateUrl: "views/admin/admin-periodo-editar.html",
	        	controller: 'AdminPeriodoEditarCtrl',
	        	windowClass: 'dialog-modal',
	        	resolve: {
	          		periodo: function() {
	          			var p = angular.copy(periodo);
	          			if (p) {
		          			var inicio = new Date(p.fechaInicio.replace(/-/g, '\/').replace(/T.+/, ''));
							p.fechaInicio = inicio;
							var fin = new Date(p.fechaFin.replace(/-/g, '\/').replace(/T.+/, ''));
							p.fechaFin = fin;
						} else {
							p = {};
						}
	          			return p;
	          		}
	        	}
	    	});
        	modalInstance.result.then(
        		function(result) {
        			if (result) {
  	        			$scope.init();
  	        		}
  	        	}, 
	            $rootScope.showErrorResponse
          	);
        }

    	$scope.borrar = function (periodo) {
    		usSpinnerService.spin('spinner-1');
			AdminPeriodosService.borrarPeriodo(
    			periodo,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$scope.init();
    			},
				$rootScope.showErrorResponse
    		);
    	}
	}
]);

angular.module('app')
  .controller('AdminPeriodoEditarCtrl', ['$scope', '$rootScope', 'AdminPeriodosService', 'usSpinnerService', '$modal', '$modalInstance', 'periodo',
  	function ($scope, $rootScope, AdminPeriodosService, usSpinnerService, $modal, $modalInstance, periodo) {
    	$scope.periodo = periodo;
    	$scope.currentFeriado = {periodo_id: periodo.id};
    	
    	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

    	$scope.init = function () {
    		
    	}

    	$scope.guardar = function() {
    		usSpinnerService.spin('spinner-1');
			AdminPeriodosService.savePeriodo(
    			$scope.periodo,
    			function (response) {
    				usSpinnerService.stop('spinner-1');
    				$modalInstance.close(response);
    			},
				$rootScope.showErrorResponse
    		);
    	}

    	$scope.close = function() {
          	$modalInstance.close();
        };
	}
]);