'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciasCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('HomeCtrl', ['$scope', '$rootScope', '$modal', 'HomeService', 
    function ($scope, $rootScope, $modal, HomeService) {
    
    $scope.baseAppUrl = baseAppUrl;
    $scope.currentPeriodo = null;
    $scope.nextPeriodo = null;

    $scope.initHome = function() {
    	HomeService.getCurrentPeriodo(function (response) {
    		$scope.currentPeriodo = response;
    	}, $rootScope.showErrorResponse);

    	HomeService.getNextPeriodo(function (response) {
    		$scope.nextPeriodo = response;
    	}, $rootScope.showErrorResponse);
    };

    $scope.delegar = function() {
      var modalInstance = $modal.open({
          backdrop : MODALES_BACKDROP,
          templateUrl: "views/delegar-dialog.html",
          controller: 'DelegarCtrl',
          windowClass: 'dialog-modal',
          resolve: {
              empleado: function() {
                return $rootScope.user.empleado_id;
              }
          }
      });
      modalInstance.result.then(
        function(result) {
          if (result) {
            }
          }, 
          $rootScope.showErrorResponse
        );
    }

    $scope.novedades = function() {
      var modalInstance = $modal.open({
          backdrop : MODALES_BACKDROP,
          templateUrl: "views/admin/admin-novedades.html",
          controller: 'NovedadesCtrl',
          windowClass: 'dialog-modal'
      });
      modalInstance.result.then(
        function(result) {
            if (result) {
            }
          }, 
          $rootScope.showErrorResponse
        );
    }

        $scope.getNombre = function () {
            var iniciales = $rootScope.user.nombre.split(" ").map(n => `${n[0]}. `);
            return `${iniciales.join("")}${$rootScope.user.apellido}`;
        }
    
  }]);

