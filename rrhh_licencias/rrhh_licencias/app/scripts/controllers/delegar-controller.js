'use strict';

angular.module('app')
  	.controller('DelegarCtrl', ['$scope', '$rootScope', '$modal', 'AdminLicenciasService', 'usSpinnerService', '$modalInstance', 'empleado', 
  		function ($scope, $rootScope, $modal, AdminLicenciasService, usSpinnerService, $modalInstance, empleado_id) {
	  		$scope.performed = false;

	  		$scope.empleado_id = empleado_id;

	  		$scope.search = '';
	  		$scope.resultados = [];
	  		$scope.jefe = {};
	  		$scope.delegado = {};
    		$scope.wasDelegado = false;

	  		$scope.init= function() {
	  			AdminLicenciasService.getEmpleado(
	  				$scope.empleado_id,
	  				function (response) {
	  					$scope.jefe = response;

	  					if ($scope.jefe.delegado_id) {
	  						$scope.setCurrentDelegado();
	  					}

	  				},
	  				$rootScope.showErrorResponse
	  			);
	  		}

	  		$scope.setCurrentDelegado = function() {
	  			AdminLicenciasService.getEmpleado(
	  				$scope.jefe.delegado_id,
	  				function (response) {
	  					//console.debug(response);
	  					$scope.delegado = response;
						$scope.wasDelegado = true;
	  				},
	  				$rootScope.showErrorResponse
	  			);
	  		}

	  		$scope.elegirJefe = function() {
	  			$scope.elegirEmpleado(function (response) {
	  				$scope.wasDelegado = false;
	  				$scope.jefe = response;
	  				if ($scope.jefe.delegado_id) {
  						$scope.setCurrentDelegado();
  					}
	  			});
	  		}

	  		$scope.elegirDelegado = function() {
	  			$scope.elegirEmpleado(function (response) {
	  				$scope.delegado = response;
	  			});
	  		}

	  		$scope.elegirEmpleado = function (callback) {
		      var modalInstance = $modal.open({
		        backdrop : MODALES_BACKDROP,
		        templateUrl: "views/licencia/licencia-pick-user.html",
		        controller: 'LicenciasPickUsrCtrl',
		        windowClass: 'dialog-modal'
		      });

		      modalInstance.result.then(
		        function(result) {
		          if (result) {
		              callback(result);
		          }
		        }, 
		        $rootScope.showErrorResponse
		      );
		    }

	  		$scope.delegar = function (empleado) {
	  			AdminLicenciasService.delegarEmpleado(
  					$scope.delegado.id,
  					$scope.jefe.id,
  					function (response) {
  						$modalInstance.close(true);
  					},
  					$rootScope.showErrorResponse
  				);
	  		}

	  		$scope.deshacer = function (empleado) {
	  			AdminLicenciasService.desDelegarEmpleado(
  					$scope.delegado.id,
  					function (response) {
  						$modalInstance.close(true);
  					},
  					$rootScope.showErrorResponse
  				);
	  		}

	  		$scope.close = function() {
	          	$modalInstance.close();
	        };

	  	}
	]
);
