'use strict';

/**
 * @ngdoc function
 * @name app.controller:LicenciasCtrl
 * @description
 * # LicenciasCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('LoginCtrl', ['$scope', '$rootScope', '$state', '$http', '$cookies', 'LoginService', 'usSpinnerService', '$modal',
        function ($scope, $rootScope, $state, $http, $cookies, LoginService, usSpinnerService, $modal) {

            $scope.username = "";
            $scope.password = "";

            $scope.login = function () {
                if ($scope.username !== "" && $scope.password !== "") {
                    let user = {
                        usuario: $scope.username,
                        password: $scope.password
                    };
                    usSpinnerService.spin('spinner-1');
                    LoginService.login(
                        user,
                        function (response) {
                            let token = response.token;
                            console.debug(response);
                            let username = response.usuario;
                            LoginService.getPerfil(username, function (response) {
                                console.debug(angular.toJson(response));
                                $rootScope.user = response;
                                $rootScope.user.token = token;
                                $http.defaults.headers.common['Authorization'] = token;
                                let usuario = angular.toJson(response);
                                $cookies.put('user', usuario);
                                usSpinnerService.stop('spinner-1');
                                $state.go("app.licencias");
                            }, $rootScope.showErrorResponse);
                        }, $rootScope.showErrorResponse);
                } else {
                    $rootScope.showError("El usuario y la contrase&ntilde;a no pueden estar vac&iacute;os");
                }
            }
        }
    ]);
// LoginCtrl end