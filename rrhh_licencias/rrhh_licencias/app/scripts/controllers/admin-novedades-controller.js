'use strict';

angular.module('app')
  	.controller('NovedadesCtrl', ['$scope', '$rootScope', '$modal', 'AdminLicenciasService', 'usSpinnerService', '$modalInstance', 
  		function ($scope, $rootScope, $modal, AdminLicenciasService, usSpinnerService, $modalInstance) {
			
			var date = new Date();
			$scope.dateFrom = new Date(date.getFullYear(), date.getMonth() - 1, 1).toISOString().slice(0, 10);
			$scope.dateTo =  new Date(date.getFullYear(), date.getMonth(), 1).toISOString().slice(0, 10);
			$scope.reProcess = "0";

			$scope.init= function() {
				
			}

			$scope.close = function() {
				$modalInstance.close();
			};

			$scope.descargar = function() {
				var url = baseAppUrl + "/Export/ExportNovedades?reProcess=" + $scope.reProcess + "&dateFrom=" + $scope.dateFrom + "&dateTo=" + $scope.dateTo;
				window.location.href = url;
				$modalInstance.close();
			}

	  	}
	]
);
