'use strict';


/**
 * Config for the router
 */
angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams', '$templateCache', '$location', '$cookies', '$http', '$modal', 'usSpinnerService', 'LoginService',
            function ($rootScope, $state, $stateParams, $templateCache, $location, $cookies, $http, $modal, usSpinnerService, LoginService) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;

                $rootScope.$on('$routeChangeStart', function (event, next, current) {
                    if (typeof (current) !== 'undefined') {
                        $templateCache.remove(current.templateUrl);
                    }
                });

                /**
                 * Define el funciones para conversión de fechas UTC a angular dataFilter
                 */
                $rootScope.toUTCDate = function (date) {
                    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                    return _utc;
                };

                $rootScope.parseStringDate = function (strDate) {
                    var parts = strDate.split('/');
                    var date = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0]));
                    return date;
                }

                /**
                 * Define el funciones para conversión de fechas UTC a angular dataFilter
                 */
                $rootScope.millisToUTCDate = function (millis) {
                    return toUTCDate(new Date(millis));
                };

                $rootScope.showErrorResponse = function (response) {
                    if (response.shutup) return;
                    usSpinnerService.stop('spinner-1');

                    if (angular.isArray(response))
                        response = response[0];

                    var message = response.data && response.data.message
                        ? response.data.message
                        : response.status === undefined
                            ? response.message
                            : response.status.toString();

                    $rootScope.showError(message);
                };

                $rootScope.showError = function (message, validations) {
                    var ModalInstanceCtrl = function ($scope, $modalInstance) {
                        $scope.close = function () {
                            $modalInstance.close();
                        };
                    };

                    $rootScope.errorMessage = message;
                    var validationText = "";
                    var index = 0;
                    for (index in validations) {
                        validationText += index + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>";
                    }
                    $rootScope.errorValidations = validationText;

                    if ($rootScope.errorMessage === "") $rootScope.errorMessage = 'Error inesperado!';

                    var modalInstance = $modal.open({
                        backdrop: MODALES_BACKDROP,
                        templateUrl: "views/common/dialog-modal-error.html",
                        controller: ModalInstanceCtrl,
                        windowClass: 'dialog-modal'
                    });
                };

                /**
                 * Define el ShowInfo para toda la aplicacion
                 */
                $rootScope.showInfo = function (i18n) {
                    var ModalInstanceCtrl = function ($scope, $modalInstance) {
                        $scope.close = function () {
                            $modalInstance.close();
                        };
                    };

                    $rootScope.infoMessage = i18n;
                    if ($rootScope.infoMessage == "") $rootScope.infoMessage = 'Aviso default';

                    var modalInstance = $modal.open({
                        backdrop: MODALES_BACKDROP,
                        templateUrl: "views/common/dialog-modal-info.html",
                        controller: ModalInstanceCtrl,
                        windowClass: 'dialog-modal'
                    });
                };

                /**
                 * Define el ShowConfirm para toda la aplicacion
                 */
                $rootScope.showConfirm = function (scope, i18n) {
                    var ModalInstanceCtrl = function ($rootScope, $modalInstance) {
                        $rootScope.ok = function () {
                            $modalInstance.close();
                        };

                        $rootScope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    };

                    $rootScope.confirmMessage = i18n;
                    if ($rootScope.confirmMessage == "") $rootScope.confirmMessage = 'Info default';

                    var modalInstance = $modal.open({
                        backdrop: MODALES_BACKDROP,
                        //keyboard: MODALES_KEYBOARD,
                        templateUrl: "views/common/dialog-modal-confirm.html",
                        controller: ModalInstanceCtrl,
                        windowClass: 'dialog-modal'
                    });

                    modalInstance.result.then(function () {
                            scope.confirmedClick();
                        },
                        function () {
                            if (scope.cancelClick) {
                                scope.cancelClick();
                            }
                        }
                    );
                };

                $rootScope.getPageRanges = function (paginationResponse) {
                    var page = paginationResponse.page;
                    var total = paginationResponse.pagesCount;

                    var totalRange = 5;
                    var partialRange = 3;

                    var pages = [];
                    if (total <= totalRange) {
                        for (var i = 0; i < total; i++) {
                            pages.push({k: i + 1, v: i});
                        }
                    } else {
                        if (page < partialRange - 1) {
                            for (var i = 0; i < totalRange - 2; i++) {
                                pages.push({k: i + 1, v: i});
                            }
                            pages.push({k: '...', v: -1});
                            pages.push({k: total, v: total - 1});
                        } else if (total - page < partialRange) {
                            pages.push({k: 1, v: 0});
                            pages.push({k: '...', v: -1});
                            for (var i = total - partialRange; i < total; i++) {
                                pages.push({k: i + 1, v: i});
                            }
                        } else {
                            pages = [
                                {k: '1', v: 0},
                                {k: '...', v: -1},
                                {k: page, v: page - 1},
                                {k: page + 1, v: page},
                                {k: page + 2, v: page + 1},
                                {k: '...', v: -2},
                                {k: total, v: total - 1}
                            ];
                        }
                    }
                    return pages;
                }

                /* Reset error when a new view is loaded */
                $rootScope.$on('$viewContentLoaded', function () {
                    delete $rootScope.error;
                });

                $rootScope.hasPermiso = function (permiso) {

                    if ($rootScope.user === undefined) {
                        return false;
                    }

                    if ($rootScope.user.permisos === undefined) {
                        return false;
                    }

                    return $.inArray(permiso, $rootScope.user.permisos) >= 0;
                };

                $rootScope.hasRol = function (rol) {

                    if ($rootScope.user === undefined) {
                        return false;
                    }

                    if ($rootScope.user.roles === undefined) {
                        return false;
                    }

                    return $.inArray(rol, $rootScope.user.roles) >= 0;
                };

                $rootScope.logout = function () {
                    delete $rootScope.user;
                    delete $http.defaults.headers.common['Authorization'];
                    $cookies.remove('user');

                    //$cookies.remove('JSESSIONID');
                    $location.path("/login");
                };

                /* Try getting valid user from cookie or go to login page */
                var originalPath = $location.path();
                var user = $cookies.get('user');
                if (user !== undefined) {
                    $rootScope.user = angular.fromJson(user);
                    $http.defaults.headers.common['Authorization'] = $rootScope.user.token;
                    $location.path(originalPath);
                } else {
                    $location.path("/login");
                }

                //manejo de cambios de pagina. Solo puede ir a los estados protegidos si esta logeado.
                //esto evita que pueda usar el history o el back button del browser luego de deslogueado
                $rootScope.$on('$routeChangeSuccess', function (scope, currView, prevView) {
                    if (!currView.access.isFree && !$rootScope.user) {
                        $location.path("/login");
                    }
                });

                //manejo de cambios de pagina. Solo puede ir a los estados protegidos si esta logeado.
                //esto evita que pueda usar el history o el back button del browser luego de deslogueado
                $rootScope.$on("$stateChangeStart",
                    function (event, toState, toParams, fromState, fromParams) {
                        if (!toState.access.isFree && !$rootScope.user) {
                            event.preventDefault();
                            //$state.go('appunsecure.login');
                            $state.go('app.licencias');
                        }
                    }
                );

                $rootScope.formatDate = "dd/MM/yyyy";
            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider', '$httpProvider',
            function ($stateProvider, $urlRouterProvider, $httpProvider) {


                $httpProvider.interceptors.push(function ($q, $rootScope) {
                    return {
                        'responseError': function (rejection) {
                            if (rejection.status && rejection.status == '401') {
                                $rootScope.logout();
                                return $q.reject({shutup: true});
                            }
                            return $q.reject(rejection);
                        }
                    };
                });

                $urlRouterProvider
                    //.otherwise('/login');
                    .otherwise('/licencias');
                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/',
                        controller: 'HomeCtrl',
                        templateUrl: 'views/main.html'
                    })
                    .state('appunsecure', {
                        abstract: true,
                        url: '/',
                        templateUrl: 'views/main.unsecured.html'
                    })
                    .state('appunsecure.login', {
                        url: 'login',
                        templateUrl: 'views/login.html',
                        controller: 'LoginCtrl',
                        access: {
                            isFree: true
                        }
                    })
                    .state('app.licencias', {
                        url: 'licencias',
                        templateUrl: 'views/licencias.html',
                        controller: 'LicenciasCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.adjuntos', {
                        url: 'adjuntos',
                        templateUrl: 'views/adjuntos.html',
                        controller: 'AdjuntosCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.aprobacion', {
                        url: 'aprobacion/:tipo',
                        templateUrl: 'views/aprobacion.html',
                        controller: 'AprobacionCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.historia', {
                        url: 'historia',
                        templateUrl: 'views/historia.html',
                        controller: 'HistoriaCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-licencias', {
                        url: 'admin-licencias',
                        templateUrl: 'views/admin-licencias.html',
                        controller: 'AdminLicenciasCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.licenciaEmpleado', {
                        url: 'licenciaEmpleado/:empleadoId',
                        templateUrl: 'views/licenciaEmpleado.html',
                        controller: 'LicenciaEmpleadoCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-empleados', {
                        url: 'admin-empleados',
                        templateUrl: 'views/admin-empleados.html',
                        controller: 'AdminEmpleadosCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-areas', {
                        url: 'admin-areas',
                        templateUrl: 'views/admin-areas.html',
                        controller: 'AdminAreasCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-licencias-hoy', {
                        url: 'admin-licencias-hoy',
                        templateUrl: 'views/admin-licencias-hoy.html',
                        controller: 'AdminLicenciasHoyCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-periodos', {
                        url: 'admin-periodos',
                        templateUrl: 'views/admin-periodos.html',
                        controller: 'AdminPeriodosCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-feriados', {
                        url: 'admin-feriados',
                        templateUrl: 'views/admin-feriados.html',
                        controller: 'AdminFeriadosCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-tipos-licencia', {
                        url: 'admin-tipos-licencia',
                        templateUrl: 'views/admin-tipos-licencia.html',
                        controller: 'AdminTipoLicenciasCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.admin-roles', {
                        url: 'admin-roles',
                        templateUrl: 'views/admin-roles.html',
                        controller: 'AdminRolesCtrl',
                        access: {
                            isFree: false
                        }
                    })
                    .state('app.resetPassword', {
                        url: 'resetPassword',
                        templateUrl: 'views/resetPassword.html',
                        controller: 'ResetPasswordCtrl',
                        access: {
                            isFree: false
                        }
                    }).state('app.admin-notificar-vencimiento', {
                        url: 'admin-notificar-vencimiento',
                        templateUrl: 'views/admin-notificar-vencimiento.html',
                        controller: 'AdminNotificarVencimientoCtrl',
                        access: {
                            isFree: false
                        }
                    });
            }
        ]
    );