// config

var app =  
angular.module('app')
  .config(
    [        '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide, $httpProvider) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

    }
  ]);

angular.module('app').run(function(DTDefaultOptions) {
    DTDefaultOptions.setLanguage({
        sUrl: baseUrl + 'lang/get'
    });
});

angular.module('app')
    .directive('onlyIntegers', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) return;
                ngModel.$parsers.unshift(function (inputValue) {
                    var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                    ngModel.$viewValue = digits;
                    ngModel.$render();
                    return digits;
                });
            }
        };
    })
    .directive('uploadFile', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            scope: { someCtrlFn: '&callbackFn' },
            link: function (scope, element, attrs) {
                var file_uploaded = $parse(attrs.uploadFile);

                element.bind('change', function () {
                    scope.$apply(function () {
                        file_uploaded.assign(scope, element[0].files[0]);
                        scope.someCtrlFn({ arg1: element[0].files[0] });
                        element.val(null);
                    });

                });
            }
        };
    }]);