'use strict';

/**
 * @ngdoc overview
 * @name licencias_rrhh_ui
 * @description
 * # licencias_rrhh_ui
 *
 * Main module of the application.
 */
angular
  .module('app', [
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.calendar',
    'angularSpinner',
    'datatables',
    'datatables.bootstrap'
  ]);

var MODALES_BACKDROP = 'static';
var MODALES_KEYBOARD = false;

/**
 *  Configura la varialble de Services 
 */
var baseAppUrl = '..';
var baseUrl = baseAppUrl + '/api/';
var licenciasServices = angular.module('licenciasServices', ['ngResource']);

/**
 *  Configura la varialble de Controllers 
 */
var licenciasControllers = angular.module('licenciasControllers', ['coneduServices']);
