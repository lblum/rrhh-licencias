'use strict';

app.factory('AdjuntosService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getLicenciasToApprobeAttachment: function(request, callbackOk, callbackFail) {
                var url = baseUrl + 'licencia/postforapprobalattachments';
            	return $resource(url, { }, {
	            	getLicenciasToApprobe: {
		                    method: 'POST'
		                }
		            }
		        ).getLicenciasToApprobe({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},
			
			approbeLicenciaAttachment: function(licencia, callbackOk, callbackFail) {
				//console.debug(angular.toJson(licencia));
				var url = baseUrl + 'licencia/postapprobeattachment';
            	return $resource(url, {}, {
	            	saveLicencia: {
		                    method: 'POST'
		                }
		            }
		        ).saveLicencia({}, angular.toJson(licencia))
				.$promise.then(callbackOk, callbackFail);
			},
			
            rejectLicenciaAttachment: function(licencia, callbackOk, callbackFail) {
				//console.debug(angular.toJson(licencia));
				var url = baseUrl + 'licencia/postrejectattachment';
            	return $resource(url, {}, {
	            	saveLicencia: {
		                    method: 'POST'
		                }
		            }
		        ).saveLicencia({}, angular.toJson(licencia))
				.$promise.then(callbackOk, callbackFail);
			},

			getJefesYDelegados: function(callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetJefesYDelegados';
				return $resource(url, {} )
					.query(callbackOk, callbackFail);
			},

		}
	}
]);