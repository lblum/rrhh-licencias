'use strict';

app.factory('AdminPeriodosService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getPeriodos: function(callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getPeriodosPaginated: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/PostPaginated';
            	return $resource(url, {}, {
	            	getPeriodosPaginated: {
		                    method: 'POST'
		                }
		            }
		        ).getPeriodosPaginated({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},
			
			getUltimooPeriodo: function(callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/GetLast';
            	return $resource(url, {} )
            		.get(callbackOk, callbackFail);
			},

			savePeriodo: function(periodo, callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/post';
            	return $resource(url, {}, {
		            	savePeriodo: {
		                    method: 'POST'
		                }
		            }
		        ).savePeriodo({}, angular.toJson(periodo))
				.$promise.then(callbackOk, callbackFail);
			},
			
			borrarPeriodo: function(periodo, callbackOk, callbackFail) {
				var url = baseUrl + 'periodos/GetBorrarPeriodo';
            	return $resource(url, {'id': periodo.id} )
            		.get(callbackOk, callbackFail);
			},

		}


	}
]);