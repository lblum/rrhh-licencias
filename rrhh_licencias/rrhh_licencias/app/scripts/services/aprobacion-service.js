'use strict';

app.factory('AprobacionService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getLicenciasToApprobeOld: function(tipo, callbackOk, callbackFail) {
				var url = baseUrl + 'licencia/getforapprobal';
            	return $resource(url, {'id': tipo} )
            		.query(callbackOk, callbackFail);
			},

			getLicenciasToApprobe: function(tipo, request, callbackOk, callbackFail) {
				var url = baseUrl + 'licencia/postforapprobal';
            	return $resource(url, {'id': tipo}, {
	            	getLicenciasToApprobe: {
		                    method: 'POST'
		                }
		            }
		        ).getLicenciasToApprobe({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},
			
			approbeLicencia: function(licencia, callbackOk, callbackFail) {
				//console.debug(angular.toJson(licencia));
				var url = baseUrl + 'licencia/postapprobe';
            	return $resource(url, {}, {
	            	saveLicencia: {
		                    method: 'POST'
		                }
		            }
		        ).saveLicencia({}, angular.toJson(licencia))
				.$promise.then(callbackOk, callbackFail);
			},
			
			rejectLicencia: function(licencia, callbackOk, callbackFail) {
				//console.debug(angular.toJson(licencia));
				var url = baseUrl + 'licencia/postreject';
            	return $resource(url, {}, {
	            	saveLicencia: {
		                    method: 'POST'
		                }
		            }
		        ).saveLicencia({}, angular.toJson(licencia))
				.$promise.then(callbackOk, callbackFail);
			},

			getJefesYDelegados: function(callbackOk, callbackFail) {
				var url = baseUrl + 'empleado/GetJefesYDelegados';
				return $resource(url, {} )
					.query(callbackOk, callbackFail);
			},

		}


	}
]);