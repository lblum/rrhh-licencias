'use strict';

app.factory('AdminLicenciasHoyService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

        return {

            getLicenciasHoy: function (date, request, callbackOk, callbackFail) {
                var url = baseUrl + 'licencia/postfordate';
                return $resource(url, {'date': date}, {
                    getLicenciasHoy: {
                        method: 'POST'
                    }
                }
                ).getLicenciasHoy({}, angular.toJson(request))
                    .$promise.then(callbackOk, callbackFail);
            },

        };


	}
]);