'use strict';

app.factory('AdminEmpleadosService', ['$resource', '$http', '$rootScope',
    function ($resource, $http, $rootScope) {

        return {

            getEmpleados: function (callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/get';
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            getEmpleadosByPrincipal: function (callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/GetByPrincipalRol';
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            getEmpleadosPaginadosByPrincipal: function (request, callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/PostByPrincipalRol';
                return $resource(url, {}, {
                    getEmpleadosPaginadosByPrincipal: {
                        method: 'POST'
                    }
                }
                ).getEmpleadosPaginadosByPrincipal({}, angular.toJson(request))
                    .$promise.then(callbackOk, callbackFail);
            },


            getAreas: function (callbackOk, callbackFail) {
                var url = baseUrl + 'areas/get';
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            getEmpresas: function (callbackOk, callbackFail) {
                var url = baseUrl + 'empresas/get';
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            getRoles: function (callbackOk, callbackFail) {
                var url = baseUrl + 'rol/get';
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            getEmpleadoUsuario: function (empleado, callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/getUsuario';
                return $resource(url, { 'id': empleado.id })
                    .get(callbackOk, callbackFail);
            },

            saveEmpleado: function (empleado, callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/post';
                return $resource(url, {}, {
                    saveEmpleado: {
                        method: 'POST'
                    }
                }
                ).saveEmpleado({}, angular.toJson(empleado))
                    .$promise.then(callbackOk, callbackFail);
            },

            saveUsuario: function (usuario, callbackOk, callbackFail) {
                var url = baseUrl + 'usuario/post';
                return $resource(url, {}, {
                    saveUsuario: {
                        method: 'POST'
                    }
                }
                ).saveUsuario({}, angular.toJson(usuario))
                    .$promise.then(callbackOk, callbackFail);
            },

            sendNotificacionVencimiento: function (callbackOk, callbackFail) {
                var url = baseUrl + 'licencia/getnotificarvencimiento';
                return $resource(url, {})
                    .get(callbackOk, callbackFail);
            },
        }
    }
]);