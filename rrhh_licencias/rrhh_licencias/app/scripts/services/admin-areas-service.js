'use strict';

app.factory('AdminAreasService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getAreas: function(callbackOk, callbackFail) {
				var url = baseUrl + 'areas/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getAreasPaginated: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'areas/PostPaginated';
            	return $resource(url, {}, {
	            	getAreasPaginated: {
		                    method: 'POST'
		                }
		            }
		        ).getAreasPaginated({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},
			
			getEmpleados: function(area, callbackOk, callbackFail) {
				var url = baseUrl + 'areas/GetEmpleadosByArea';
            	return $resource(url, {'id': area.id} )
            		.query(callbackOk, callbackFail);
			},
			
			saveArea: function(area, callbackOk, callbackFail) {
				var url = baseUrl + 'areas/post';
            	return $resource(url, {}, {
	            	saveArea: {
		                    method: 'POST'
		                }
		            }
		        ).saveArea({}, angular.toJson(area))
				.$promise.then(callbackOk, callbackFail);
			},
			
			borrarArea: function(area, callbackOk, callbackFail) {
				var url = baseUrl + 'areas/GetBorrarArea';
            	return $resource(url, {'id': area.id} )
            		.get(callbackOk, callbackFail);
			},

		}


	}
]);