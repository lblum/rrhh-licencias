'use strict';

app.factory('AdminFeriadosService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getFeriados: function(callbackOk, callbackFail) {
				var url = baseUrl + 'feriado/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getFeriadosPaginated: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'feriado/PostPaginated';
            	return $resource(url, {}, {
	            	getLicenciasHistory: {
		                    method: 'POST'
		                }
		            }
		        ).getLicenciasHistory({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},

			saveFeriado: function(feriado, callbackOk, callbackFail) {
				var url = baseUrl + 'feriado/post';
            	return $resource(url, {}, {
		            	saveFeriado: {
		                    method: 'POST'
		                }
		            }
		        ).saveFeriado({}, angular.toJson(feriado))
				.$promise.then(callbackOk, callbackFail);
			},
			
			borrarFeriado: function(feriado, callbackOk, callbackFail) {
				var url = baseUrl + 'feriado/GetDelete/:id';
            	return $resource(url, {'id': feriado.id}, {
		            	borrarFeriado: {
		                    method: 'GET'
		                }
		            }
		        ).borrarFeriado({}, angular.toJson(feriado))
				.$promise.then(callbackOk, callbackFail);
			},

		}


	}
]);