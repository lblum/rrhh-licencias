'use strict';

app.factory('LicenciasService', ['$resource', '$http', '$rootScope',
    function ($resource, $http, $rootScope) {

        return {

            getTiposLicenciaUsuario: function (empleado_id, periodo_id, callbackOk, callbackFail) {
                var url = baseUrl + 'tiposlicenciausuario/get/:empleado_id';

                //console.log("empleado_id" + empleado_id +  "periodoId" + periodo_id);
                return $resource(url, {"empleado_id": empleado_id, "periodoId": periodo_id})
                    .query(callbackOk, callbackFail);
            },

            getVacacionesHistorico: function (empleado_id, callbackOk, callbackFail) {
                var url = baseUrl + 'tiposlicenciausuario/getVacacionesHistorico/:empleado_id';

                //console.log("empleado_id" + empleado_id +  "periodoId" + periodo_id);
                return $resource(url, {"empleado_id": empleado_id})
                    .query(callbackOk, callbackFail);
            },

            getVacacionesPendientesFor: function (id, callbackOk, callbackFail) {
                var url = baseUrl + 'tiposlicenciausuario/GetVacacionesPendientes/:id';
                return $resource(url, { "id": id })
                .get(callbackOk, callbackFail);
            },

            getFeriados: function (callbackOk, callbackFail) {
                var url = baseUrl + 'feriado/get';

                //console.log("empleado_id" + empleado_id +  "periodoId" + periodo_id);
                return $resource(url, {})
                    .query(callbackOk, callbackFail);
            },

            saveLicencia: function (licencia, callbackOk, callbackFail) {
                //console.debug(angular.toJson(licencia));
                var url = baseUrl + 'licencia/post';
                return $resource(url, {}, {
                        saveLicencia: {
                            method: 'POST'
                        }
                    }
                ).saveLicencia({}, angular.toJson(licencia))
                    .$promise.then(callbackOk, callbackFail);
            },

            editarLicencia: function (licencia, callbackOk, callbackFail) {
                //console.debug(angular.toJson(licencia));
                var url = baseUrl + 'licencia/postedit';
                return $resource(url, {}, {
                        saveLicencia: {
                            method: 'POST'
                        }
                    }
                ).saveLicencia({}, angular.toJson(licencia))
                    .$promise.then(callbackOk, callbackFail);
            },

            borrarLicencia: function (licencia, callbackOk, callbackFail) {
                //console.debug(angular.toJson(licencia));
                var url = baseUrl + 'licencia/GetDelete';
                return $resource(url, {'id': licencia.id}, {
                        saveLicencia: {
                            method: 'GET'
                        }
                    }
                ).saveLicencia({}, {})
                    .$promise.then(callbackOk, callbackFail);
            },

            getPeriodoFor: function (date, callbackOk, callbackFail) {
                var url = baseUrl + 'periodos/getfor/:date';
                return $resource(url, {"date": date.toISOString().substring(0, 10)})
                    .get(callbackOk, callbackFail);
            },

            getCalendarEvents: function (empleado_id, start, end, callbackOk, callbackFail) {
                var url = baseUrl + 'licencia/getcalendar/:empleado_id';
                return $resource(url, {"empleado_id": empleado_id, "start": start, "end": end})
                    .query(callbackOk, callbackFail);
            },

            getEmpleado: function (empleado_id, callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/get';
                return $resource(url, {'id': empleado_id})
                    .get(callbackOk, callbackFail);
            },

            buscarEmpleados: function (term, callbackOk, callbackFail) {
                var url = baseUrl + 'empleado/GetByTerm';
                return $resource(url, {'id': term})
                    .query(callbackOk, callbackFail);
            },

            uploadFile: function (theFile, callbackOk, callbackFail) {
                var url = baseUrl + "licencia/PostAttachment";
                var data = new FormData();
                data.append("uploadedFile", theFile);
                return $resource(url, {}, {
                    uploadImageFile: {
                        method: 'POST',
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }
                }).uploadImageFile({}, data)
                    .$promise.then(callbackOk, callbackFail);
            },

            downloadFile: function (id, callbackOk, callbackFail) {
                var url = baseUrl + "licencia/GetAttachment?id=" + id;
                window.open(url, '_blank', '');
            },

        }

    }
]);