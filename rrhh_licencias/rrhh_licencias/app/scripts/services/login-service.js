'use strict';

app.factory('LoginService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			loginToken: function(user, callbackOk, callbackFail) {
				var url = baseUrl + 'token';
	            $http({
				        url: url,
				        method: 'POST',
				        data: "username=" + user.username + "&password=" + user.password + 
				              "&grant_type=password"
				}).success(callbackOk).error(callbackFail);
	        },
			
			login: function(user, callbackOk, callbackFail) {
				//console.debug(angular.toJson(licencia));
				var url = baseUrl + 'login/post';
            	return $resource(url, {}, {
	            	login: {
		                    method: 'POST'
		                }
		            }
		        ).login({}, angular.toJson(user))
				.$promise.then(callbackOk, callbackFail);
			},

	        getPerfil: function (login, callbackOk, callbackFail) {
	        	var url = baseUrl + 'permisos/get/:login';
        		return $resource(url, {"login":login}).get(
        			{},
        			callbackOk,
        			callbackFail
        		);
	        },

	        getPerfilDefault: function (callbackOk, callbackFail) {
	        	var url = baseUrl + 'permisos/get';
        		return $resource(url, {}).get(
        			{},
        			callbackOk,
        			callbackFail
        		);
	        },
		}


	}
]);