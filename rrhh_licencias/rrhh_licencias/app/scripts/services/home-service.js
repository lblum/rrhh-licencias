'use strict';

app.factory('HomeService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getCurrentPeriodo: function(ucallbackOk, callbackFail) {
				var url = baseUrl + 'periodos/getcurrent';
            	return $resource(url, {} )
            		.get(ucallbackOk, callbackFail);
			},
			
			getNextPeriodo: function(ucallbackOk, callbackFail) {
				var url = baseUrl + 'periodos/getnext';
            	return $resource(url, {} )
            		.get(ucallbackOk, callbackFail);
			}

		}


	}
]);