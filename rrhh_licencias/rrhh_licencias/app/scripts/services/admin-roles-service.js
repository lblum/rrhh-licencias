'use strict';

app.factory('AdminRolesService',  ['$resource', '$http', '$rootScope', 
	function( $resource, $http, $rootScope){

		return {
			
			getRoles: function(callbackOk, callbackFail) {
				var url = baseUrl + 'rol/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},

			getRolesPaginados: function(request, callbackOk, callbackFail) {
				var url = baseUrl + 'rol/PostPaginated';
            	return $resource(url, {}, {
	            	getLicenciasHistory: {
		                    method: 'POST'
		                }
		            }
		        ).getLicenciasHistory({}, angular.toJson(request))
				.$promise.then(callbackOk, callbackFail);
			},

			
			getPermisos: function(callbackOk, callbackFail) {
				var url = baseUrl + 'permiso/get';
            	return $resource(url, {} )
            		.query(callbackOk, callbackFail);
			},
			
			saveRol: function(rol, callbackOk, callbackFail) {
				var url = baseUrl + 'rol/post';
            	return $resource(url, {}, {
	            	saveRol: {
		                    method: 'POST'
		                }
		            }
		        ).saveRol({}, angular.toJson(rol))
				.$promise.then(callbackOk, callbackFail);
			},
			
			borrarRol: function(rol, callbackOk, callbackFail) {
				var url = baseUrl + 'rol/GetDelete';
            	return $resource(url, {'id': rol.id}, {
	            	borrarRol: {
		                    method: 'GET'
		                }
		            }
		        ).borrarRol({}, {})
				.$promise.then(callbackOk, callbackFail);
			},

		}


	}
]);