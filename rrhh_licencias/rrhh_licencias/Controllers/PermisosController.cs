﻿using System;
using System.Linq;
using System.Net.Http;
using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Controllers
{
    public class PermisosController : BaseApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PermisosController));

        public HttpResponseMessage Get()
        {
            var headers = Request.Headers;
            if (headers.Contains("Authorization"))
            {
                string token = headers.GetValues("Authorization").First();
            }
            string userName = CurrentUser;
            return Get(userName);
        }
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage response;
            try
            {
                usuario dbUser = db.usuario.FirstOrDefault(u => u.login.ToLower().Contains(id.ToLower()));

                if (dbUser == null || dbUser.empleado == null || dbUser.empleado.activo != 1)
                {
                    Exception e = new Exception("Usuario no encontrado");
                    return getErrorResponse(e);
                }
                UserCredentialsTO to = new UserCredentialsTO();
                to.login = dbUser.login;
                to.id = dbUser.id;
                if (dbUser.empleado != null)
                {
                    to.empleado_id = dbUser.empleado_id.HasValue ? dbUser.empleado_id.Value : 0;
                    to.nombre = dbUser.empleado.nombres;
                    to.apellido = dbUser.empleado.apellidos;
                    to.legajo = dbUser.empleado.legajo;
                }
                else
                {
                    to.nombre = id;
                    to.apellido = id;
                    to.legajo = "n/a";
                }

                foreach (usuario_rol ur in dbUser.usuario_rol)
                {
                    if (!to.roles.Contains(ur.rol.nombre))
                    {
                        to.roles.Add(ur.rol.nombre);
                        foreach (rol_permiso rp in ur.rol.rol_permiso)
                        {
                            if (!to.permisos.Contains(rp.permiso.nombre))
                            {
                                to.permisos.Add(rp.permiso.nombre);
                            }
                        }
                    }
                }

                if (dbUser.empleado.delegado_de != null)
                {
                    foreach (empleado padre in dbUser.empleado.delegado_de)
                    {
                        usuario padreUsuario = padre.usuario.FirstOrDefault();
                        if (padreUsuario != null)
                        {
                            foreach (usuario_rol ur in padreUsuario.usuario_rol)
                            {
                                if (!to.roles.Contains(ur.rol.nombre))
                                {
                                    to.roles.Add(ur.rol.nombre);
                                    foreach (rol_permiso rp in ur.rol.rol_permiso)
                                    {
                                        if (!to.permisos.Contains(rp.permiso.nombre))
                                        {
                                            to.permisos.Add(rp.permiso.nombre);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                response = getSuccessResponse(to);
            }
            catch (Exception e)
            {
                response = getErrorResponse(e);
            }
            return response;
        }
    }
}