﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using rrhh_licencias.Exceptions;

namespace rrhh_licencias.Controllers
{
    public class LoginController : BaseApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LoginController));

        [AllowAnonymous]
        public HttpResponseMessage Post(LoginTO login)
        {
            HttpResponseMessage response;
            try
            {
                LoginService service = (LoginService)new LoginService().setDatabase(db);
                usuario user = service.loginBasic(login);
                if (user.password == login.password)
                {
                    string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(user.login + ":" + user.password));

                    login.token = "Basic " + base64;
                    login.userId = user.id;
                    response = this.getSuccessResponse(login);
                }
                else
                {
                    throw new WrongUserOrPasswordException();
                }
            }
            catch (Exception e)
            {
                response = this.getErrorResponse(e);
            }
            return response;
        }
        public HttpResponseMessage Delete()
        {
            HttpResponseMessage response;
            /*try
			{
				HttpContext.Current.Session.Abandon();
				HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
				LoginService service = (LoginService)new LoginService().setDatabase(db);
				service.logout();*/
            response = this.getSuccessResponse(new { status = "OK" });
            /*}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}*/
            return response;
        }
    }
}