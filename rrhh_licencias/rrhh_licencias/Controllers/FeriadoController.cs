﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class FeriadoController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(FeriadoController));

		// GET api/feriado
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				FeriadoService service = (FeriadoService)new FeriadoService().setDatabase(db);
				List<feriado> feriados = service.getAll();

				response = this.getSuccessResponse(feriados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/feriado
		public HttpResponseMessage PostPaginated(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				FeriadoService service = (FeriadoService)new FeriadoService().setDatabase(db);
				PaginationResponseTO<feriado> feriados = service.getAll(request);

				response = this.getSuccessResponse(feriados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/feriado/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				FeriadoService service = (FeriadoService)new FeriadoService().setDatabase(db);
				feriado f = service.find(id);

				response = this.getSuccessResponse(f);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/feriado
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] feriado f)
		{
			HttpResponseMessage response;
			try
			{
				FeriadoService service = (FeriadoService)new FeriadoService().setDatabase(db);
				f = service.saveOrUpdate(f);
                LicenciaService lService = (LicenciaService)new LicenciaService().setDatabase(db);
                lService.updateLicenciasConFecha(f.fecha);
				response = this.getSuccessResponse(f);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

        // DELETE api/feriado/5
        //[ApiAuthenticationFilter(true)]
        public HttpResponseMessage GetDelete(long id)
		{
			HttpResponseMessage response;
			try
			{
				FeriadoService service = (FeriadoService)new FeriadoService().setDatabase(db);
				feriado f = service.find(id);
				service.delete(id);
                LicenciaService lService = (LicenciaService)new LicenciaService().setDatabase(db);
                lService.updateLicenciasConFecha(f.fecha);
                response = this.getSuccessResponse(f);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}