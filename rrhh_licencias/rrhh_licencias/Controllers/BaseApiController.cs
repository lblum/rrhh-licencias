﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class BaseApiController : ApiController
	{
		protected holando_rrhh_licenciasEntities db = new holando_rrhh_licenciasEntities();
        private static readonly ILog log = LogManager.GetLogger(typeof(BaseApiController));
        protected HttpResponseMessage getSuccessResponse(Object data)
		{
            log.Info(data);
			return Request.CreateResponse(HttpStatusCode.OK, data);
		}

		protected HttpResponseMessage getErrorResponse(Exception e)
		{
            log.Error(e);
            string simplifiedExceptions = ConfigurationManager.AppSettings["SimplifiedExceptions"];
			if (simplifiedExceptions == "true")
			{
				SimplifiedExceptionTO to = new SimplifiedExceptionTO();
				to.Message = e.Message;
				to.ClassName = e.GetType().FullName;
				return Request.CreateResponse(HttpStatusCode.InternalServerError, to);
			}
			else
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
			}
		}
        protected string CurrentUser
        {
            get
            {
                var request = HttpContext.Current.Request;
                var authHeader = request.Headers["Authorization"];
                var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
                var encoding = Encoding.GetEncoding("iso-8859-1");
                var credentials = encoding.GetString(Convert.FromBase64String(authHeaderVal.Parameter));
                int separator = credentials.IndexOf(':');
                string name = credentials.Substring(0, separator);
                return name;
            }
        }
        
	}
}