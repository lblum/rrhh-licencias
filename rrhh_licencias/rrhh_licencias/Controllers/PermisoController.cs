﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class PermisoController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PermisoController));

		// GET api/permiso
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				PermisoService service = (PermisoService)new PermisoService().setDatabase(db);
				List<permiso> permisos = service.getAll();

				response = this.getSuccessResponse(permisos);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/permiso/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				PermisoService service = (PermisoService)new PermisoService().setDatabase(db);
				permiso p = service.find(id);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/permiso
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] permiso p)
		{
			HttpResponseMessage response;
			try
			{
				PermisoService service = (PermisoService)new PermisoService().setDatabase(db);
				p = service.saveOrUpdate(p);

				response = this.getSuccessResponse(p);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/permiso/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				PermisoService service = (PermisoService)new PermisoService().setDatabase(db);
				permiso p = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}