﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class CondicionEspecialController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(CondicionEspecialController));

		// GET api/condicionespecial
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				CondicionEspecialService service = (CondicionEspecialService)new CondicionEspecialService().setDatabase(db);
				List<condicion_especial> condiciones_especiales = service.getAll();

				response = this.getSuccessResponse(condiciones_especiales);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/condicionespecial/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				CondicionEspecialService service = (CondicionEspecialService)new CondicionEspecialService().setDatabase(db);
				condicion_especial ce = service.find(id);

				response = this.getSuccessResponse(ce);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/condicionespecial
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] condicion_especial ce)
		{
			HttpResponseMessage response;
			try
			{
				CondicionEspecialService service = (CondicionEspecialService)new CondicionEspecialService().setDatabase(db);
				ce = service.saveOrUpdate(ce);

				response = this.getSuccessResponse(ce);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/condicionespecial/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				CondicionEspecialService service = (CondicionEspecialService)new CondicionEspecialService().setDatabase(db);
				condicion_especial ce = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(ce);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}