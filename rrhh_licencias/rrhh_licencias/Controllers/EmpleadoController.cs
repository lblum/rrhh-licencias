﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class EmpleadoController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(EmpleadoController));

		// GET api/empleado
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				List<empleado> empleados = service.getAll();

				response = this.getSuccessResponse(empleados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado
		public HttpResponseMessage GetJefesYDelegados()
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				List<empleado> empleados = service.GetJefesYDelegados();

				response = this.getSuccessResponse(empleados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetByPrincipalRol()
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				List<empleado> empleados = service.getByPrincipalRol(CurrentUser);

				response = this.getSuccessResponse(empleados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage PostByPrincipalRol(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				PaginationResponseTO<empleado> empleados = service.getPagesByPrincipalRol(request, CurrentUser);

				response = this.getSuccessResponse(empleados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				empleado e = service.find(id);

				response = this.getSuccessResponse(e);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetDelegar(long id, long jefeId)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				empleado jefe = service.delegar(id, jefeId);

				response = this.getSuccessResponse(jefe);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetDeshacerDelegar(long id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				empleado jefe = service.desDelegar(id);

				response = this.getSuccessResponse(jefe);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/empleado/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetUsuario(long id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				empleado e = service.find(id);

				response = this.getSuccessResponse(e.usuario != null ? e.usuario.FirstOrDefault() : null);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetByTerm(String id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				List<empleado> emps = service.findByTerm(id);

				response = this.getSuccessResponse(emps);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/empleado
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] empleado e)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				AreaService areaService = (AreaService)new AreaService().setDatabase(db);
                EmpresaService empresaService = (EmpresaService)new EmpresaService().setDatabase(db);


                area a = areaService.find(e.area.id);
				e.area = a;
				e.area_id = a.id;

                empresa emp = empresaService.find(e.empresa.id);
                e.empresa = emp;
                e.empresa_id = emp.id;

				if (e.jefe != null)
				{
					e.jefe_id = e.jefe.id;
				}
				else
				{
					e.jefe_id = null;
				}
				e.jefe = null;

				e = service.saveOrUpdate(e);

				response = this.getSuccessResponse(e);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/empleado/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				empleado e = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(e);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}