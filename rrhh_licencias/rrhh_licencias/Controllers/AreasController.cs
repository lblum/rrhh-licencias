﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class AreasController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(AreasController));

		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				AreaService service = (AreaService)new AreaService().setDatabase(db);
				List<area> areas = service.getAll();

				response = this.getSuccessResponse(areas);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage PostPaginated(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				AreaService service = (AreaService)new AreaService().setDatabase(db);
				PaginationResponseTO<area> areas = service.getAll(request);

				response = this.getSuccessResponse(areas);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetBorrarArea(long id)
		{
			HttpResponseMessage response;
			try
			{
				AreaService service = (AreaService)new AreaService().setDatabase(db);
				area a = service.find(id);

				service.delete(id);

				response = this.getSuccessResponse(a);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetEmpleadosByArea(long id)
		{
			HttpResponseMessage response;
			try
			{
				EmpleadoService service = (EmpleadoService)new EmpleadoService().setDatabase(db);
				List<empleado> empleados = service.getByArea(id, true);

				response = this.getSuccessResponse(empleados);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] area a)
		{
			HttpResponseMessage response;
			try
			{
				AreaService service = (AreaService)new AreaService().setDatabase(db);
				a = service.saveOrUpdate(a);

				response = this.getSuccessResponse(a);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				AreaService service = (AreaService)new AreaService().setDatabase(db);
				area a = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(a);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}