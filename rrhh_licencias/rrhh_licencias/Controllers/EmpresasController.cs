﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace rrhh_licencias.Controllers
{
	public class EmpresasController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(EmpresasController));

		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
                EmpresaService service = (EmpresaService)new EmpresaService().setDatabase(db);
				List<empresa> empresas = service.getAll();

				response = this.getSuccessResponse(empresas);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}