﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class TiposLicenciaController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TiposLicenciaController));

		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				List<tipo_licencia> tiposlicencia = service.getAll();

				response = this.getSuccessResponse(tiposlicencia);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage PostPaginated(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				PaginationResponseTO<tipo_licencia> tiposlicencia = service.getAll(request);

				response = this.getSuccessResponse(tiposlicencia);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				tipo_licencia tl = service.find(id);

				response = this.getSuccessResponse(tl);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] tipo_licencia tl)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				tl = service.saveOrUpdate(tl);

				response = this.getSuccessResponse(tl);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetDelete(long id)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				tipo_licencia tl = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(tl);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}