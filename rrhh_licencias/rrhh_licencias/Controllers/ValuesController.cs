﻿using rrhh_licencias.Models;
using rrhh_licencias.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace rrhh_licencias.Controllers
{
	public class ValuesController : BaseApiController
	{
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaService service = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);

                string user = CurrentUser;// BaseService.getPrincipalName();
				List<tipo_licencia> tls = service.getAll();

				response = this.getSuccessResponse(new { user = user, test = tls });
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}