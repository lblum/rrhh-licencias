﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class RolController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(RolController));

		// GET api/rol
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				RolService service = (RolService)new RolService().setDatabase(db);
				List<rol> roles = service.getAll();

				response = this.getSuccessResponse(roles);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/rol
		public HttpResponseMessage PostPaginated(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				RolService service = (RolService)new RolService().setDatabase(db);
				PaginationResponseTO<rol> roles = service.getAll(request);

				response = this.getSuccessResponse(roles);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/rol/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				RolService service = (RolService)new RolService().setDatabase(db);
				rol r = service.find(id);

				response = this.getSuccessResponse(r);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/rol
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] rol r)
		{
			HttpResponseMessage response;
			try
			{
				RolService service = (RolService)new RolService().setDatabase(db);
				PermisoService permisoService = (PermisoService)new PermisoService().setDatabase(db);

				rol_permiso[] permisos = new rol_permiso[r.rol_permiso.Count()];
				r.rol_permiso.CopyTo(permisos, 0);
				r.rol_permiso = new List<rol_permiso>();

				r = service.saveOrUpdate(r);
				service.deletePermisos(r.id);

				foreach (rol_permiso rp in permisos)
				{
					permiso dbP = permisoService.find(rp.permiso.id);
					rp.id = 0;
					rp.rol_id = r.id;
					rp.permiso = dbP;

					service.saveOrUpdatePermiso(rp);
				}

				response = this.getSuccessResponse(r);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/rol/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetDelete(long id)
		{
			HttpResponseMessage response;
			try
			{
				RolService service = (RolService)new RolService().setDatabase(db);
				rol r = service.find(id);
				service.deletePermisos(r.id);
				service.delete(id);

				response = this.getSuccessResponse(r);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

	}
}