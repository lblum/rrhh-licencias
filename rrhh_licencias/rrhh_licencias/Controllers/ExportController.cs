﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using rrhh_licencias.Helper;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Controllers
{
    public class ExportController : Controller
	{

		protected holando_rrhh_licenciasEntities db = new holando_rrhh_licenciasEntities();
        //
        // GET: /Export/ExportNovedades

        public FileResult ExportNovedades(DateTime dateFrom, DateTime dateTo, int reProcess)
        {
            LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
            DateTime today = DateTime.Now;


            List<licencia> licencias = new List<licencia>();
            if (reProcess == 0)
            {
                licencias = db.licencia.Where(
                     l => l.estado == "aprobado"
                         //&& l.fecha_inicio <= dateTo.Date
                         //&& l.fecha_inicio >= dateFrom.Date
                         && l.fecha_exportacion == null
                 ).ToList();
            }
            else
            {
                licencias = db.licencia.Where(
                     l => l.estado == "aprobado"
                         && l.aprobador_rrhh_fecha <= dateTo.Date
                         && l.aprobador_rrhh_fecha >= dateFrom.Date
                 ).ToList();
            }

            var output = new MemoryStream();
            ExcelHelper.Novedades(output, licencias);
            output.Seek(0, SeekOrigin.Begin);

            foreach (licencia l in licencias)
            {
                l.fecha_exportacion = today;
                service.saveOrUpdate(l);
            }

            return File(output, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "novedades.xlsx");
        }

        public FileResult ExportLicenciasForDate(DateTime date, string term)
        {
            LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
            List<licencia> licencias = service.getForDate(date, term, null, null).ToList();
            MemoryStream output = ExcelHelper.LicenciasAlDia(licencias);
            output.Position = 0;
            return File(output, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Licencias al dia.xlsx");
        }

		public FileResult ExportSaldos()
		{
			TipoLicenciaUsuarioService service = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);
			EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
			List<empleado> empleados = empleadoService.getAll();
            List<TipoLicenciaUsuarioTO> lus = new List<TipoLicenciaUsuarioTO>();
			foreach (empleado emp in empleados)
			{
				lus.AddRange(service.getLicenciasSaldosFor(emp));
			}
            //lus = lus.FindAll(lu => lu.cantidadDisponible > 0);
            MemoryStream output = ExcelHelper.Saldos(lus);

			output.Position = 0;

			return File(output, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "saldos.xlsx");
		}
	}
}