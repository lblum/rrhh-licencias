﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
    public class LicenciaController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(LicenciaController));

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetCalendar(long id, DateTime start, DateTime end)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				List<CalendarEntryTO> licencias = service.getBetween(id, start, end);

				response = this.getSuccessResponse(licencias);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage PostForHistory(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				PaginationResponseTO<LicenciaGeneradaTO> licencias = service.getForHistory(request, CurrentUser);

				response = this.getSuccessResponse(licencias);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

        public HttpResponseMessage PostForDate(DateTime date, PaginationRequestTO request)
        {
            HttpResponseMessage response;
            try
            {
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                PaginationResponseTO<LicenciaHoyTO> licencias = service.getForDate(date, request);

                response = this.getSuccessResponse(licencias);
            }
            catch (Exception e)
            {
                response = this.getErrorResponse(e);
            }
            return response;
        }

        //[ApiAuthenticationFilter(true)]
        public HttpResponseMessage PostForApprobal(string id, PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				PaginationResponseTO<licencia> licencias = service.getForApprobal(id, request, CurrentUser);

				response = this.getSuccessResponse(licencias);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

        //[ApiAuthenticationFilter(true)]
        public HttpResponseMessage PostForApprobalAttachments(PaginationRequestTO request)
        {
            HttpResponseMessage response;
            try
            {
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                PaginationResponseTO<LicenciaGeneradaTO> licencias = service.getForApprobalAttachments(request, CurrentUser);

                response = this.getSuccessResponse(licencias);
            }
            catch (Exception e)
            {
                response = this.getErrorResponse(e);
            }
            return response;
        }

        // GET api/licencia
        public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				List<licencia> licencias = service.getAll();

				response = this.getSuccessResponse(licencias);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/licencia/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				licencia l = service.find(id);

				response = this.getSuccessResponse(l);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//TODO: validar seguridad en TODOS los métodos de TODOS los controllers
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage PostApprobe([FromBody] LicenciaGeneradaTO l)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				LicenciaGeneradaTO to = service.approbe(l, CurrentUser);

				response = this.getSuccessResponse(to);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		//TODO: validar seguridad en TODOS los métodos de TODOS los controllers
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage PostReject([FromBody] LicenciaGeneradaTO l)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				LicenciaGeneradaTO to = service.reject(l, CurrentUser);

				response = this.getSuccessResponse(to);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

        //TODO: validar seguridad en TODOS los métodos de TODOS los controllers
        //[ApiAuthenticationFilter(true)]
        public HttpResponseMessage PostApprobeAttachment([FromBody] LicenciaGeneradaTO l)
        {
            HttpResponseMessage response;
            try
            {
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                LicenciaGeneradaTO to = service.approbeAttachment(l);

                response = this.getSuccessResponse(to);
            }
            catch (Exception ex)
            {
                response = this.getErrorResponse(ex);
            }
            return response;
        }

        //TODO: validar seguridad en TODOS los métodos de TODOS los controllers
        //[ApiAuthenticationFilter(true)]
        public HttpResponseMessage PostRejectAttachment([FromBody] LicenciaGeneradaTO l)
        {
            HttpResponseMessage response;
            try
            {
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                LicenciaGeneradaTO to = service.rejectAttachment(l);

                response = this.getSuccessResponse(to);
            }
            catch (Exception ex)
            {
                response = this.getErrorResponse(ex);
            }
            return response;
        }
        public HttpResponseMessage PostEdit([FromBody] licencia l)
		{
			HttpResponseMessage response;
			try
			{
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                l = service.edit(l, CurrentUser);
                
                response = this.getSuccessResponse(l);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// POST api/licencia
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] licencia l)
		{
			HttpResponseMessage response;
			try
			{
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                l = service.generate(l, CurrentUser);

				response = this.getSuccessResponse(l);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/licencia/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetDelete(long id)
		{
			HttpResponseMessage response;
			try
			{
				LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
				licencia l = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(l);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
        public HttpResponseMessage GetAttachment(long id)
        {
            try
            {
                LicenciaService service = (LicenciaService)new LicenciaService().setDatabase(db);
                licencia l = service.find(id);
                if (l == null || string.IsNullOrWhiteSpace(l.adjunto_id) || string.IsNullOrWhiteSpace(l.adjunto_id))
                    return Request.CreateResponse(HttpStatusCode.Gone);

                var sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/uploads/");
                var filePath = Path.Combine(sPath, l.adjunto_id);

                if (!File.Exists(filePath))
                    return Request.CreateResponse(HttpStatusCode.Gone);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StreamContent(new FileStream(filePath, FileMode.Open, FileAccess.Read));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = l.adjunto_nombre;
                return response;
            }
            catch (Exception e)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.Gone);
            }
        }

        [HttpPost]
        public HttpResponseMessage PostAttachment()
        {
            HttpResponseMessage response;
            try
            {
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                HttpContext.Current.Request.Files[0] : null;

                if (file != null && file.ContentLength > 0)
                {
                    var directory = HttpContext.Current.Server.MapPath("~/uploads");
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);
                    var guid = Guid.NewGuid().ToString("N");
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(directory, guid);
                    file.SaveAs(path);

                    response = this.getSuccessResponse(new { guid, fileName });
                }
                else
                {
                    response = this.getErrorResponse(new ArgumentException("File not found."));
                }
            }
            catch (Exception e)
            {
                response = this.getErrorResponse(e);
            }
            return response;
        }
        [HttpGet]
        public HttpResponseMessage GetNotificarVencimiento()
        {
            HttpResponseMessage response;
            try
            {
                LicenciasPorVencerService service = (LicenciasPorVencerService)new LicenciasPorVencerService().setDatabase(db);
                EmpleadosListTO res = service.SendMail();
                response = getSuccessResponse(res);
            }
            catch (Exception ex)
            {
                response = getErrorResponse(ex);
            }
            return response;
        }
    }
}