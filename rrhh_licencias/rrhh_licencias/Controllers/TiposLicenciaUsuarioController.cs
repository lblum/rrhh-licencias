﻿using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace rrhh_licencias.Controllers
{
	public class TiposLicenciaUsuarioController : BaseApiController
	{
		public HttpResponseMessage GetVacacionesPendientes(long id)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaUsuarioService service = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

				empleado e = this.db.empleado.Find(id);

				if (e == null)
				{
					throw new Exception("No se encuentra el empleado con id: " + id);
				}

				List<TipoLicenciaUsuarioTO> lus = service.getVacacionesFor(e);
				DateTime today = DateTime.Now;
				int cantidad = 0;
				foreach (TipoLicenciaUsuarioTO to in lus)
				{
					if (to.periodo.fechaFin >= e.fechaIngreso && today > to.periodo.fechaInicio) //&& to.cantidadDisponible> 0
						cantidad += to.cantidadDisponible;
				}

				response = this.getSuccessResponse(new { cantidad = cantidad });
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetVacacionesHistorico(long id)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaUsuarioService service = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

				empleado e = this.db.empleado.Find(id);

				if (e == null)
				{
					throw new Exception("No se encuentra el empleado con id: " + id);
				}

				List<TipoLicenciaUsuarioTO> lus = service.getVacacionesFor(e);
				//area a = service.getLicenciasFor()

				response = this.getSuccessResponse(lus);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Get(long id, long periodoId)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaUsuarioService service = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

				empleado e = this.db.empleado.Find(id);
				periodo p = this.db.periodo.Find(periodoId);

				if (e == null)
				{
					throw new Exception("No se encuentra el empleado con id: " + id);
				}
				if (p == null)
				{
					throw new Exception("No se encuentra el periodo con id: " + periodoId);
				}

				List<TipoLicenciaUsuarioTO> lus = service.getLicenciasFor(e, p);
				//area a = service.getLicenciasFor()

				response = this.getSuccessResponse(lus);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post(TipoLicenciaUsuarioTO to)
		{
			HttpResponseMessage response;
			try
			{
				TipoLicenciaUsuarioService service = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);
				EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
				TipoLicenciaService tipoLicenciaService = (TipoLicenciaService)new TipoLicenciaService().setDatabase(db);
				CondicionEspecialService condicionEspecialService = (CondicionEspecialService)new CondicionEspecialService().setDatabase(db);

				empleado e = empleadoService.find(to.empleado_id);
				tipo_licencia tl = tipoLicenciaService.find(to.id);

				condicion_especial ce = condicionEspecialService.setCondicionEspecial(e, to.periodo, tl, to.cantidadEspecial, to.ingresoAcordado);

				response = this.getSuccessResponse(ce);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}