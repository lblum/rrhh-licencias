﻿using rrhh_licencias.Models;
using rrhh_licencias.Services;
using rrhh_licencias.TransferObjects;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class PeriodosController : BaseApiController
	{
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				List<periodo> periodos = service.getAll();

				response = this.getSuccessResponse(periodos);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage PostPaginated(PaginationRequestTO request)
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				PaginationResponseTO<periodo> periodos = service.getAll(request);

				response = this.getSuccessResponse(periodos);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetFor(string id)
		{
			HttpResponseMessage response;
			try
			{
				DateTime date = DateTime.Parse(id);
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo periodo = service.getFor(date);

				response = this.getSuccessResponse(periodo);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetLast()
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo periodo = service.getLast();

				response = this.getSuccessResponse(periodo);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		public HttpResponseMessage GetCurrent()
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo periodo = service.getCurrent();

				response = this.getSuccessResponse(periodo);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetNext()
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo periodo = service.getNext();

				response = this.getSuccessResponse(periodo);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo p = service.find(id);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage GetBorrarPeriodo(long id)
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);

				periodo p = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] periodo p)
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				FeriadoService feriadoService = (FeriadoService)new FeriadoService().setDatabase(db);

				p = service.saveOrUpdate(p);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				PeriodoService service = (PeriodoService)new PeriodoService().setDatabase(db);
				periodo p = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(p);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}
	}
}