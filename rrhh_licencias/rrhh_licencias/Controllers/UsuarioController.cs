﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace rrhh_licencias.Controllers
{
	public class UsuarioController : BaseApiController
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(UsuarioController));

		// GET api/usuario
		public HttpResponseMessage Get()
		{
			HttpResponseMessage response;
			try
			{
				UsuarioService service = (UsuarioService)new UsuarioService().setDatabase(db);
				List<usuario> usuarios = service.getAll();

				response = this.getSuccessResponse(usuarios);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// GET api/usuario/5
		public HttpResponseMessage Get(long id)
		{
			HttpResponseMessage response;
			try
			{
				UsuarioService service = (UsuarioService)new UsuarioService().setDatabase(db);
				usuario u = service.find(id);

				response = this.getSuccessResponse(u);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}

		// POST api/usuario
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Post([FromBody] usuario u)
		{
			HttpResponseMessage response;
			try
			{
				UsuarioService service = (UsuarioService)new UsuarioService().setDatabase(db);
				RolService rolService = (RolService)new RolService().setDatabase(db);

				usuario dbuser = service.findByLogin(u.login);
				if (dbuser == null)
				{
					usuario tmp = new usuario();
					tmp.email = u.email;
					tmp.empleado_id = u.empleado_id;
					tmp.lastLogin = u.lastLogin;
					tmp.login = u.login;
					tmp.password = u.password;
					
					dbuser = service.saveOrUpdate(tmp);
				}
				else
				{
					dbuser.login = u.login;
					dbuser.email = u.email;
					dbuser.password = u.password;
				}

				List<usuario_rol> urs = u.usuario_rol.ToList();
				u.usuario_rol = new List<usuario_rol>();

				service.deleteUsuarioRoles(dbuser.id);

				List<usuario_rol> dbUrs = new List<usuario_rol>();
				foreach (usuario_rol ur in urs)
				{
					usuario_rol urDb = service.findUsuarioRol(dbuser.id, ur.rol_id);
					if (urDb == null)
					{
						urDb = new usuario_rol();
						rol r = rolService.find(ur.rol.id);
						urDb.rol_id = r.id;
						urDb.usuario_id = dbuser.id;
						service.saveOrUpdate(urDb);
					}

					dbUrs.Add(urDb);
				}

				u = service.saveOrUpdate(dbuser);

				response = this.getSuccessResponse(dbuser);
			}
			catch (Exception ex)
			{
				response = this.getErrorResponse(ex);
			}
			return response;
		}

		// DELETE api/usuario/5
		//[ApiAuthenticationFilter(true)]
		public HttpResponseMessage Delete(long id)
		{
			HttpResponseMessage response;
			try
			{
				UsuarioService service = (UsuarioService)new UsuarioService().setDatabase(db);
				usuario u = service.find(id);
				service.delete(id);

				response = this.getSuccessResponse(u);
			}
			catch (Exception e)
			{
				response = this.getErrorResponse(e);
			}
			return response;
		}


	}
}