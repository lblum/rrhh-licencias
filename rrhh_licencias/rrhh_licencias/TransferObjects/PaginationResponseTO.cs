﻿using System.Collections.Generic;

namespace rrhh_licencias.TransferObjects
{
	public class PaginationResponseTO<T>
	{
		public int totalCount { get; set; }
		public int pagesCount { get; set; }
		public int page { get; set; }
		public List<T> rows { get; set; }
		public int rowsFrom { get; set; }
		public int rowsTo { get; set; }
	}
}