﻿using System;

namespace rrhh_licencias.TransferObjects
{
	public class CalendarEntryTO
	{
        public long id { get; set; }
		public string title { get; set; }
		public DateTime start { get; set; }
		public DateTime end { get; set; }
		public bool allDay { get; set; }
		public string className { get; set; }
		public string status { get; set; }
		public string comentarios { get; set; }
		public int cantidad { get; set; }
        public string adjunto_nombre { get; set; }
        public string adjunto_id { get; set; }
        public string adjunto_estado;
	}
}