﻿namespace rrhh_licencias.TransferObjects
{
	public class PaginationRequestTO
	{
		public string term { get; set; }
		public string orderBy { get; set; }
		public string orderDirection { get; set; }
		public int page { get; set; }
		public int pageSize { get; set; }
	}
}