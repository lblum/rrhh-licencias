﻿using System;

namespace rrhh_licencias.TransferObjects
{
	public class LicenciaHoyTO
	{
        public string employee_id { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string tipo_licencia { get; set; }
        public string jefe_nombres { get; set; }
        public string jefe_apellidos { get; set; }
        public string sector { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public string empresa { get; set; }
        public string legajo { get; set; }
    }
}