﻿using System;
using Newtonsoft.Json;
using rrhh_licencias.Models;

namespace rrhh_licencias.TransferObjects
{
	public class TipoLicenciaUsuarioTO
	{

		public long id { get; set; }
		public string nombre { get; set; }
		public string simbolo { get; set; }
		public int cantidadOriginal { get; set; }
		public int cantidadEspecial { get; set; }
		public int cantidadUtilizada { get; set; }
		public int cantidadDisponible { get; set; }
		public long periodo_id { get; set; }
		public periodo periodo { get; set; }
		public long empleado_id { get; set; }
		public DateTime ingresoAcordado { get; set; }
		public string tipoLicencia { get; set; }
        public bool adjuntoObligatorio { get; set; }

		[JsonIgnore]
		public empleado empleado { get; set; }

    }
}
