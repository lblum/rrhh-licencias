﻿using System.Collections.Generic;

namespace rrhh_licencias.TransferObjects
{
	public class UserCredentialsTO
	{
		public UserCredentialsTO()
		{
			this.permisos = new HashSet<string>();
            this.roles = new HashSet<string>();
        }

		public long id { get; set; }
		public long empleado_id { get; set; }
		public string nombre { get; set; }
		public string apellido { get; set; }
		public string login { get; set; }
		public string legajo { get; set; }
		public ICollection<string> permisos { get; set; }
        public ICollection<string> roles { get; set; }

    }
}