﻿using rrhh_licencias.Models;
using System.Linq;

namespace rrhh_licencias.TransferObjects
{
    public class EmpleadoTO
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Legajo { get; set; }

        public EmpleadoTO(empleado e)
        {
            this.Nombres = e.nombres;
            this.Apellidos = e.apellidos;
            this.Legajo = e.legajo;
            if (e.usuario.Count > 0)
            {
                this.Email = e.usuario.FirstOrDefault().email;
            }
        }
    }
}