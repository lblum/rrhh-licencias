﻿using System.Collections.Generic;

namespace rrhh_licencias.TransferObjects
{
    public class EmpleadosListTO
    {
        public List<EmpleadoTO> Success { get; set; }
        public List<EmpleadoTO> Errors { get; set; }

        public EmpleadosListTO()
        {
            Success = new List<EmpleadoTO>();
            Errors = new List<EmpleadoTO>();
        }
    }
}