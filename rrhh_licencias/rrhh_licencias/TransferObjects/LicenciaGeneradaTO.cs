﻿using rrhh_licencias.Models;

namespace rrhh_licencias.TransferObjects
{
	public class LicenciaGeneradaTO : licencia
	{
		public TipoLicenciaUsuarioTO tipo_licencia_usuario { get; set; }
	}
}