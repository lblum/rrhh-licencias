﻿namespace rrhh_licencias.TransferObjects
{
	public class SimplifiedExceptionTO
	{
		public string Message { get; set; }
		public string ClassName { get; set; }
	}
}