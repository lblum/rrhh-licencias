﻿-- v??.??.??
CREATE TABLE empresa (
  id bigint IDENTITY(1,1) NOT NULL,
  nombre varchar(100)  NOT NULL,
);
GO
ALTER TABLE empresa ADD CONSTRAINT PK_empresa PRIMARY KEY (id);
GO
INSERT INTO [dbo].[empresa] ([nombre])
VALUES ('DEFAULT');
GO
ALTER TABLE empleado ADD empresa_id bigint NULL;
GO
UPDATE empleado SET empresa_id = 1;
GO
ALTER TABLE empleado ALTER COLUMN empresa_id bigint NOT NULL;
GO
ALTER TABLE empleado
ADD CONSTRAINT FK_EmpleadoEmpresa
FOREIGN KEY (empresa_id) REFERENCES empresa(id);
GO
---
ALTER TABLE tipo_licencia ADD adjunto_obligatorio bit;
GO
UPDATE tipo_licencia SET adjunto_obligatorio = 0;
GO
ALTER TABLE tipo_licencia ALTER COLUMN adjunto_obligatorio bit NOT NULL;
GO
ALTER TABLE licencia ADD adjunto_nombre varchar(200) NULL;
GO
ALTER TABLE licencia ADD adjunto_id varchar(32) NULL;
GO
---
ALTER TABLE licencia ADD adjunto_estado varchar(200) NULL;
GO

USE Holando_RRHH_Licencias
  INSERT INTO tipo_licencia
  (nombre, cantidad, descripcion, acumulable,simbolo, event_class, codigo, adjunto_obligatorio)
  VALUES
  ('Compensación', 100, 'Compensación', 0, 'event-compensacion', 'event-compensacion', 26, 0)


USE Holando_RRHH_Licencias
INSERT INTO tipo_licencia
(nombre, cantidad, descripcion, acumulable, simbolo, event_class, codigo, adjunto_obligatorio)
VALUES   ('Medio RP - Llegada tarde',300, 'Medio RP - Llegada tarde',0,'event-tarde','event-tarde',30,0),
         ('Medio RP - Retiro Anticipado',300, 'Medio RP - Retiro Anticipado',0,'event-anticipado','event-anticipado',31,0)
INSERT INTO rol (nombre) values ('admin');
INSERT INTO permiso (nombre) values ('admin.resetear');
INSERT INTO rol_permiso (rol_id, permiso_id) values ((SELECT id from rol where nombre = 'admin'), (SELECT id FROM permiso WHERE nombre = 'admin.resetear'));