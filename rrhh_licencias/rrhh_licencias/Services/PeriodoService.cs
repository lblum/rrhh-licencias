﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
	public class PeriodoService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PeriodoService));

		public PaginationResponseTO<periodo> getAll(PaginationRequestTO request)
		{
			int rangeStart = request.pageSize * request.page;
			string term = request.term.ToLower();


			List<periodo> ret = db.periodo.Where(
				r => r.nombre.ToLower().Contains(term)
			).ToList();


			// SORT
			if (request.orderBy == "nombre")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.nombre).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.nombre).ToList();
				}
			}
			else if (request.orderBy == "fecha_inicio")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.fechaInicio).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.fechaInicio).ToList();
				}
			}
			else if (request.orderBy == "fecha_fin")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.fechaFin).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.fechaFin).ToList();
				}
			}

			PaginationResponseTO<periodo> toResponse = new PaginationResponseTO<periodo>();
			toResponse.page = request.page;
			toResponse.totalCount = ret.Count;
			toResponse.pagesCount = (ret.Count / request.pageSize) + 1;

			ret = ret.Skip(rangeStart).Take(request.pageSize).ToList();
			toResponse.rowsFrom = rangeStart;
			toResponse.rowsTo = rangeStart + ret.Count - 1;

			toResponse.rows = ret;

			return toResponse;
		}

		public List<periodo> getAll()
		{
			return db.periodo.ToList();
		}

		public periodo getLast()
		{
			return db.periodo.ToList().OrderBy(p => p.fechaFin).LastOrDefault();
		}

		public periodo getCurrent()
		{
			return db.periodo.FirstOrDefault(p => p.fechaInicio <= DateTime.Now &&
                                                       p.fechaFin > DateTime.Now);
		}

		public periodo getNext()
		{
			periodo current = getCurrent();
			return db.periodo.Where(
				p => p.fechaInicio > current.fechaInicio
			).OrderByDescending(p => p.fechaInicio).FirstOrDefault();
		}

		public periodo getFor(DateTime date)
		{
			return db.periodo.FirstOrDefault(p => p.fechaInicio <= date &&
                                                       p.fechaFin > date);
		}

		public periodo find(long? id)
		{
			return db.periodo.Find(id);
		}

		public periodo saveOrUpdate(periodo p)
		{
			if (p.id > 0)
			{
				db.Entry(p).State = EntityState.Modified;
			}
			else
			{
				periodo viejo = db.periodo.OrderByDescending(per => per.fechaInicio).FirstOrDefault();
				p = db.periodo.Add(p);
				if (viejo != null)
				{
					foreach (condicion_especial cold in viejo.condicion_especial)
					{
						condicion_especial ce = new condicion_especial();
						ce.cantidad_acordada = cold.cantidad_acordada;
						ce.empleado_id = cold.empleado_id;
						ce.ingreso_acordado = cold.ingreso_acordado;
						ce.periodo_id = p.id;
						ce.tipo_licencia_id = cold.tipo_licencia_id;
						db.condicion_especial.Add(ce);
					}
				}
			}
			save();

			return p;
		}

		public void delete(long id)
		{
			periodo p = db.periodo.Find(id);
			db.periodo.Remove(p);
			save();
		}
	}
}