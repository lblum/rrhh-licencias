﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using log4net;
using rrhh_licencias.Exceptions;
using rrhh_licencias.Models;

namespace rrhh_licencias.Services
{
	public class BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(BaseService));

		protected holando_rrhh_licenciasEntities db;

		public BaseService setDatabase(holando_rrhh_licenciasEntities db)
		{
			this.db = db;
			return this;
		}

        public usuario getCurrentUsuario(string userName)
		{
            Trace.WriteLine("--------------------------------->userName:" + userName);

			return db.usuario.FirstOrDefault(u => u.login == userName);
		}

		public int save()
		{
			try
			{
				return db.SaveChanges();
			}
			catch (DbUpdateException e)
			{
				Trace.WriteLine("----------------->DbUpdateException");
				log.Error(e.Message, e);
				string sqlMessage = null;
				var sqlException = e.GetBaseException() as SqlException;
				if (sqlException != null)
				{
					if (sqlException.Errors.Count > 0)
					{
						switch (sqlException.Errors[0].Number)
						{
							case 547: // Foreign Key violation
								sqlMessage = "No se puede actualizar el registro por referencias dependientes.";
								break;
							case 2627:
								sqlMessage = "Ya existe una entidad idéntica";
								break;
						}
					}
				}
				if (sqlMessage != null)
				{
					throw new ServiceException(sqlMessage);
				}

                throw new ServiceException("Error inesperado: " + e.Message);
            }
			catch (DbEntityValidationException e)
			{
				Trace.WriteLine("----------------->DbEntityValidationException");
				List<string> details = new List<string>();
				foreach (DbEntityValidationResult res in e.EntityValidationErrors)
				{
					foreach (DbValidationError err in res.ValidationErrors)
					{
						string detail = res.Entry.Entity.GetType().Name + "." + err.PropertyName + ": " + err.ErrorMessage;
						details.Add(detail);
						Trace.WriteLine("----------------->" + detail);
					}
				}
				throw new ServiceException("Error inesperado: " + e.Message, details);
			}
			catch (Exception e)
			{
				Trace.WriteLine("----------------->");
				Trace.WriteLine(e.GetType().Name);
				throw new ServiceException("Error inesperado: " + e.Message);
			}
		}
	}
}