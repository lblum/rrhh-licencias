﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;

namespace rrhh_licencias.Services
{
	public class UsuarioService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(UsuarioService));

		public List<usuario> getAll()
		{
			return db.usuario.ToList();
		}

		public usuario find(long? id)
		{
			return db.usuario.Find(id);
		}

		public usuario findByLogin(string login)
		{
			return db.usuario.FirstOrDefault(u => u.login == login);
		}

		public usuario_rol findUsuarioRol(long id, long rol_id)
		{
			return db.usuario_rol.FirstOrDefault(ur => ur.rol_id == rol_id && ur.usuario_id == id);
		}

		public usuario saveOrUpdate(usuario u)
		{
			usuario dbu;
			if (u.id > 0)
			{
				dbu = db.usuario.Find(u.id);
				dbu.email = u.email;
				dbu.password = u.password;
				dbu.login = u.login;
				db.Entry(dbu).State = EntityState.Modified;
			}
			else
			{
				dbu = db.usuario.Add(u);
			}
			save();

			return dbu;
		}

		public usuario_rol saveOrUpdate(usuario_rol ur)
		{
			if (ur.id > 0)
			{
				db.Entry(ur).State = EntityState.Modified;
			}
			else
			{
				ur = db.usuario_rol.Add(ur);
			}
			save();

			return ur;
		}

		public void delete(long id)
		{
			usuario u = db.usuario.Find(id);
			db.usuario.Remove(u);
			save();
		}

		public void deleteUsuarioRoles(long id)
		{
			List<usuario_rol> urs = db.usuario_rol.Where(ur => ur.usuario_id == id).ToList();
			foreach (usuario_rol ur in urs)
			{
				db.usuario_rol.Remove(ur);
			}
			save();
		}
	}
}