﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
    public class EmpleadoService : BaseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmpleadoService));

        public List<empleado> getAll()
        {
            return getAll(false);
        }

        public List<empleado> GetJefesYDelegados()
        {
            List<empleado> todos = db.empleado.Where(e => e.activo == 1).ToList();

            List<empleado> jefes = new List<empleado>();
            foreach (empleado e in todos)
            {
                if (e.delegado != null)
                {
                    jefes.Add(e.delegado);
                }
                if (e.jefe != null)
                {
                    jefes.Add(e.jefe);
                }
            }
            return jefes;
        }

        public List<empleado> getByPrincipalRol(string currentUser)
        {
            usuario current = getCurrentUsuario(currentUser);
            string type = "jefe";
            foreach (usuario_rol ur in current.usuario_rol)
            {
                foreach (rol_permiso rp in ur.rol.rol_permiso)
                {
                    if (rp.permiso.nombre == "licencia.aprobar.rrhh" || rp.permiso.nombre == "tipo.licencia.usuario.editar")
                    {
                        type = "rrhadmin";
                        break;
                    }
                }
                if (type == "rrhadmin") break;
            }

            if (type == "rrhadmin")
            {
                return getAll(false);
            }

            List<empleado> ret = new List<empleado>();
            foreach (empleado delegadoDe in current.empleado.delegado_de)
            {
                foreach (empleado sub in delegadoDe.subalternos)
                {
                    ret.Add(sub);
                }
            }
            foreach (empleado sub in current.empleado.subalternos)
            {
                ret.Add(sub);
            }
            return ret;

        }

        public PaginationResponseTO<empleado> getPagesByPrincipalRol(PaginationRequestTO request, string currentUser)
        {
            int rangeStart = request.pageSize * request.page;
            string term = request.term.ToLower();

            usuario current = getCurrentUsuario(currentUser);
            string type = "jefe";

            var permiso1 = db.permiso.FirstOrDefault(x => x.nombre == "licencia.aprobar.rrhh");
            var permiso2 = db.permiso.FirstOrDefault(x => x.nombre == "tipo.licencia.usuario.editar");
            if (permiso1 != null && permiso2 != null)
            {
                var roles = db.rol_permiso.Where(x => x.permiso_id == permiso1.id || x.permiso_id == permiso2.id);
                if (current.usuario_rol.Any(x => roles.Any(y => y.rol_id == x.rol_id)))
                    type = "rrhadmin";
            }
            else if (permiso1 != null)
            {
                var roles = db.rol_permiso.Where(x => x.permiso_id == permiso1.id);
                if (current.usuario_rol.Any(x => roles.Any(y => y.rol_id == x.rol_id)))
                    type = "rrhadmin";
            }
            else if (permiso2 != null)
            {
                var roles = db.rol_permiso.Where(x => x.permiso_id == permiso2.id);
                if (current.usuario_rol.Any(x => roles.Any(y => y.rol_id == x.rol_id)))
                    type = "rrhadmin";
            }

            IQueryable<empleado> ret = null;

            if (type == "rrhadmin")
            {
                ret = db.empleado.Where(
                            e => e.activo == 1
                                && (
                                    (e.nombres + " " + e.apellidos).ToLower().Contains(term)
                                    || e.employee_id.Contains(term)
                                    || e.legajo.Contains(term)
                                    || (e.jefe_id == 0 || (e.jefe.nombres + " " + e.jefe.apellidos).ToLower().Contains(term))
                                    || e.empresa.nombre.ToLower().Contains(term)
                                )
                        );
            }
            else
            {

                ret = current.empleado.delegado_de.SelectMany(x => x.subalternos)
                    .Union(current.empleado.subalternos).AsQueryable();

                foreach (var t in term.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    ret = ret.Where(x => x.nombres.ToLower().Contains(t)
                            || x.apellidos.ToLower().Contains(t)
                            || x.employee_id.Contains(t)
                            || x.legajo.Contains(t)
                            || (
                                x.jefe_id == 0
                                || x.jefe.nombres.ToLower().Contains(t)
                                || x.jefe.apellidos.ToLower().Contains(t)
                            )
                            || x.empresa.nombre.ToLower().Contains(term)
                        );
                }
            }

            // SORT
            if (request.orderBy == "employee_id")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.employee_id);
                }
                else
                {
                    ret = ret.OrderBy(l => l.employee_id);
                }
            }
            else if (request.orderBy == "legajo")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.nombres).ThenBy(l => l.legajo);
                }
                else
                {
                    ret = ret.OrderBy(l => l.nombres).ThenByDescending(l => l.legajo);
                }
            }
            else if (request.orderBy == "nombre")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.nombres).ThenBy(l => l.apellidos);
                }
                else
                {
                    ret = ret.OrderBy(l => l.nombres).ThenByDescending(l => l.apellidos);
                }
            }
            else if (request.orderBy == "fecha_ingreso")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.fechaIngreso);
                }
                else
                {
                    ret = ret.OrderBy(l => l.fechaIngreso);
                }
            }
            else if (request.orderBy == "jefe")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.jefe.nombres);
                }
                else
                {
                    ret = ret.OrderBy(l => l.jefe.nombres);
                }
            }
            else if (request.orderBy == "empresa")
            {
                if (request.orderDirection == "desc")
                {
                    ret = ret.OrderByDescending(l => l.empresa.nombre);
                }
                else
                {
                    ret = ret.OrderBy(l => l.empresa.nombre);
                }
            }
            else
            {
                ret = ret.OrderBy(l => l.id);
            }

            PaginationResponseTO<empleado> toResponse = new PaginationResponseTO<empleado>();
            toResponse.page = request.page;
            toResponse.totalCount = ret.Count();
            toResponse.pagesCount = (toResponse.totalCount / request.pageSize) + 1;

            var result = ret.Skip(rangeStart).Take(request.pageSize).ToList();
            toResponse.rowsFrom = rangeStart;
            toResponse.rowsTo = rangeStart + result.Count - 1;

            toResponse.rows = result;

            return toResponse;
        }

        public List<empleado> getByArea(long area_id)
        {
            return getByArea(area_id, false);
        }

        public List<empleado> getByArea(long area_id, bool includeEliminados)
        {
            if (includeEliminados)
            {
                return db.empleado.Where(e => e.area_id == area_id).ToList();
            }

            return db.empleado.Where(e => e.area_id == area_id && e.activo == 1).ToList();
        }

        public List<empleado> getAll(bool includeEliminados)
        {
            List<empleado> emps;
            if (includeEliminados)
            {
                emps = db.empleado.ToList();
            }
            else
            {
                emps = db.empleado.Where(e => e.activo == 1).ToList();
            }


            return emps;
        }

        public empleado find(long? id)
        {
            return db.empleado.Find(id);
        }

        public List<empleado> findByTerm(String term)
        {
            return db.empleado.Where(
                    e => e.apellidos.Contains(term) ||
                        e.nombres.Contains(term) ||
                        e.legajo.Contains(term)
                ).ToList();
        }

        public empleado delegar(long empleadoid, long jefeId)
        {
            empleado e = db.empleado.Find(empleadoid);
            if (!e.usuario.First().usuario_rol.Any(ur => ur.rol.nombre == "Jefe"))
            {
                throw new Exception("No se puede delegar a un empleado sin rol de Jefe");
            }
            empleado jefe = db.empleado.Find(jefeId);
            jefe.delegado_id = e.id;
            db.Entry(jefe).State = EntityState.Modified;
            save();

            return jefe;
        }

        public empleado desDelegar(long empleadoid)
        {
            empleado jefe = db.empleado.FirstOrDefault(v => v.delegado_id == empleadoid);
            jefe.delegado_id = null;

            db.Entry(jefe).State = EntityState.Modified;
            save();

            return jefe;
        }

        public empleado saveOrUpdate(empleado e)
        {
            return saveOrUpdate(e, true);
        }

        public empleado saveOrUpdate(empleado e, bool hit)
        {
            if (e.id > 0)
            {
                db.Entry(e).State = EntityState.Modified;
            }
            else
            {
                e = db.empleado.Add(e);
            }
            if (hit)
                save();

            return e;
        }

        public void delete(long id)
        {
            empleado e = db.empleado.Find(id);
            db.empleado.Remove(e);
            save();
        }
    }
}