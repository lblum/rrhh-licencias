﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;
using rrhh_licencias.Helper;

namespace rrhh_licencias.Services
{
    public class LicenciasPorVencerService : BaseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LicenciasPorVencerService));

        public EmpleadosListTO SendMail()
        {
            //api/licencia/GetNotificarVencimiento
            EmpleadosListTO datos = new EmpleadosListTO();
            TipoLicenciaUsuarioService tipoLicenciaUsuarioService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(this.db);

            DateTime anio = DateTime.Now.AddYears((-1) * ConfigurationHelper.LimiteAniosAcumulacion);
            periodo periodoAVencer = (from p in db.periodo
                                      where p.fechaInicio < anio && p.fechaFin > anio
                                      select p).FirstOrDefault();
            tipo_licencia tipo = (from tl in db.tipo_licencia
                                  where tl.simbolo == "event-vacaciones"
                                  select tl).FirstOrDefault();
            List<empleado> empleados = (from e in db.empleado
                                        where e.activo == 1 && e.fechaIngreso < anio
                                        select e).ToList();
            List<empleado> empleadosForMail = new List<empleado>();
            foreach (empleado e in empleados)
            {
                TipoLicenciaUsuarioTO to = tipoLicenciaUsuarioService.getLicenciaFor(e, periodoAVencer, tipo);
                if (to.cantidadDisponible > 0)
                {
                    try
                    {
                        EmailHelper.sendAlertVencimiento(e, db, to.cantidadDisponible.ToString(), to.periodo.nombre);
                        log.Info($"Mail de notificacion a {e.id} - {e.legajo}");
                        datos.Success.Add(new EmpleadoTO(e));
                    }
                    catch (Exception)
                    {
                        log.Error($"Error al mandar mail de notificacion a {e.id} - {e.legajo}");
                        datos.Errors.Add(new EmpleadoTO(e));
                    }

                }
            }
            return datos;
        }
    }
}