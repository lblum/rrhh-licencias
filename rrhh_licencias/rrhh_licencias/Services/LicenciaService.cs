﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using log4net;
using rrhh_licencias.Helper;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
    public class LicenciaService : BaseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LicenciaService));

        public static string STATUS_PENDIENTE = "pendiente";
        public static string STATUS_APROBADO_JEFE = "app_jefe";
        public static string STATUS_RECHAZADO = "rechazado";
        public static string STATUS_APROBADO = "aprobado";
        public static string STATUS_BALANCE = "balance";

        public List<licencia> getAll()
        {
            return db.licencia.ToList();
        }
        public licencia generate(licencia l, string currentUser)
        {
            usuario current = getCurrentUsuario(currentUser);
            bool rrhh = EsRRHH(current) && current.empleado_id != l.empleado_id;
            //TODO: Validar que la fecha de inicio no sea mayor a la de fin
            //TODO: Validar que el usuario sea el usuario del empleado en cuestion o se trate de su jefe o se trate de RRHH
            //TODO: Validar que la cantidad de días no superen los días disponibles
            l.estado = STATUS_PENDIENTE;
            l.fecha_solicitud = DateTime.Now;
            validarLicencia(l, current, rrhh);
            verificarCantidadDias(l);
            l = saveOrUpdate(l);
            sendEmailModificacion(l, rrhh);
            return l;
        }
        private void validarMedioRP(licencia l)
        {
            if (l.fecha_inicio != l.fecha_fin)
            {
                throw new Exception("El período para este tipo de licencia no puede superar al día");
            }
            List<licencia> licencias = db.licencia.Where(
                lic => lic.empleado_id == l.empleado_id
                    && lic.estado != STATUS_RECHAZADO
                    && (
                            (lic.tipo_licencia_id == l.tipo_licencia_id
                            && l.fecha_inicio == lic.fecha_inicio
                            && l.fecha_fin == lic.fecha_fin)
                        )
            ).ToList();
            if (licencias != null && licencias.Count > 1)
            {
                throw new Exception("No puede tomarse mas de dos medios RP en un día");
            }

        }

        private bool esMedioRP(licencia l)
        {
            string rp_class = ConfigurationManager.AppSettings["RP_CLASS"];
            var licenciaRP = db.tipo_licencia.FirstOrDefault(tl => tl.event_class == rp_class);
            if (licenciaRP == null) throw new Exception("Error de configuración en el servidor");
            return l.tipo_licencia_id == licenciaRP.id;
        }

        internal void updateLicenciasConFecha(DateTime fecha)
        {
            List<licencia> licenciasAVerificar = (from l in db.licencia
                                                  where l.fecha_inicio <= fecha
                                                      && l.fecha_fin >= fecha
                                                  //&& l.estado != LicenciaService.STATUS_RECHAZADO
                                                  select l).ToList();
            foreach (licencia l in licenciasAVerificar)
            {
                update(l);
            }
        }
        private licencia update(licencia l)
        {
            verificarCantidadDias(l);

            licencia dbLic = find(l.id);
            dbLic.fecha_inicio = l.fecha_inicio;
            dbLic.fecha_fin = l.fecha_fin;
            dbLic.cantidad = l.cantidad;
            dbLic.notas_empleado = l.notas_empleado;
            dbLic.aprobador_rrhh_notas = l.aprobador_rrhh_notas;
            if (dbLic.adjunto_id != l.adjunto_id)
            {
                dbLic.adjunto_id = l.adjunto_id;
                dbLic.adjunto_nombre = l.adjunto_nombre;
                dbLic.adjunto_estado = l.adjunto_id != null ? "pendiente" : null;
            }
            dbLic.notas_empleado = l.notas_empleado;
            return saveOrUpdate(dbLic);
        }
        public licencia edit(licencia l, string currentUser)
        {
            usuario current = getCurrentUsuario(currentUser);
            bool rrhh = EsRRHH(current) && current.empleado_id != l.empleado_id;
            bool empleadoEdit = l.empleado_id == current.empleado_id;
            validarLicencia(l, current, rrhh);
            bool adjuntoEdit = this.adjuntoEdit(l);
            l = update(l);
            if (empleadoEdit && ! adjuntoEdit)
            {
                sendEmailModificacion(l, rrhh);
            }
            else if (adjuntoEdit)
            {
                sendEmailAdjuntoEdit(l, rrhh);
            }
            else
            {
                sendEmailEmpleadoYJefe(l, rrhh);
            }
            return l;
        }

        public bool adjuntoEdit(licencia l)
        {
            return find(l.id).adjunto_id != l.adjunto_id;
        }

        public LicenciaGeneradaTO reject(LicenciaGeneradaTO toIn, string currentUser)
        {
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            licencia lIn = db.licencia.FirstOrDefault(l => l.id == toIn.id);
            lIn.aprobador_jefe_fecha = toIn.aprobador_jefe_fecha;
            lIn.aprobador_jefe_id = toIn.aprobador_jefe_id;
            lIn.aprobador_jefe_notas = toIn.aprobador_jefe_notas;
            lIn.aprobador_rrhh_fecha = toIn.aprobador_rrhh_fecha;
            lIn.aprobador_rrhh_id = toIn.aprobador_rrhh_id;
            lIn.aprobador_rrhh_notas = toIn.aprobador_rrhh_notas;
            lIn.cantidad = toIn.cantidad;
            lIn.estado = toIn.estado;
            lIn.estado_notificado = toIn.estado_notificado;
            lIn.fecha_exportacion = toIn.fecha_exportacion;
            lIn.fecha_fin = toIn.fecha_fin;
            lIn.fecha_inicio = toIn.fecha_inicio;
            lIn.fecha_solicitud = toIn.fecha_solicitud;
            lIn.notas_empleado = lIn.notas_empleado;
            lIn.tipo_licencia_id = toIn.tipo_licencia_id;

            usuario current = getCurrentUsuario(currentUser);
            string type = "jefe";
            foreach (usuario_rol ur in current.usuario_rol)
            {
                foreach (rol_permiso rp in ur.rol.rol_permiso)
                {
                    if (rp.permiso.nombre == "licencia.aprobar.rrhh")
                    {
                        type = "rrhh";
                        break;
                    }
                }
                if (type == "rrhh") break;
            }

            if (type == "jefe")
            {
                lIn.aprobador_jefe_fecha = DateTime.Now;
                lIn.aprobador_jefe_id = current.empleado_id;
            }
            else
            {
                lIn.aprobador_rrhh_fecha = DateTime.Now;
                lIn.aprobador_rrhh_id = current.empleado_id;
            }
            lIn.estado = STATUS_RECHAZADO;

            lIn.empleado = null;
            lIn.tipo_licencia = null;
            Trace.WriteLine("----------------------->" + lIn.id);
            licencia lic = saveOrUpdate(lIn);

            LicenciaGeneradaTO to = new LicenciaGeneradaTO();
            lic.CopyProperties(to);
            TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(lic.empleado, lic.licencia_periodo_cantidad.LastOrDefault().periodo, lic.tipo_licencia);
            to.tipo_licencia_usuario = tipoTo;

            if (type == "rrhh")
            {
                EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
                var emp = empleadoService.find(lIn.empleado_id);
                var jefe = empleadoService.find(emp?.jefe_id);
                if (jefe != null && jefe.delegado_id != null)
                {
                    EmailHelper.sendRejection(empleadoService.find(jefe?.delegado_id), emp, lic.aprobador_rrhh_notas, lic.tipo_licencia.nombre, db);
                }
                else
                {
                    EmailHelper.sendRejection(jefe, emp, lic.aprobador_rrhh_notas, lic.tipo_licencia.nombre, db);
                }                 
            }
            else
            {
                EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
                var emp = empleadoService.find(lIn.empleado_id);
                EmailHelper.sendRejectionFromJefe(emp, lic.aprobador_jefe_notas, lic.tipo_licencia.nombre, db);
            }

            return to;
        }

        public LicenciaGeneradaTO approbe(LicenciaGeneradaTO toIn, string currentUser)
        {
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            licencia lIn = db.licencia.FirstOrDefault(l => l.id == toIn.id);
            lIn.aprobador_jefe_fecha = toIn.aprobador_jefe_fecha;
            lIn.aprobador_jefe_id = toIn.aprobador_jefe_id;
            lIn.aprobador_jefe_notas = toIn.aprobador_jefe_notas;
            lIn.aprobador_rrhh_fecha = toIn.aprobador_rrhh_fecha;
            lIn.aprobador_rrhh_id = toIn.aprobador_rrhh_id;
            lIn.aprobador_rrhh_notas = toIn.aprobador_rrhh_notas;
            lIn.cantidad = toIn.cantidad;
            lIn.estado = toIn.estado;
            lIn.estado_notificado = toIn.estado_notificado;
            lIn.fecha_exportacion = toIn.fecha_exportacion;
            lIn.fecha_fin = toIn.fecha_fin;
            lIn.fecha_inicio = toIn.fecha_inicio;
            lIn.fecha_solicitud = toIn.fecha_solicitud;
            lIn.notas_empleado = lIn.notas_empleado;
            lIn.tipo_licencia_id = toIn.tipo_licencia_id;

            usuario current = getCurrentUsuario(currentUser);
            string type = "jefe";
            foreach (usuario_rol ur in current.usuario_rol)
            {
                foreach (rol_permiso rp in ur.rol.rol_permiso)
                {
                    if (rp.permiso.nombre == "licencia.aprobar.rrhh")
                    {
                        type = "rrhh";
                        break;
                    }
                }
                if (type == "rrhh") break;
            }
            var initialStatus = toIn.estado;
            if (type == "jefe")
            {
                lIn.aprobador_jefe_fecha = DateTime.Now;
                lIn.aprobador_jefe_id = current.empleado_id;
                lIn.estado = STATUS_APROBADO_JEFE;
            }
            else
            {
                lIn.aprobador_rrhh_fecha = DateTime.Now;
                lIn.aprobador_rrhh_id = current.empleado_id;
                lIn.estado = STATUS_APROBADO;
            }

            lIn.empleado = null;
            lIn.tipo_licencia = null;
            licencia lic = saveOrUpdate(lIn);

            LicenciaGeneradaTO to = new LicenciaGeneradaTO();
            lic.CopyProperties(to);
            TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(lic.empleado, lic.licencia_periodo_cantidad.LastOrDefault().periodo, lic.tipo_licencia);
            to.tipo_licencia_usuario = tipoTo;
            EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
            var emp = empleadoService.find(lIn.empleado_id);
            var jefe = empleadoService.find(emp?.jefe_id);
            if (jefe != null && jefe.delegado_id != null)
            {
                jefe = empleadoService.find(jefe?.delegado_id);
            }
            if (type == "rrhh")
            {
                if (initialStatus == STATUS_PENDIENTE)
                {
                    EmailHelper.sendAlertRRHH(emp, lIn.aprobador_rrhh_notas, lIn.tipo_licencia.nombre, db);
                }
                EmailHelper.sendApproval(jefe, emp, lIn.aprobador_rrhh_notas, lIn.tipo_licencia.nombre, db);
            }
            else if (lIn.estado == STATUS_APROBADO_JEFE)
            {

                EmailHelper.sendApprovalJefe(emp, lIn.aprobador_jefe_notas, lIn.tipo_licencia.nombre, db);
            }

            return to;
        }



        public LicenciaGeneradaTO rejectAttachment(LicenciaGeneradaTO toIn)
        {
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            licencia lIn = db.licencia.FirstOrDefault(l => l.id == toIn.id);
            lIn.adjunto_estado = STATUS_RECHAZADO;
            licencia lic = saveOrUpdate(lIn);

            LicenciaGeneradaTO to = new LicenciaGeneradaTO();
            lic.CopyProperties(to);
            TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(lic.empleado, lic.licencia_periodo_cantidad.LastOrDefault().periodo, lic.tipo_licencia);
            to.tipo_licencia_usuario = tipoTo;

            return to;
        }

        public LicenciaGeneradaTO approbeAttachment(LicenciaGeneradaTO toIn)
        {
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            licencia lIn = db.licencia.FirstOrDefault(l => l.id == toIn.id);
            lIn.adjunto_estado = STATUS_APROBADO;
            licencia lic = saveOrUpdate(lIn);

            LicenciaGeneradaTO to = new LicenciaGeneradaTO();
            lic.CopyProperties(to);
            TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(lic.empleado, lic.licencia_periodo_cantidad.LastOrDefault().periodo, lic.tipo_licencia);
            to.tipo_licencia_usuario = tipoTo;

            return to;
        }

        public PaginationResponseTO<LicenciaGeneradaTO> getForHistory(PaginationRequestTO request, string currentUser)
        {
            int rangeStart = request.pageSize * request.page;
            string term = request.term.ToLower();

            string strHistoricoFrom = ConfigurationManager.AppSettings["HISTORICO_FROM"];
            DateTime historicoFrom = DateTime.Parse(strHistoricoFrom);
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            usuario current = getCurrentUsuario(currentUser);
            IQueryable<licencia> licencias = null;
            string type = "jefe";
            var permiso = db.permiso.FirstOrDefault(x => x.nombre == "licencia.aprobar.rrhh");
            if (permiso != null)
            {
                var roles = db.rol_permiso.Where(x => x.permiso_id == permiso.id);
                if (current.usuario_rol.Any(x => roles.Any(y => y.rol_id == x.rol_id)))
                    type = "rrhh";
            }
            if (type == "jefe")
            {
                licencias = db.licencia.Where(
                    l => (l.empleado.jefe_id == current.empleado_id || l.empleado.jefe.delegado_id == current.id)
                        && (l.estado == STATUS_APROBADO || l.estado == STATUS_RECHAZADO) && l.fecha_inicio > historicoFrom
                        && ( // TERM SEARCH
                            (l.empleado.nombres.ToLower() + " " + l.empleado.apellidos.ToLower()).Contains(term)
                        )
                ).OrderByDescending(l => l.fecha_solicitud);
            }
            else
            {
                licencias = db.licencia.Where(
                    l => (
                        l.estado == STATUS_APROBADO || l.estado == STATUS_RECHAZADO) && l.fecha_inicio > historicoFrom
                        && ( // TERM SEARCH
                            (l.empleado.nombres.ToLower() + " " + l.empleado.apellidos.ToLower()).Contains(term)
                        )
                ).OrderByDescending(
                    l => l.fecha_solicitud
                );
            }

            // SORT
            if (request.orderBy == "estado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.estado);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.estado);
                }
            }
            else if (request.orderBy == "empleado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.nombres).ThenByDescending(l => l.empleado.apellidos);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
            }
            else if (request.orderBy == "fecha_desde")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_inicio);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_inicio);
                }
            }
            else if (request.orderBy == "fecha_hasta")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_fin);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_fin);
                }
            }
            else
            {
                if (request.orderDirection != "desc")
                {
                    licencias = licencias.OrderBy(l => l.fecha_solicitud);
                }
            }
            PaginationResponseTO<LicenciaGeneradaTO> toResponse = new PaginationResponseTO<LicenciaGeneradaTO>();
            toResponse.page = request.page;
            toResponse.totalCount = licencias.Count();
            toResponse.pagesCount = (toResponse.totalCount / request.pageSize) + 1;
            licencias = licencias.Skip(rangeStart).Take(request.pageSize);


            List<LicenciaGeneradaTO> ret = new List<LicenciaGeneradaTO>();
            List<licencia> licenciasList = licencias.ToList();
            foreach (licencia l in licenciasList)
            {
                LicenciaGeneradaTO to = new LicenciaGeneradaTO();
                l.CopyProperties(to);
                TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(l.empleado, l.licencia_periodo_cantidad.LastOrDefault().periodo, l.tipo_licencia);
                to.tipo_licencia_usuario = tipoTo;
                ret.Add(to);
            }
            toResponse.rowsFrom = rangeStart;
            toResponse.rowsTo = rangeStart + ret.Count - 1;
            toResponse.rows = ret;

            return toResponse;
        }

        public PaginationResponseTO<licencia> getForApprobal(string tipo, PaginationRequestTO request, string currentUser)
        {
            int rangeStart = request.pageSize * request.page;
            string term = request.term.ToLower();
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            usuario current = getCurrentUsuario(currentUser);
            IQueryable<licencia> licencias = null;

            string userRole = "jefe";
            var permiso = db.permiso.FirstOrDefault(x => x.nombre == "licencia.aprobar.rrhh");
            if (permiso != null)
            {
                var roles = db.rol_permiso.Where(x => x.permiso_id == permiso.id);
                if (current.usuario_rol.Any(x => roles.Any(y => y.rol_id == x.rol_id)))
                    userRole = "rrhh";
            }

            if (tipo == "rrhh" && userRole == "rrhh")
            {
                licencias = db.licencia.Where(
                        l => l.estado == STATUS_APROBADO_JEFE
                            && l.empleado_id != current.empleado_id
                            && ( // TERM SEARCH
                                (l.empleado.nombres.ToLower() + l.empleado.apellidos.ToLower()).Contains(term)
                            )
                    ).OrderByDescending(
                        l => l.fecha_solicitud
                    );
            }
            else if (tipo == "jefe")
            {
                if (userRole == "rrhh")
                {
                    licencias = db.licencia.Where(
                            l => l.estado == STATUS_PENDIENTE
                                && l.empleado_id != current.empleado_id
                                && ( // TERM SEARCH
                                    (l.empleado.nombres.ToLower() + " " + l.empleado.apellidos.ToLower()).Contains(term)
                                )
                        ).OrderByDescending(
                            l => l.fecha_solicitud
                        );
                }
                else
                {
                    licencias = db.licencia.Where(
                            l => (l.empleado.jefe_id == current.empleado_id || l.empleado.jefe.id == current.empleado_id
                                    || l.empleado.jefe.delegado.id == current.empleado_id || l.empleado.jefe.delegado_id == current.empleado_id
                            )
                                && l.empleado_id != current.empleado_id
                                && l.estado == STATUS_PENDIENTE
                                && ( // TERM SEARCH
                                    (l.empleado.nombres.ToLower() + " " + l.empleado.apellidos.ToLower()).Contains(term)
                                )
                        ).OrderByDescending(l => l.fecha_solicitud);

                }
            }

            // SORT
            if (request.orderBy == "estado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.estado);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.estado);
                }
            }
            else if (request.orderBy == "empleado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.nombres).ThenByDescending(l => l.empleado.apellidos);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
            }
            else if (request.orderBy == "fecha_desde")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_inicio);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_inicio);
                }
            }
            else if (request.orderBy == "fecha_hasta")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_fin);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_fin);
                }
            }
            else
            {
                if (request.orderDirection != "desc")
                {
                    licencias = licencias.OrderBy(l => l.fecha_solicitud);
                }
            }
            List<licencia> licenciasList = licencias.ToList();
            PaginationResponseTO<licencia> toResponse = new PaginationResponseTO<licencia>();
            toResponse.page = request.page;
            toResponse.totalCount = licenciasList.Count();
            toResponse.pagesCount = (toResponse.totalCount / request.pageSize) + 1;
            licenciasList = licencias.Skip(rangeStart).Take(request.pageSize).ToList();

            toResponse.rowsFrom = rangeStart;
            toResponse.rowsTo = rangeStart + licencias.Count() - 1;
            toResponse.rows = licenciasList;//licencias.ToList();

            return toResponse;
        }

        public PaginationResponseTO<LicenciaGeneradaTO> getForApprobalAttachments(PaginationRequestTO request, string currentUser)
        {
            int rangeStart = request.pageSize * request.page;
            string term = request.term.Trim().ToLower();
            TipoLicenciaUsuarioService tluService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            usuario current = getCurrentUsuario(currentUser);
            var tipoLicenciaAdjuntoObligatorio = db.tipo_licencia.Where(x => x.adjunto_obligatorio > 0).Select(x => x.id).ToList();

            IQueryable<licencia> licencias = db.licencia
                .Where(x => tipoLicenciaAdjuntoObligatorio.Contains(x.tipo_licencia_id))
                .Where(x => x.estado != STATUS_RECHAZADO)
                //.Where(l => l.adjunto_id != null)
                .Where(l => l.adjunto_estado == null || l.adjunto_estado == STATUS_PENDIENTE);

            if (!string.IsNullOrEmpty(term))
                licencias = licencias.Where(l => (l.empleado.nombres.ToLower() + l.empleado.apellidos.ToLower()).Contains(term));// TERM SEARCH

            // SORT
            if (request.orderBy == "estado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.estado);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.estado);
                }
            }
            else if (request.orderBy == "empleado")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.nombres).ThenByDescending(l => l.empleado.apellidos);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
            }
            else if (request.orderBy == "fecha_desde")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_inicio);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_inicio);
                }
            }
            else if (request.orderBy == "fecha_hasta")
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_fin);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_fin);
                }
            }
            else
            {
                if (request.orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_solicitud);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_solicitud);
                }
            }

            PaginationResponseTO<LicenciaGeneradaTO> toResponse = new PaginationResponseTO<LicenciaGeneradaTO>();
            toResponse.page = request.page;
            toResponse.totalCount = licencias.Count();
            toResponse.pagesCount = (toResponse.totalCount / request.pageSize) + 1;
            licencias = licencias.Skip(rangeStart).Take(request.pageSize);
            List<licencia> licenciaList = licencias.ToList();
            List<LicenciaGeneradaTO> ret = new List<LicenciaGeneradaTO>();
            foreach (licencia l in licenciaList)
            {
                LicenciaGeneradaTO to = new LicenciaGeneradaTO();
                l.CopyProperties(to);

                TipoLicenciaUsuarioTO tipoTo = tluService.getLicenciaFor(l.empleado, l.licencia_periodo_cantidad.LastOrDefault().periodo, l.tipo_licencia);
                to.tipo_licencia_usuario = tipoTo;

                ret.Add(to);
            }

            toResponse.rowsFrom = rangeStart;
            toResponse.rowsTo = rangeStart + ret.Count - 1;
            toResponse.rows = ret;

            return toResponse;
        }

        public PaginationResponseTO<LicenciaHoyTO> getForDate(DateTime date, PaginationRequestTO request)
        {
            int rangeStart = request.pageSize * request.page;

            var licencias = getForDate(date, request.term, request.orderBy, request.orderDirection);

            PaginationResponseTO<LicenciaHoyTO> toResponse = new PaginationResponseTO<LicenciaHoyTO>();
            toResponse.page = request.page;
            toResponse.totalCount = licencias.Count();
            toResponse.pagesCount = (toResponse.totalCount / request.pageSize) + 1;
            licencias = licencias.Skip(rangeStart).Take(request.pageSize);

            List<LicenciaHoyTO> ret = licencias.Select(x => new LicenciaHoyTO
            {
                nombres = x.empleado.nombres,
                apellidos = x.empleado.apellidos,
                fecha_fin = x.fecha_fin,
                fecha_inicio = x.fecha_inicio,
                tipo_licencia = x.tipo_licencia.nombre,
                empresa = x.empleado.empresa.nombre,
                employee_id = x.empleado.employee_id,
                legajo = x.empleado.legajo,
                sector = x.empleado.area.nombre,
                jefe_apellidos = x.empleado.jefe != null ? x.empleado.jefe.apellidos : "",
                jefe_nombres = x.empleado.jefe != null ? x.empleado.jefe.nombres : "",

            }).ToList();
            toResponse.rowsFrom = rangeStart;
            toResponse.rowsTo = rangeStart + ret.Count - 1;
            toResponse.rows = ret;

            return toResponse;
        }



        public IQueryable<licencia> getForDate(DateTime date, string term, string orderBy, string orderDirection)
        {
            term = term.ToLower();

            var start = date;
            var end = start.AddDays(1);
            var licencias = db.licencia.Where(l =>
                (l.fecha_inicio >= start && l.fecha_inicio < end) ||
                (l.fecha_fin >= start && l.fecha_fin < end) ||
                (l.fecha_inicio < start && l.fecha_fin >= end)
            ).Where(x => (x.empleado.nombres.ToLower() + " " + x.empleado.apellidos.ToLower()).Contains(term)
                || x.tipo_licencia.nombre.ToLower().Contains(term)
                || x.empleado.empresa.nombre.ToLower().Contains(term))
            .Where(l => l.estado == "aprobado");

            // SORT
            if (orderBy == "empleado")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.nombres).ThenByDescending(l => l.empleado.apellidos);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
            }
            if (orderBy == "employee_id")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.employee_id);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.employee_id);
                }
            }
            if (orderBy == "legajo")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.legajo);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.legajo);
                }
            }
            if (orderBy == "sector")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.area.nombre);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.area.nombre);
                }
            }
            if (orderBy == "tipo_licencia")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.tipo_licencia.nombre);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.tipo_licencia.nombre);
                }
            }
            else if (orderBy == "fecha_inicio")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_inicio);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_inicio);
                }
            }
            else if (orderBy == "fecha_fin")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.fecha_fin);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.fecha_fin);
                }
            }
            else if (orderBy == "empresa")
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.empresa.nombre);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.empresa.nombre);
                }
            }
            else
            {
                if (orderDirection == "desc")
                {
                    licencias = licencias.OrderByDescending(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
                else
                {
                    licencias = licencias.OrderBy(l => l.empleado.nombres).ThenBy(l => l.empleado.apellidos);
                }
            }
            return licencias;
        }

        public List<CalendarEntryTO> getBetween(long empleado_id, DateTime start, DateTime end)
        {
            List<CalendarEntryTO> ret = new List<CalendarEntryTO>();
            List<licencia> licencias = db.licencia.Where(l => l.empleado_id == empleado_id &&
                ((l.fecha_inicio >= start && l.fecha_inicio <= end) ||
                (l.fecha_fin >= start && l.fecha_fin <= end))
            ).ToList();

            foreach (licencia l in licencias)
            {
                CalendarEntryTO to = new CalendarEntryTO();
                to.id = l.id;
                to.allDay = true;
                to.className = l.tipo_licencia.event_class;
                to.end = l.fecha_fin;
                to.start = l.fecha_inicio;
                to.status = l.estado;
                to.title = l.tipo_licencia.nombre;
                to.comentarios = l.notas_empleado;
                to.cantidad = l.cantidad;
                to.adjunto_id = l.adjunto_id;
                to.adjunto_nombre = l.adjunto_nombre;
                to.adjunto_estado = l.adjunto_estado;
                //TODO: HACK, puede quedar
                to.end = to.end.AddDays(1);

                ret.Add(to);
            }

            return ret;
        }

        public licencia find(long? id)
        {
            return db.licencia.Find(id);
        }

        private licencia doSaveOrUpdate(licencia l)
        {
            TipoLicenciaUsuarioService tipoLicenciaUsuarioService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);
            if (l.id > 0)
            {
                db.Entry(l).State = EntityState.Modified;
            }
            else
            {
                l = db.licencia.Add(l);
            }
            save();

            // QUITAR ENTRADAS DE CANTIDAD ANTIGUAS
            List<licencia_periodo_cantidad> viejas = db.licencia_periodo_cantidad.Where(
                pc => pc.licencia_id == l.id
            ).ToList();
            foreach (licencia_periodo_cantidad lpc in viejas)
            {
                db.licencia_periodo_cantidad.Remove(lpc);
            }

            return l;
        }

        public licencia saveOrUpdate(licencia l)
        {
            TipoLicenciaUsuarioService tipoLicenciaUsuarioService = (TipoLicenciaUsuarioService)new TipoLicenciaUsuarioService().setDatabase(db);

            // SETEAR LAS CANTIDADES POR PERIODO
            tipo_licencia tipo = db.tipo_licencia.Find(l.tipo_licencia_id);
            if (tipo.acumulable == 0)
            {
                periodo per = getPeriodo(l);
                
                if (per == null)
                {
                    throw new Exception("Periodo inexistente");
                }
                l = doSaveOrUpdate(l);
                licencia_periodo_cantidad lpc = new licencia_periodo_cantidad();
                lpc.licencia_id = l.id;
                lpc.periodo_id = per.id;
                lpc.cantidad = l.cantidad;
                db.licencia_periodo_cantidad.Add(lpc);
            }
            else
            {
                l = doSaveOrUpdate(l);
                int cantidadAAsignar = l.cantidad;
                empleado e = db.empleado.Find(l.empleado_id);
                var limiteAcumulacion = e.fechaIngreso.FechaAcumulacionLimite();
                List<periodo> periodos = db.periodo.Where(
                    p => p.fechaFin > limiteAcumulacion
                ).OrderBy(p => p.fechaInicio).ToList();
                foreach (periodo p in periodos)
                {
                    TipoLicenciaUsuarioTO to = tipoLicenciaUsuarioService.getLicenciaFor(e, p, tipo, l);
                    if (to.cantidadDisponible > 0)
                    {
                        licencia_periodo_cantidad lpc = new licencia_periodo_cantidad();
                        lpc.licencia_id = l.id;
                        lpc.periodo_id = p.id;
                        if (cantidadAAsignar > to.cantidadDisponible)
                        {
                            lpc.cantidad = to.cantidadDisponible;
                            cantidadAAsignar -= to.cantidadDisponible;
                            db.licencia_periodo_cantidad.Add(lpc);
                        }
                        else
                        {
                            lpc.cantidad = cantidadAAsignar;
                            db.licencia_periodo_cantidad.Add(lpc);
                            break;
                        }
                    }
                }
            }

            save();

            return l;
        }

        private periodo getPeriodo(licencia licencia)
        {
            tipo_licencia tipo = db.tipo_licencia.FirstOrDefault(tl => tl.simbolo == "saldo-2018");
            if (licencia.tipo_licencia_id == tipo.id)
            {
                return db.periodo.FirstOrDefault(p => p.nombre == "2021");
            }
            return db.periodo.FirstOrDefault(p => p.fechaInicio <= licencia.fecha_inicio
                                                  && p.fechaFin >= licencia.fecha_inicio);
        }

        public void delete(long id)
        {
            licencia l = db.licencia.Find(id);

            List<licencia_periodo_cantidad> viejas = db.licencia_periodo_cantidad.Where(
                pc => pc.licencia_id == l.id
            ).ToList();
            foreach (licencia_periodo_cantidad lpc in viejas)
            {
                db.licencia_periodo_cantidad.Remove(lpc);
            }

            db.licencia.Remove(l);
            save();
        }
        private bool EsRRHH(usuario current)
        {
            return current.usuario_rol.Select(ur => ur.rol.nombre.ToLower()).Contains("rrhh");
        }
        private void validarLicencia(licencia l, usuario current, bool rrhh)
        {
            if (rrhh && current.empleado_id == l.empleado_id)
            {
                l.estado = STATUS_APROBADO;
                l.aprobador_rrhh_id = current.empleado_id;
                l.aprobador_rrhh_fecha = DateTime.Now;
                l.aprobador_rrhh_notas = l.notas_empleado;
                l.notas_empleado = null;
            }

            l.tipo_licencia_id = l.tipo_licencia.id;
            l.tipo_licencia = null;

            if (esMedioRP(l))
            {
                validarMedioRP(l);
            }
            else
            {
                //EVITAR LICENCIAS EN MISMA FECHA
                List<licencia> licencias = db.licencia.Where(
                lic => lic.empleado_id == l.empleado_id
                    && lic.id != l.id
                    && lic.estado != STATUS_RECHAZADO
                    && (
                        (
                            (lic.fecha_inicio >= l.fecha_inicio && lic.fecha_inicio <= l.fecha_fin)
                            || (lic.fecha_fin >= l.fecha_inicio && lic.fecha_fin <= l.fecha_fin)
                        )
                        ||
                        (
                            (l.fecha_inicio >= lic.fecha_inicio && l.fecha_inicio <= lic.fecha_fin)
                            || (l.fecha_fin >= lic.fecha_inicio && l.fecha_fin <= lic.fecha_fin)
                        )
                    )
            ).ToList();
                if (licencias != null && licencias.Count > 0)
                {
                    throw new Exception("Ya existe otra licencia en esas fechas");
                }
            }

        }
        private void verificarCantidadDias(licencia l)
        {
            var feriados = (db.feriado.Where(f =>
                    l.fecha_inicio <= f.fecha
                    && f.fecha <= l.fecha_fin)
                .Select(f => f.fecha)).ToList();
            feriados = feriados.Where(f => f.DayOfWeek != DayOfWeek.Saturday 
                                           && f.DayOfWeek != DayOfWeek.Sunday).ToList();
            l.cantidad = (int)(l.fecha_fin - l.fecha_inicio).TotalDays + 1;
            DateTime temp = l.fecha_inicio;

            while (temp <= l.fecha_fin)
            {
                if (temp.DayOfWeek == DayOfWeek.Saturday || temp.DayOfWeek == DayOfWeek.Sunday)
                {
                    l.cantidad -= 1;
                }
                temp = temp.AddDays(1);
            }
            l.cantidad -= feriados.Count;

            if (l.fecha_inicio.DayOfWeek == DayOfWeek.Saturday)
                l.fecha_inicio = l.fecha_inicio.AddDays(2);
            else if (l.fecha_inicio.DayOfWeek == DayOfWeek.Sunday)
                l.fecha_inicio = l.fecha_inicio.AddDays(1);
            if (l.fecha_fin.DayOfWeek == DayOfWeek.Saturday)
                l.fecha_fin = l.fecha_fin.AddDays(-1);
            else if (l.fecha_fin.DayOfWeek == DayOfWeek.Sunday)
                l.fecha_fin = l.fecha_fin.AddDays(-2);
            if (l.cantidad <= 0)
            {
                throw new Exception("No hay días hábiles en el rango de fechas elegido");
            }
        }

        private void sendEmailJefe(licencia l, bool rrhh, Action<empleado, empleado, string, string, holando_rrhh_licenciasEntities> emailFunc)
        {
            try
            {
                EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
                if (!rrhh)
                {
                    empleado emp = empleadoService.find(l.empleado_id);
                    empleado jefe = empleadoService.find(emp.jefe_id);

                    if (jefe != null && jefe.delegado_id > 0)
                    {
                        empleado delegado = empleadoService.find(jefe.delegado_id);
                        emailFunc(delegado, emp, l.notas_empleado, l.tipo_licencia.nombre, db);
                        //EmailHelper.sendMailAlertJefe(delegado, emp, l.notas_empleado, l.tipo_licencia.nombre, db);
                    }
                    else
                    {
                        emailFunc(jefe, emp, l.notas_empleado, l.tipo_licencia.nombre, db);
                        //EmailHelper.sendMailAlertJefe(jefe, emp, l.notas_empleado, l.tipo_licencia.nombre, db);
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("ERROR ENVIANDO MAIL: " + e.Message);
            }
        }

        private void sendEmailAdjuntoEdit(licencia l, bool rrhh)
        {
            sendEmailJefe(l, rrhh, EmailHelper.sendMailAlertAdjunto);
        }

        private void sendEmailModificacion(licencia l, bool rrhh)
        {
            sendEmailJefe(l, rrhh, EmailHelper.sendMailAlertJefe);
        }
        private void sendEmailEmpleadoYJefe(licencia l, bool rrhh)
        {
            try
            {
                EmpleadoService empleadoService = (EmpleadoService)new EmpleadoService().setDatabase(db);
                if (rrhh)
                {
                    empleado emp = empleadoService.find(l.empleado_id);
                    empleado jefe = empleadoService.find(emp.jefe_id);
                    
                    if (jefe != null && jefe.delegado_id > 0)
                    {
                        empleado delegado = empleadoService.find(jefe.delegado_id);
                        EmailHelper.sendMailAlertEmpleadoYJefe(delegado, emp, l.aprobador_rrhh_notas, l.tipo_licencia.nombre, db);
                    }
                    else
                    {
                        EmailHelper.sendMailAlertEmpleadoYJefe(jefe, emp, l.aprobador_rrhh_notas, l.tipo_licencia.nombre, db);
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("ERROR ENVIANDO MAIL EMPLEADO Y JEFE: " + e.Message);
            }
        }
    }
}