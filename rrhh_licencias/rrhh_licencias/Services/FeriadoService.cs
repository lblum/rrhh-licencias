﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
	public class FeriadoService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(FeriadoService));

		public PaginationResponseTO<feriado> getAll(PaginationRequestTO request)
		{
			int rangeStart = request.pageSize * request.page;
			string term = request.term.ToLower();


			List<feriado> ret = db.feriado.Where(
				r => r.nombre.ToLower().Contains(term) ||
					r.simbolo.ToLower().Contains(term)
			).ToList();


			// SORT
			if (request.orderBy == "nombre")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.nombre).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.nombre).ToList();
				}
			}
			else if (request.orderBy == "fecha")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.fecha).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.fecha).ToList();
				}
			}
			else if (request.orderBy == "simbolo")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.simbolo).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.simbolo).ToList();
				}
			}

			PaginationResponseTO<feriado> toResponse = new PaginationResponseTO<feriado>();
			toResponse.page = request.page;
			toResponse.totalCount = ret.Count;
			toResponse.pagesCount = (ret.Count / request.pageSize) + 1;

			ret = ret.Skip(rangeStart).Take(request.pageSize).ToList();
			toResponse.rowsFrom = rangeStart;
			toResponse.rowsTo = rangeStart + ret.Count - 1;

			toResponse.rows = ret;

			return toResponse;
		}

		public List<feriado> getAll()
		{
			return db.feriado.ToList();
		}

		public feriado find(long? id)
		{
			return db.feriado.Find(id);
		}

		public feriado saveOrUpdate(feriado f)
		{
			if (f.id > 0)
			{
				db.Entry(f).State = EntityState.Modified;
			}
			else
			{
				f = db.feriado.Add(f);
			}
			save();

			return f;
		}

		public void delete(long id)
		{
			feriado f = db.feriado.Find(id);
			db.feriado.Remove(f);
			save();
		}
	}
}