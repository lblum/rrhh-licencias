﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
	public class AreaService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(AreaService));

		public PaginationResponseTO<area> getAll(PaginationRequestTO request)
		{
			int rangeStart = request.pageSize * request.page;
			string term = request.term.ToLower();


			List<area> ret = db.area.Where(
				r => r.nombre.ToLower().Contains(term)
			).ToList();


			// SORT
			if (request.orderBy == "nombre")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.nombre).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.nombre).ToList();
				}
			}

			PaginationResponseTO<area> toResponse = new PaginationResponseTO<area>();
			toResponse.page = request.page;
			toResponse.totalCount = ret.Count;
			toResponse.pagesCount = (ret.Count / request.pageSize) + 1;

			ret = ret.Skip(rangeStart).Take(request.pageSize).ToList();
			toResponse.rowsFrom = rangeStart;
			toResponse.rowsTo = rangeStart + ret.Count - 1;

			toResponse.rows = ret;

			return toResponse;
		}

		public List<area> getAll()
		{
			return db.area.ToList();
		}

		public area find(long? id)
		{
			return db.area.Find(id);
		}

		public area saveOrUpdate(area a)
		{
			if (a.id > 0)
			{
				db.Entry(a).State = EntityState.Modified;
			}
			else
			{
				a = db.area.Add(a);
			}
			save();

			return a;
		}

		public void delete(long id)
		{
			area a = db.area.Find(id);
			db.area.Remove(a);
			save();
		}
	}
}