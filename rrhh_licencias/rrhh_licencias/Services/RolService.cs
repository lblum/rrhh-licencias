﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace rrhh_licencias.Services
{
	public class RolService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(RolService));

		public PaginationResponseTO<rol> getAll(PaginationRequestTO request)
		{
			int rangeStart = request.pageSize * request.page;
			string term = request.term.ToLower();


			List<rol> ret = this.db.rol.Where(
				r => r.nombre.ToLower().Contains(term)
			).ToList();


			// SORT
			if (request.orderBy == "nombre")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.nombre).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.nombre).ToList();
				}
			}

			PaginationResponseTO<rol> toResponse = new PaginationResponseTO<rol>();
			toResponse.page = request.page;
			toResponse.totalCount = ret.Count;
			toResponse.pagesCount = (ret.Count / request.pageSize) + 1;

			ret = ret.Skip(rangeStart).Take(request.pageSize).ToList();
			toResponse.rowsFrom = rangeStart;
			toResponse.rowsTo = rangeStart + ret.Count - 1;

			toResponse.rows = ret;

			return toResponse;
		}

		public List<rol> getAll()
		{
			return this.db.rol.ToList();
		}

		public rol find(long? id)
		{
			return this.db.rol.Find(id);
		}

		public rol saveOrUpdate(rol r)
		{
			if (r.id > 0)
			{
				db.Entry(r).State = EntityState.Modified;
			}
			else
			{
				r = this.db.rol.Add(r);
			}
			this.save();

			return r;
		}

		public rol_permiso saveOrUpdatePermiso(rol_permiso rp)
		{
			if (rp.id > 0)
			{
				db.Entry(rp).State = EntityState.Modified;
			}
			else
			{
				rp = this.db.rol_permiso.Add(rp);
			}
			this.save();

			return rp;
		}

		public void deleteNotIn(long rol_id, rol_permiso[] parentPermisos)
		{
			List<rol_permiso> permisos = this.db.rol.Find(rol_id).rol_permiso.ToList();

			foreach (rol_permiso rp in permisos)
			{
				bool found = false;
				foreach (rol_permiso prp in parentPermisos)
				{
					if (rp.id == prp.id) found = true;
				}
				if (found) continue;

				this.db.rol_permiso.Remove(rp);
			}
			this.save();
		}

		public void deletePermisos(long id)
		{
			List<rol_permiso> rps = this.db.rol_permiso.Where(rp => rp.rol_id == id).ToList();
			foreach (rol_permiso rp in rps)
			{
				this.db.rol_permiso.Remove(rp);
			}

			this.save();
		}

		public void delete(long id)
		{
			rol r = this.db.rol.Find(id);
			this.db.rol.Remove(r);
			this.save();
		}
	}
}