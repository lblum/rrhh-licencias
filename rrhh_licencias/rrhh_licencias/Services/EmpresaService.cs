﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using rrhh_licencias.Models;

namespace rrhh_licencias.Services
{
	public class EmpresaService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(EmpresaService));

		public List<empresa> getAll()
		{
			return db.empresa.ToList();
		}

		public empresa find(long? id)
		{
			return db.empresa.Find(id);
		}
	}
}