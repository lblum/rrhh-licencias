﻿using System.Linq;
using System.Web.Security;
using log4net;
using rrhh_licencias.Exceptions;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
    public class LoginService : BaseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LoginService));
        
        public usuario loginBasic(LoginTO login)
        {
            usuario user = db.usuario.FirstOrDefault(u => u.login == login.usuario);
            if (user != null && login.password == user.password)
            {
                return user;
            }
            throw new WrongUserOrPasswordException();
        }
        public bool checkUser(string nombre, string password)
        {
            usuario user = db.usuario.FirstOrDefault(u => u.login == nombre && u.password == password);
            return user != null;
        }
        public void logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}