﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DocumentFormat.OpenXml.VariantTypes;
using log4net;
using rrhh_licencias.Helper;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;

namespace rrhh_licencias.Services
{
    public class TipoLicenciaUsuarioService : BaseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TipoLicenciaUsuarioService));

        public List<TipoLicenciaUsuarioTO> getVacacionesFor(empleado e)
        {
            List<TipoLicenciaUsuarioTO> result = new List<TipoLicenciaUsuarioTO>();
            tipo_licencia tl = db.tipo_licencia.FirstOrDefault(t => t.simbolo == "event-vacaciones");

            var limiteAcumulacion = e.fechaIngreso.FechaAcumulacionLimite();
            List<periodo> periodos = db.periodo
                .Where(p => p.fechaFin > limiteAcumulacion)
                .OrderByDescending(p => p.fechaInicio)
                .ToList();
            foreach (periodo p in periodos)
            {
                TipoLicenciaUsuarioTO to = getLicenciaFor(e, p, tl);
                if (to.cantidadDisponible > 0)
                    result.Add(to);
            }

            return result;
        }
        public List<TipoLicenciaUsuarioTO> getLicenciasSaldosFor(empleado e)
        {
            List<TipoLicenciaUsuarioTO> result = new List<TipoLicenciaUsuarioTO>();
            tipo_licencia vacaciones = db.tipo_licencia
                .FirstOrDefault(t => t.simbolo == "event-vacaciones");


            var limiteAcumulacion = e.fechaIngreso.FechaAcumulacionLimite();
            List<periodo> periodos = db.periodo
                .Where(p => p.fechaFin > limiteAcumulacion)
                .OrderByDescending(p => p.fechaInicio)
                .ToList();
            foreach (periodo p in periodos)
            {
                TipoLicenciaUsuarioTO to = getLicenciaFor(e, p, vacaciones);
                if (to.cantidadDisponible > 0)
                {
                    result.Add(to);
                }
            }
            string[] tipos_licencia_saldos = ConfigurationManager.AppSettings["TIPO_LICENCIAS_SALDOS"].Split(',');
            List<tipo_licencia> tls = db.tipo_licencia.Where(t => tipos_licencia_saldos.Contains(t.simbolo)).ToList();
            periodo periodo_actual = db.periodo.FirstOrDefault(
                    periodo => periodo.fechaInicio <= DateTime.Today
                    && periodo.fechaFin >= DateTime.Today);
            foreach (tipo_licencia tl in tls)
            {
                TipoLicenciaUsuarioTO to = getLicenciaFor(e, periodo_actual, tl);
                if (to.cantidadDisponible > 0)
                {
                    result.Add(to);
                }
            }
            return result;
        }
        public List<TipoLicenciaUsuarioTO> getLicenciasFor(empleado e, periodo p)
        {
            List<TipoLicenciaUsuarioTO> result = new List<TipoLicenciaUsuarioTO>();
            List<tipo_licencia> tipos_licencia = db.tipo_licencia.ToList();

            foreach (tipo_licencia rcl in tipos_licencia)
            {
                TipoLicenciaUsuarioTO to = getLicenciaFor(e, p, rcl);

                result.Add(to);
            }
            return result;
        }

        public TipoLicenciaUsuarioTO getLicenciaFor(empleado e, periodo p, tipo_licencia rcl)
        {
            return getLicenciaFor(e, p, rcl, null);
        }

        public TipoLicenciaUsuarioTO getLicenciaFor(empleado e, periodo p, tipo_licencia tl, licencia toIgnore)
        {
            int vacacionesForA = int.Parse(ConfigurationManager.AppSettings["VACACIONES_FOR_A"]);
            int vacacionesForB = int.Parse(ConfigurationManager.AppSettings["VACACIONES_FOR_B"]);
            int vacacionesForC = int.Parse(ConfigurationManager.AppSettings["VACACIONES_FOR_C"]);
            int vacacionesForElse = int.Parse(ConfigurationManager.AppSettings["VACACIONES_FOR_ELSE"]);

            int vacacionesDaysForWorkDays = int.Parse(ConfigurationManager.AppSettings["VACACIONES_DAYS_FOR_WORK_DAYS"]);

            int vacacionesMonth20X1 = int.Parse(ConfigurationManager.AppSettings["VACACIONES_MONTH_20_X_1"]);
            int vacacionesYearsUntilA = int.Parse(ConfigurationManager.AppSettings["VACACIONES_YEARS_UNTIL_A"]);
            int vacacionesYearsUntilB = int.Parse(ConfigurationManager.AppSettings["VACACIONES_YEARS_UNTIL_B"]);
            int vacacionesYearsUntilC = int.Parse(ConfigurationManager.AppSettings["VACACIONES_YEARS_UNTIL_C"]);


            string vacacionesAvailableFrom = ConfigurationManager.AppSettings["VACACIONES_AVAILABLE_FROM"];
            string[] availableFromArray = vacacionesAvailableFrom.Split('-');
            int vacacionesAvailableFromDay = int.Parse(availableFromArray[0]);
            int vacacionesAvailableFromMonth = int.Parse(availableFromArray[1]);

            int aniosAcumulacion = int.Parse(ConfigurationManager.AppSettings["LIMITE_ACUMULACION"]);
            int _cantidad_acordada = -1;
            int _cantidad_utilizada = 0;
            int _cantidad_disponible = 0;

            DateTime fechaIngreso = e.fechaIngreso;
            tipo_licencia rcl = new tipo_licencia();
            tl.CopyProperties(rcl);
            //Obtener condiciones especiales
            List<condicion_especial> _condiciones_especiales = db.condicion_especial.Where(
                ce => ce.empleado_id == e.id
                    && ce.periodo_id == p.id
                    && ce.tipo_licencia_id == rcl.id
            ).ToList();

            foreach (condicion_especial ce in _condiciones_especiales)
            {
                if (ce.cantidad_acordada.HasValue && ce.cantidad_acordada.Value > 0)
                    _cantidad_acordada = ce.cantidad_acordada.Value;
            }
            if (rcl.simbolo == "event-vacaciones")
            {
                DateTime today = DateTime.Now;
                int acumulacion = today.Year - aniosAcumulacion;
                int cantidadLegal = 0;
                int cantidadDeMesesActual = Math.Abs(12 * (p.fechaFin.Year - fechaIngreso.Year) + p.fechaFin.Month - fechaIngreso.Month);
                int since = (cantidadDeMesesActual + e.antiguedad) / 12;

                bool periodoMesValido = (today.Month == vacacionesAvailableFromMonth
                        && today.Day < vacacionesAvailableFromDay)
                        || today.Month < vacacionesAvailableFromMonth;

                if (p.fechaFin.Year > today.Year
                    || (p.fechaFin.Year == today.Year && periodoMesValido)
                    || (p.fechaFin.Year == acumulacion && !periodoMesValido))
                {
                    cantidadLegal = -1;
                }
                else if (p.fechaFin.Year == fechaIngreso.Year
                         && fechaIngreso.Month >= vacacionesMonth20X1)
                {
                    int days = p.fechaFin.Subtract(fechaIngreso).Days;
                    var x = days % vacacionesDaysForWorkDays;
                    cantidadLegal = days % vacacionesDaysForWorkDays == 0
                        ? (days / vacacionesDaysForWorkDays)
                        : (days / vacacionesDaysForWorkDays) + 1;
                    if (cantidadLegal >= vacacionesForA)
                    {
                        cantidadLegal = vacacionesForA;
                    }
                }
                else if (since < vacacionesYearsUntilA)
                {
                    cantidadLegal = vacacionesForA;
                }
                else if (since < vacacionesYearsUntilB) // despues de 5 años
                {
                    cantidadLegal = vacacionesForB;
                }
                else if (since < vacacionesYearsUntilC) // despues de 10 años
                {
                    cantidadLegal = vacacionesForC;
                }
                else  // después de 15 años
                {
                    cantidadLegal = vacacionesForElse;
                }

                if (cantidadLegal < 0)
                {
                    rcl.cantidad = 0;
                }
                else if (_cantidad_acordada >= cantidadLegal)
                {
                    rcl.cantidad = _cantidad_acordada;
                }
                else
                {
                    rcl.cantidad = cantidadLegal;
                }
                //Trace.WriteLine("---------------------->Periodo:" + p.nombre + " - CantidadLegal:" + cantidadLegal + " - Acordada:" + _cantidad_acordada + " - Cantidad:" + rcl.cantidad);
            }

            if (_cantidad_acordada > rcl.cantidad)
            {
                rcl.cantidad = _cantidad_acordada;
            }

            //Obtener las licencias
            _cantidad_utilizada = 0;
            IQueryable<licencia_periodo_cantidad> _licencias = db.licencia_periodo_cantidad.Where(
                l => l.licencia.empleado_id == e.id
                && l.periodo_id == p.id
                && l.licencia.tipo_licencia_id == rcl.id
                && l.licencia.estado != LicenciaService.STATUS_RECHAZADO
            );
            foreach (licencia_periodo_cantidad lpc in _licencias)
            {
                if (ReferenceEquals(null, toIgnore) || lpc.licencia_id != toIgnore.id)
                {
                    _cantidad_utilizada += lpc.cantidad;
                }
            }
            _cantidad_disponible = Math.Max(0, rcl.cantidad - _cantidad_utilizada);

            TipoLicenciaUsuarioTO to = new TipoLicenciaUsuarioTO
            {
                id = rcl.id,
                simbolo = rcl.simbolo,
                cantidadOriginal = rcl.cantidad,
                tipoLicencia = rcl.descripcion,
                cantidadUtilizada = _cantidad_utilizada,
                cantidadDisponible = _cantidad_disponible,
                cantidadEspecial = _cantidad_acordada,
                nombre = rcl.nombre,
                empleado_id = e.id,
                periodo_id = p.id,
                periodo = p,
                empleado = e,
                adjuntoObligatorio = rcl.adjunto_obligatorio == 1
            };
            return to;
        }
    }
}