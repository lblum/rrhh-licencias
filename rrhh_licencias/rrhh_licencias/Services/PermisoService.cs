﻿using log4net;
using rrhh_licencias.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace rrhh_licencias.Services
{
	public class PermisoService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PermisoService));

		public List<permiso> getAll()
		{
			return this.db.permiso.ToList();
		}

		public permiso find(long? id)
		{
			return this.db.permiso.Find(id);
		}

		public permiso saveOrUpdate(permiso p)
		{
			if (p.id > 0)
			{
				db.Entry(p).State = EntityState.Modified;
			}
			else
			{
				p = this.db.permiso.Add(p);
			}
			this.save();

			return p;
		}

		public void delete(long id)
		{
			permiso p = this.db.permiso.Find(id);
			this.db.permiso.Remove(p);
			this.save();
		}
	}
}