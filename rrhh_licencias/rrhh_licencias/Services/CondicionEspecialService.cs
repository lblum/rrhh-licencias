﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using log4net;
using rrhh_licencias.Models;

namespace rrhh_licencias.Services
{
	public class CondicionEspecialService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(CondicionEspecialService));

		public List<condicion_especial> getAll()
		{
			return db.condicion_especial.ToList();
		}

		public condicion_especial find(long? id)
		{
			return db.condicion_especial.Find(id);
		}

		public condicion_especial saveOrUpdate(condicion_especial ce)
		{
			if (ce.id > 0)
			{
				db.Entry(ce).State = EntityState.Modified;
			}
			else
			{
				ce = db.condicion_especial.Add(ce);
			}
			save();

			return ce;
		}

		public condicion_especial setCondicionEspecial(empleado e, periodo p, tipo_licencia rcl, int cantidad, DateTime ingresoEspecial)
		{
			condicion_especial ce = db.condicion_especial.FirstOrDefault(c => c.empleado_id == e.id && c.periodo_id == p.id && c.tipo_licencia_id == rcl.id);
			if (ce == null || ce.id == 0)
			{
				ce = new condicion_especial();
				ce.empleado_id = e.id;
				ce.periodo_id = p.id;
				ce.tipo_licencia_id = rcl.id;
			}
			ce.cantidad_acordada = cantidad;
			ce.ingreso_acordado = ingresoEspecial;

			return saveOrUpdate(ce);
		}

		public void delete(long id)
		{
			condicion_especial ce = db.condicion_especial.Find(id);
			db.condicion_especial.Remove(ce);
			save();
		}
	}
}