﻿using log4net;
using rrhh_licencias.Models;
using rrhh_licencias.TransferObjects;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace rrhh_licencias.Services
{
	public class TipoLicenciaService : BaseService
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TipoLicenciaService));

		public PaginationResponseTO<tipo_licencia> getAll(PaginationRequestTO request)
		{
			int rangeStart = request.pageSize * request.page;
			string term = request.term.ToLower();


			List<tipo_licencia> ret = this.db.tipo_licencia.Where(
				r => r.nombre.ToLower().Contains(term) ||
					r.simbolo.ToLower().Contains(term) ||
					r.event_class.ToLower().Contains(term)
			).ToList();


			// SORT
			if (request.orderBy == "nombre")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.nombre).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.nombre).ToList();
				}
			}
			else if (request.orderBy == "evento")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.event_class).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.event_class).ToList();
				}
			}
			else if (request.orderBy == "simbolo")
			{
				if (request.orderDirection == "desc")
				{
					ret = ret.OrderByDescending(l => l.simbolo).ToList();
				}
				else
				{
					ret = ret.OrderBy(l => l.simbolo).ToList();
				}
			}

			PaginationResponseTO<tipo_licencia> toResponse = new PaginationResponseTO<tipo_licencia>();
			toResponse.page = request.page;
			toResponse.totalCount = ret.Count;
			toResponse.pagesCount = (ret.Count / request.pageSize) + 1;

			ret = ret.Skip(rangeStart).Take(request.pageSize).ToList();
			toResponse.rowsFrom = rangeStart;
			toResponse.rowsTo = rangeStart + ret.Count - 1;

			toResponse.rows = ret;

			return toResponse;
		}

		public List<tipo_licencia> getAll()
		{
			return this.db.tipo_licencia.ToList();
		}

		public tipo_licencia find(long? id)
		{
			// List<condicion_especial> cp = new List<condicion_especial>;                      
			return this.db.tipo_licencia.Find(id);
		}

		public tipo_licencia saveOrUpdate(tipo_licencia tl)
		{
			
            if (tl.id > 0)
			{
				db.Entry(tl).State = EntityState.Modified;
			}
			else
            {
                tl.id = this.db.tipo_licencia.OrderByDescending(u => u.id).FirstOrDefault().id + 1;
                tl = this.db.tipo_licencia.Add(tl);
			}
			this.save();

			return tl;
		}

		public void delete(long id)
		{
			tipo_licencia tl = this.db.tipo_licencia.Find(id);
			this.db.tipo_licencia.Remove(tl);
			this.save();
		}
	}
}