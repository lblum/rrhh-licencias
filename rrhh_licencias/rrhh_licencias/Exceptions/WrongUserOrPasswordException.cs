﻿using System;

namespace rrhh_licencias.Exceptions
{
	public class WrongUserOrPasswordException : Exception
	{
		public WrongUserOrPasswordException()
			: base("Usuario o contraseña inválido")
		{

		}
	}
}