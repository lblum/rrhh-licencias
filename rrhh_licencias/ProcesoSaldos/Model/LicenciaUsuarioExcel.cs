﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesoSaldos.Model {
    public class LicenciaUsuarioExcel {
        public string ID_EMPRESA { get; set; }
        public string X_EMPRESA { get; set; }
        public string ID_EMPLEADO { get; set; }
        public string X_LEGAJO { get; set; }
        public string X_APELLIDO { get; set; }
        public string X_NOMBRE { get; set; }
        public string ID_CONCEPTO_VAC { get; set; }
        public string XL_CONCEPTO_VAC { get; set; }
        public int AA_VAC { get; set; }
        public float K_DIAS { get; set; }
        public float? K_1_2_DIAS {get; set;}
        public float K_DIAS_PENDIENTES { get; set; }
        public float? K_1_2_DIAS_PENDIENTES {get; set;}
        public DateTime? FECHA_INICIO { get; set; }
        public DateTime? FECHA_FIN { get; set; }
        public float CANT_DIAS_TOMADOS { get; set; }
        public int? CANT_1_2_DIAS_TOMADOS { get; set; }
    }
}