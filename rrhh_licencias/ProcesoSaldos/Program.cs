﻿using System;
using System.Collections.Generic;
using ProcesoSaldos.ExcelHelper;
using ProcesoSaldos.Model;

namespace ProcesoSaldos
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LicenciaUsuarioExcel> licencias = ClosedXMLHelper.ProcessExcel("saldos.xlsx");
            LicenciaHelper.ProcessLicencias(licencias);
            Console.WriteLine("finalizado");
            Console.ReadKey();
        }
    }
}
