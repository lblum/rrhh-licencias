﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClosedXML.Excel;
using ProcesoSaldos.Model;

namespace ProcesoSaldos.ExcelHelper
{
    public static class ClosedXMLHelper
    {
        public static List<LicenciaUsuarioExcel> ProcessExcel(string filename)
        {
            var workbook = new XLWorkbook(filename);
            var ws1 = workbook.Worksheet(1);
            var rows = ws1.RowsUsed();
            List<LicenciaUsuarioExcel> licencias = new List<LicenciaUsuarioExcel>();
            int i = 1;
            foreach (IXLRow row in rows)
            {
                Console.Write($"procesando registro: {i}");
                if (!row.Cell(1).Value.ToString().StartsWith("ID_EMPRESA"))
                {
                    LicenciaUsuarioExcel licencia = new LicenciaUsuarioExcel
                    {
                        ID_EMPRESA = row.Cell(1).Value.ToString(),
                        X_EMPRESA = row.Cell(2).Value.ToString(),
                        ID_EMPLEADO = row.Cell(3).Value.ToString(),
                        X_LEGAJO = row.Cell(4).Value.ToString(),
                        X_APELLIDO = row.Cell(5).Value.ToString(),
                        X_NOMBRE = row.Cell(6).Value.ToString(),
                        ID_CONCEPTO_VAC = row.Cell(7).Value.ToString(),
                        XL_CONCEPTO_VAC = row.Cell(8).Value.ToString(),
                        AA_VAC = row.Cell(9).Value.CastTo<int>(),
                        K_DIAS = float.Parse(row.Cell(10).Value.ToString())
                    };
                    try
                    {
                        licencia.K_1_2_DIAS = row.Cell(11).Value?.CastTo<float>();
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.K_DIAS_PENDIENTES = row.Cell(12).Value.CastTo<float>();
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.K_1_2_DIAS_PENDIENTES = row.Cell(13)?.Value.CastTo<float>();
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.FECHA_INICIO = DateTime.FromOADate(double.Parse(row.Cell(14)?.Value.ToString()));
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.FECHA_FIN = DateTime.FromOADate(double.Parse(row.Cell(15)?.Value.ToString()));
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.CANT_DIAS_TOMADOS = row.Cell(16).Value.CastTo<float>();
                    }
                    catch
                    {
                        //nothing
                    }
                    try
                    {
                        licencia.CANT_1_2_DIAS_TOMADOS = row.Cell(17).Value?.CastTo<int>();
                    }
                    catch
                    {
                        //nothing
                    }
                    licencias.Add(licencia);
                    Console.WriteLine($": {licencia.X_NOMBRE} {licencia.X_APELLIDO} {licencia.ID_CONCEPTO_VAC}");
                }
                else
                {
                    Console.WriteLine(": header");
                }
                i++;
            }

            return licencias.OrderBy(l => l.X_LEGAJO).ThenBy(l => l.AA_VAC).ToList();
        }

        public static void GenerateExcelError(List<LicenciaUsuarioExcel> errores)
        {
            List<object> headers = new List<object>()
            {
                "ID_EMPRESA",
                "X_EMPRESA",
                "ID_EMPLEADO",
                "X_LEGAJO",
                "X_APELLIDO",
                "X_NOMBRE",
                "ID_CONCEPTO_VAC",
                "XL_CONCEPTO_VAC",
                "AA_VAC",
                "K_DIAS",
                "K_1_2_DIAS",
                "K_DIAS_PENDIENTES",
                "K_1_2_DIAS_PENDIENTES",
                "FECHA_INICIO",
                "FECHA_FIN",
                "CANT_DIAS_TOMADOS",
                "CANT_1_2_DIAS_TOMADOS"
            };
            using (XLWorkbook workbook = new XLWorkbook())
            {
                IXLWorksheet ws = workbook.Worksheets.Add("errores");
                IXLRow row = ws.FirstRow();
                WriteRowData(row, headers);
                DateTime fechaDefault = new DateTime(DateTime.Now.Year, 1, 1);
                foreach (LicenciaUsuarioExcel error in errores)
                {

                    List<object> errorList = new List<object>()
                    {
                        error.ID_EMPRESA,
                        error.X_EMPRESA,
                        error.ID_EMPLEADO,
                        error.X_LEGAJO,
                        error.X_APELLIDO,
                        error.X_NOMBRE,
                        error.ID_CONCEPTO_VAC,
                        error.XL_CONCEPTO_VAC,
                        error.AA_VAC,
                        error.K_DIAS,
                        error.K_1_2_DIAS ?? 0,
                        error.K_DIAS_PENDIENTES,
                        error.K_1_2_DIAS_PENDIENTES ?? 0,
                        error.FECHA_INICIO ?? fechaDefault,
                        error.FECHA_FIN ?? fechaDefault,
                        error.CANT_DIAS_TOMADOS,
                        error.CANT_1_2_DIAS_TOMADOS ?? 0
                    };
                    row = ws.LastRowUsed().RowBelow();
                    WriteRowData(row, errorList);
                }
                ws.Columns().AdjustToContents();
                workbook.SaveAs("errores.xlsx");
            }
        }

        public static void WriteRowData(IXLRow row, List<object> rowData)
        {
            foreach (object value in rowData)
            {
                WriteNextCell(row, value);
            }
        }

        private static void WriteFirstCell(IXLRangeBase row, object value)
        {
            row.FirstCell().Value = value;
        }

        private static void WriteNextCell(IXLRangeBase row, object value)
        {
            if (string.IsNullOrEmpty(row.FirstCell().Value.ToString()))
            {
                WriteFirstCell(row, value);
            }
            else
            {
                row.LastCellUsed().CellRight().Value = value;

            }
        }
    }
}