﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProcesoSaldos.Model;
using rrhh_licencias.Models;
using rrhh_licencias.Services;

namespace ProcesoSaldos.ExcelHelper
{
    public static class LicenciaHelper
    {
        private static holando_rrhh_licenciasEntities db = new holando_rrhh_licenciasEntities();
        public static void ProcessLicencias(List<LicenciaUsuarioExcel> licencias)
        {
            empleado rrhh = db.empleado.FirstOrDefault(e => e.legajo == "9245");
            var licenciaService = (LicenciaService)new LicenciaService().setDatabase(db);
            List<LicenciaUsuarioExcel> errores = new List<LicenciaUsuarioExcel>();
            foreach (LicenciaUsuarioExcel licencia in licencias)
            {
                licencia nuevaLicencia = null;
                try
                {
                    var empleado = db.empleado.FirstOrDefault(e => e.legajo == licencia.X_LEGAJO);
                    tipo_licencia tipoLicencia = GetTipoLicencia(licencia.ID_CONCEPTO_VAC);
                    DateTime fechaAprobacion = new DateTime(licencia.AA_VAC, 1, 1);
                    if (empleado == null)
                    {
                        Console.WriteLine("Empleado no encontrado");
                    }
                    nuevaLicencia = new licencia()
                    {
                        empleado = empleado,
                        empleado_id = empleado.id,
                        tipo_licencia_id = tipoLicencia.id,
                        tipo_licencia = tipoLicencia,
                        cantidad = tipoLicencia.event_class == "event-raz-particular"
                            ? licencia.CANT_1_2_DIAS_TOMADOS.GetValueOrDefault()
                            : (int) licencia.CANT_DIAS_TOMADOS,
                        estado = "aprobado",
                        fecha_inicio = licencia.FECHA_INICIO ?? fechaAprobacion,
                        fecha_fin = licencia.FECHA_FIN ?? fechaAprobacion,
                        fecha_solicitud = fechaAprobacion,
                        aprobador_jefe_id = empleado.jefe_id,
                        aprobador_rrhh_fecha = fechaAprobacion,
                        aprobador_rrhh_id = rrhh.id,
                        aprobador_jefe_fecha = fechaAprobacion,
                        estado_notificado = 0
                    };
                    if (nuevaLicencia.cantidad <= 0) continue;
                    licenciaService.saveOrUpdate(nuevaLicencia);
                    Console.WriteLine($"{empleado.nombres} {empleado.apellidos} - {tipoLicencia.nombre}");
                    Console.WriteLine(
                        $"cantidad {nuevaLicencia.cantidad}, {nuevaLicencia.fecha_inicio} - {nuevaLicencia.fecha_fin}");
                }
                catch (Exception e)
                {
                    var x = nuevaLicencia;
                    Console.WriteLine(e.StackTrace);
                    errores.Add(licencia);
                    Console.WriteLine($"Error con la licencia de {licencia.X_NOMBRE} {licencia.X_APELLIDO}");
                }
            }
            ClosedXMLHelper.GenerateExcelError(errores);
        }

        private static tipo_licencia GetTipoLicencia(string codigo)
        {
            switch (codigo)
            {
                case "EXA_AFIN":
                case "EXA_OTRAS":
                    return db.tipo_licencia.FirstOrDefault(tl => tl.event_class == "event-lic-estudio");
                case "VAC":
                    return db.tipo_licencia.FirstOrDefault(tl => tl.event_class == "event-vacaciones");
                case "RP":
                    return db.tipo_licencia.FirstOrDefault(tl => tl.event_class == "event-raz-particular");
                default: return null;
            }
        }
    }
}
